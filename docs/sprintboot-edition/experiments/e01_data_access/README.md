# Spring Boot数据访问技术

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [前置条件](#前置条件)
4. [指引Guides](#指引guides)
    1. [内容01：环境准备](#内容01环境准备)
    2. [内容02：`druid`整合](#内容02druid整合)
    3. [内容03：`mybatis`的整合](#内容03mybatis的整合)
    4. [内容04：`jpa`的整合](#内容04jpa的整合)
    5. [内容05：`redis`的整合](#内容05redis的整合)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：综合型
1. 目的：
    1. 理解`spring data`
    1. 掌握`druid`的整合
    1. 掌握`mybatis`的整合
    1. 掌握`jpa`的整合
    1. 掌握`redis`的整合
1. 内容与要求：
    1. 内容01：环境准备
    1. 内容02：`druid`整合
    1. 内容03：`mybatis`的整合
    1. 内容04：`jpa`的整合
    1. 内容05：`redis`的整合
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`IDEA`或其他`Java`开发环境、`jdk11`、`mysql:5.7`、`redis:7.0`

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 前置条件

1. 可访问`mysql:5.7`和`redis:7.0`
1. 可通过`mysql client`运行`sql script`

## 指引Guides

❗ 以下假定已有可访问的`mysql:5.7`和`redis:7.0` ❗

### 内容01：环境准备

新建一个`maven project`，设置`groupId`、`artifactId`、`version`和`parent`，参考如下（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<groupId>org.${FN_and_EN}</groupId>
<artifactId>e01</artifactId>
<version>1.0-SNAPSHOT</version>

<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.2</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

`pom.xml`引入`mysql connector`依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <scope>runtime</scope>
    </dependency>
</dependencies>
```

在`application.properties`中配置`mysql`属性（❗ 请将其中的`password`修改成您访问的`mysql server`的`password` ❗）

```properties {.line-numbers}
# mysql
spring.datasource.url=jdbc:mysql://localhost:3306/springbootdata?serverTimezone=Asia/Shanghai&useSSL=false&useUnicode=true&characterEncoding=utf8
spring.datasource.username=root
spring.datasource.password=root
```

`pom.xml`引入`spring boot test starter`依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```

在`mysql client`运行以下`sql script`创建数据库、创建数据表以及插入数据

```sql {.line-numbers}
DROP DATABASE IF EXISTS springbootdata;

CREATE DATABASE springbootdata;
USE springbootdata;

# create table `t_article`
CREATE TABLE `t_article`
(
    `id`      int(20) NOT NULL AUTO_INCREMENT COMMENT '文章id',
    `title`   varchar(200) DEFAULT NULL COMMENT '文章标题',
    `content` longtext COMMENT '文章内容',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4;

# create table `t_comment`
CREATE TABLE `t_comment`
(
    `id`      int(20) NOT NULL AUTO_INCREMENT COMMENT '评论id',
    `content` longtext COMMENT '评论内容',
    `author`  varchar(200) DEFAULT NULL COMMENT '评论作者',
    `a_id`    int(20)      DEFAULT NULL COMMENT '关联的文章id',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb4;

# insert data into `t_article`
INSERT INTO `t_article`
VALUES ('1', 'Spring Boot基础入门', '从入门到精通讲解...');
INSERT INTO `t_article`
VALUES ('2', 'Spring Cloud基础入门', '从入门到精通讲解...');

# insert data into `t_comment`
INSERT INTO `t_comment`
VALUES ('1', '很全、很详细', '狂奔的蜗牛', '1');
INSERT INTO `t_comment`
VALUES ('2', '赞一个', 'tom', '1');
INSERT INTO `t_comment`
VALUES ('3', '很详细', 'kitty', '1');
INSERT INTO `t_comment`
VALUES ('4', '很好，非常详细', '张三', '1');
INSERT INTO `t_comment`
VALUES ('5', '很不错', '张杨', '2');
```

### 内容02：`druid`整合

`pom.xml`引入`druid starter`依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid-spring-boot-starter</artifactId>
        <version>1.2.11</version>
    </dependency>
</dependencies>
```

`application.properties`配置`druid`属性

```properties {.line-numbers}
# druid
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.initialSize=20
spring.datasource.minIdle=10
spring.datasource.maxActive=100
```

创建并编写`configuration class`（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getDruid(){
        return new DruidDataSource();
    }
}
```

### 内容03：`mybatis`的整合

`pom.xml`引入`mybatis starter`依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>2.2.2</version>
    </dependency>
</dependencies>
```

创建并编写实体类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）（❗ 请补充其中的`getters`、`setters`、`toString` ❗）

```java {.line-numbers}
package org.${FN_and_EN}.domain;

import java.util.List;

public class Article {
    private Integer id;
    private String title;
    private String content;
    private List<Comment> commentList;
    
    // omit getters and setters

    // omit toString
}
```

```java {.line-numbers}
package org.${FN_and_EN}.domain;

public class Comment {
    private Integer id;
    private String content;
    private String author;
    private Integer aId;
    
    // omit getters and setters

    // omit toString
}
```

创建并编写数据访问处理接口（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.mapper;

import com.itheima.domain.Comment;
import org.apache.ibatis.annotations.*;

@Mapper
public interface CommentMapper {
    @Select("SELECT * FROM t_comment WHERE id =#{id}")
    Comment findById(Integer id);

    @Insert("INSERT INTO t_comment(content,author,a_id) " +
        "values (#{content},#{author},#{aId})")
    int insertComment(Comment comment);

    @Update("UPDATE t_comment SET content=#{content} WHERE id=#{id}")
    int updateComment(Comment comment);

    @Delete("DELETE FROM t_comment WHERE id=#{id}")
    int deleteComment(Integer id);
}
```

```java {.line-numbers}
package org.${FN_and_EN}.mapper;

import com.itheima.domain.Article;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ArticleMapper {
    Article selectArticle(Integer id);

    int updateArticle(Article article);
}
```

创建并编写映射文件（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.${FN_and_EN}.mapper.ArticleMapper">
    <select id="selectArticle" resultMap="articleWithComment">
        SELECT a.*, c.id AS c_id, c.content AS c_content, c.author, c.a_id AS c_aid
        FROM t_article AS a,
             t_comment AS c
        WHERE a.id = c.a_id
          AND a.id = #{id}
    </select>
    <resultMap id="articleWithComment" type="org.${FN_and_EN}.domain.Article">
        <id property="id" column="id"/>
        <result property="title" column="title"/>
        <result property="content" column="content"/>
        <collection property="commentList" ofType="org.${FN_and_EN}.domain.Comment">
            <id property="id" column="c_id"/>
            <result property="content" column="c_content"/>
            <result property="author" column="author"/>
            <result property="aId" column="c_aid"/>
        </collection>
    </resultMap>

    <update id="updateArticle" parameterType="org.${FN_and_EN}.domain.Article">
        UPDATE t_article
        <set>
            <if test="title !=null and title !=''">
                title=#{title},
            </if>
            <if test="content !=null and content !=''">
                content=#{content}
            </if>
        </set>
        WHERE id=#{id}
    </update>
</mapper>
```

`application.properties`中配置`mapper`映射文件路径、下划线风格与驼峰风格映射

```properties {.line-numbers}
# application.properties

mybatis.mapper-locations=classpath:mapper/*.xml

mybatis.configuration.map-underscore-to-camel-case=true
```

创建并编写测试类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.mapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CommentMapperTest {

    @Autowired
    private CommentMapper commentMapper;

    @Test
    void findById() {
        assertThat(commentMapper.findById(1).toString(),
            is(equalTo("Comment{id=1, content='很全、很详细', author='狂奔的蜗牛', aId=1}")));
    }
}
```

```java {.line-numbers}
package org.${FN_and_EN}.mapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ArticleMapperTest {

    @Autowired
    private ArticleMapper articleMapper;

    @Test
    void selectArticle() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Article{");
        stringBuilder.append("id=1, title='Spring Boot基础入门', content='从入门到精通讲解...'");
        stringBuilder.append(", ");
        stringBuilder.append("commentList=[");
        stringBuilder.append("Comment{id=1, content='很全、很详细', author='狂奔的蜗牛', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=2, content='赞一个', author='tom', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=3, content='很详细', author='kitty', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=4, content='很好，非常详细', author='张三', aId=1}");
        stringBuilder.append("]");
        stringBuilder.append("}");
        String expected = stringBuilder.toString();
        assertThat(articleMapper.selectArticle(1).toString(), is(equalTo(expected)));
    }
}
```

🔀 运行测试用例

### 内容04：`jpa`的整合

`pom.xml`引入`jpa starter`依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
</dependencies>
```

创建并编写实体类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）（❗ 请补充其中的`getters`、`setters`、`toString` ❗）

```java {.line-numbers}
package org.${FN_and_EN}.domain;

import javax.persistence.*;

@Entity(name = "t_comment")
public class Discuss {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String content;
    private String author;
    @Column(name = "a_id")
    private Integer aId;

    // omit getters and setters
    
    // omit toString
}
```

创建并编写数据访问处理接口（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.repository;

import com.itheima.domain.Discuss;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DiscussRepository extends JpaRepository<Discuss, Integer> {

    List<Discuss> findByAuthorNotNull();

    @Query("SELECT c FROM t_comment c WHERE  c.aId = ?1")
    List<Discuss> getDiscussPaged(Integer aid, Pageable pageable);

    @Query(value = "SELECT * FROM t_comment  WHERE  a_Id = ?1", nativeQuery = true)
    List<Discuss> getDiscussPaged2(Integer aid, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE t_comment c SET c.author = ?1 WHERE  c.id = ?2")
    int updateDiscuss(String author, Integer id);

    @Transactional
    @Modifying
    @Query("DELETE t_comment c WHERE  c.id = ?1")
    int deleteDiscuss(Integer id);
}
```

创建并编写测试类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.repository;

import com.itheima.domain.Discuss;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.startsWith;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class DiscussRepositoryTest {

    @Autowired
    private DiscussRepository repository;

    @Test
    void findById() {
        assertThat(repository.findById(1).isPresent(),is(true));
    }

    @Test
    @Modifying
    @Transactional
    @Rollback(true)
    void deleteDiscuss() {
        assertThat(repository.deleteDiscuss(3), is(equalTo(1)));
    }
}
```

🔀 运行测试用例

### 内容05：`redis`的整合

`pom.xml`引入`redis starter`依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>
</dependencies>
```

创建并编写实体类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）（❗ 请补充其中的`getters`、`setters`、`toString` ❗）

```java {.line-numbers}
package org.${FN_and_EN}.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;
import java.util.List;

@RedisHash("persons")
public class Person {
    @Id
    private String id;
    @Indexed
    private String firstname;
    @Indexed
    private String lastname;
    private Address address;
    private List<Family> familyList;

    public Person() {
    }

    public Person(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    // omit getters and setters
    // omit toString
}
```

```java {.line-numbers}
package org.${FN_and_EN}.domain;

import org.springframework.data.redis.core.index.Indexed;

public class Address {
    @Indexed
    private String city;
    @Indexed
    private String country;

    public Address() {
    }

    public Address(String city, String country) {
        this.city = city;
        this.country = country;
    }

    // omit getters and setters
    // omit toString
}
```

```java {.line-numbers}
package org.${FN_and_EN}.domain;

import org.springframework.data.redis.core.index.Indexed;

public class Family {
    @Indexed
    private String type;
    @Indexed
    private String username;

    public Family() {
    }

    public Family(String type, String username) {
        this.type = type;
        this.username = username;
    }

    // omit getters and setters
    // omit toString
}
```

创建并编写数据访问处理类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.repository;

import com.itheima.domain.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, String> {
    List<Person> findByLastname(String lastname);

    Page<Person> findPersonByLastname(String lastname, Pageable page);

    List<Person> findByFirstnameAndLastname(String firstname, String lastname);

    List<Person> findByAddress_City(String city);

    List<Person> findByFamilyList_Username(String username);
}
```

`application.properties`配置`redis`属性（请根据您的环境修改相应的属性值）

```properties {.line-numbers}
# application.properties

spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.password=
```

创建并编写测试类（❗ **`"${FN_and_EN}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）

```java {.line-numbers}
package org.${FN_and_EN}.repository;

import com.itheima.domain.Address;
import com.itheima.domain.Family;
import com.itheima.domain.Person;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class PersonRepositoryTest {
    @Autowired
    private PersonRepository repository;

    @BeforeEach
    void setUp() {
        Person person = new Person("张", "有才");
        person.setAddress(new Address("北京", "China"));
        List<Family> list = new ArrayList<>();
        list.add(new Family("父亲", "张良"));
        list.add(new Family("母亲", "李香君"));
        person.setFamilyList(list);
        Person person2 = new Person("James", "Harden");
        repository.save(person);
        repository.save(person2);
    }

    @AfterEach
    void tearDown() {
        this.repository.deleteAll();
    }

    @Test
    void findByAddress_City() {
        assertThat(repository.findByAddress_City("北京").size(), is(equalTo(1)));
    }

    @Test
    public void updatePerson() {
        Person person = repository.findByFirstnameAndLastname("张", "有才").get(0);
        person.setLastname("小明");
        assertThat(repository.save(person).getLastname(), is(equalTo("小明")));
    }

    @Test
    public void deletePerson() {
        repository.delete(repository.findByFirstnameAndLastname("张", "有才").get(0));
        assertThat(repository.findByFirstnameAndLastname("张", "有才").size(), is(equalTo(0)));
    }
}
```

🔀 运行测试用例
