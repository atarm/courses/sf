# Experiments

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Contents](#contents)
2. [Environment Setup Guide](#environment-setup-guide)
    1. [`windows` Environment Setup Guide](#windows-environment-setup-guide)
        1. [`jdk` for `windows`](#jdk-for-windows)
        2. [`maven` for `windows`](#maven-for-windows)
        3. [`spring tool suite 4` for `windows`](#spring-tool-suite-4-for-windows)
        4. [`git` for `windows`](#git-for-windows)
        5. [`docker desktop for windows`](#docker-desktop-for-windows)

<!-- /code_chunk_output -->

## Contents

```bash {.line-numbers cmd=true output=text hide=true run_on_save=true}
tree -I *_code_chunk -I *.html -I *.pdf -d -L 1 .
```

```bash {.line-numbers}
.
├── e01_data_access                    Spring Boot 数据访问技术
├── e02_web_develop                    Spring Boot web应用开发
└── e03_advanced_develop               Spring Boot 应用高级开发
```

## Environment Setup Guide

1. `jdk`：`jdk8`及以上，推荐`jdk11`
1. `maven`：`maven3.5`及以上，推荐`maven3.8.6`或最新版本
1. `ide`：`spring tool suite 4`或`intellij idea educational`或`intellij idea ultimate`
1. `git`(optional)：推荐最新版本
1. `docker`(optional)
    - `macos`、`windows`：推荐`docker desktop`最新版本
    - `linux`：实验环境详见[docker.com. Install Docker Desktop on Linux.](https://docs.docker.com/desktop/install/linux-install/)

>1. [spring.io. Building an Application with Spring Boot.](https://spring.io/guides/gs/spring-boot/)
>1. [spring.io. Getting Started.](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html)

### `windows` Environment Setup Guide

#### `jdk` for `windows`

1. 下载`jdk11`：<https://www.oracle.com/java/technologies/downloads/#java11-windows>
1. 安装`jdk11`：运行下载的安装程序，按默认选项安装
1. 配置环境变量：
    1. 打开`系统属性 >>> 高级 >>> 环境变量(N)...`
    1. 在`系统变量`中新增环境变量
        1. 变量名：`JAVA_HOME`
        1. 变量值：`jdk`的安装目录
    1. 在`系统变量`中修改环境变量
        1. 变量名：`Path`
        1. 变量值：在原值的最前面添加`%JAVA_HOME%/bin`
1. 验证安装：打开`cmd`，运行`java -version`

#### `maven` for `windows`

1. 下载`maven`：<https://maven.apache.org/download.cgi>，选择并下载`Binary zip archive`一行中的`zip`链接，如：`apache-maven-3.8.6-bin.zip`
1. 安装`maven`：将下载的压缩包解压到文件系统的某个位置，例如：`c:/maven/apache-maven-3.8.6`
1. 配置环境变量：
    1. 打开`系统属性 >>> 高级 >>> 环境变量(N)...`
    1. 在`系统变量`中新增环境变量
        1. 变量名：`M2_HOME`
        1. 变量值：压缩包解压的目录，例如：解压到`c:/maven/apache-maven-3.8.6`
    1. 在`系统变量`中修改环境变量
        1. 变量名：`Path`
        1. 变量值：在原值的最前面添加`%M2_HOME%/bin`
1. 验证安装：打开`cmd`，运行`mvn -version`

#### `spring tool suite 4` for `windows`

1. 下载`spring tool suite 4`：<https://spring.io/tools>，选择并下载`windows x86_64`链接
1. 安装`spring tool suite 4`：双击运行下载的文件，进行自解压，将解压后文件夹剪切并粘贴到文件系统的某个位置，例如：`c:/sts/sts-4.15.3.RELEASE`
1. 创建桌面快捷方式：为目录下的`SpringToolSuite4.exe`创建桌椅快捷方式

#### `git` for `windows`

1. 下载`git`：<https://git-scm.com/download/win>，选择并下载`64-bit Git for Windows Setup`
1. 安装`git`：运行下载的安装程序，按默认选项安装

#### `docker desktop for windows`

1. 下载`docker desktop for windows`：<https://www.docker.com/>，选择并下载`Windows`链接
1. 确保`windows`的`wsl`或`hyper-v`已开启，详见<https://docs.docker.com/desktop/install/windows-install/#system-requirements>
1. 安装`docker desktop for windows`：运行下载的安装程序，**安装完成后重启计算机**
1. 关闭开机自动启动`docker desktop for windows`：取消勾选`docker desktop for windows >>> setting >>> Start Docker Desktop when you log in`
