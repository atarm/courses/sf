# Spring Boot Web应用开发

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [指引Guides](#指引guides)
    1. [内容01：创建项目并进行配置](#内容01创建项目并进行配置)
    2. [内容02：整合`Spring MVC`和`Thymeleaf`](#内容02整合spring-mvc和thymeleaf)
    3. [内容03：整合`Java Web`三大件](#内容03整合java-web三大件)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：综合型
1. 目的：
    1. 掌握`Spring Boot`整合`Spring MVC`和`Thymeleaf`
    1. 掌握`Spring Boot`整合`Java Web`三大件
1. 内容与要求：
    1. 内容01：创建项目并进行配置
    1. 内容02：整合`Spring MVC`和`Thymeleaf`
    1. 内容03：整合`Java Web`三大件
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`IDEA`或其他`Java`开发环境、`jdk8`或以上

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"${fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**，指引中如出现 **"zhangsan68"** 请替换成您的信息

## 思考Thinkings

1. 怎样使用`browser`（例如：`chrome`）查看发出的`http`请求❓
1. `filter`使用一个方法同时实现了`request`和`response`两个方向的过滤处理的原理是什么❓

## 指引Guides

参考实现请见`${repo_root}/codes/springboot-edition/experiments/e02/`。

### 内容01：创建项目并进行配置

新建一个`maven project`。

`pom.xml`中配置`groupId`、`artifactId`、`version`和`parent`，参考如下（❗ **`"${fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<groupId>org.${fn_and_en}</groupId>
<artifactId>e02</artifactId>
<version>1.0-SNAPSHOT</version>

<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.2</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

`pom.xml`中配置依赖和插件，参考如下：

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
    </dependency>
</dependencies>

<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

在`${project_root}/src/main/java/resources/`下新建`application.yml`文件并配置，参考如下：

```yaml {.line-numbers}
server:
    port: 8082

spring.thymeleaf:
    cache: false
    encoding: "UTF-8"
    mode: "HTML5"
    prefix: "classpath:/templates/"
    suffix: ".html"

spring.messages:
    basename: "static.i18n.main"
```

### 内容02：整合`Spring MVC`和`Thymeleaf`

将`./resources/`下的静态资源复制到`${project_root}/src/main/java/resources/`下。

❗ **`i18n/`下各个资源文件的`fn_and_en`，请修改成您的信息** ❗

在`${project_root}/src/main/java/resources/`下新建`templates/login.html`页面，参考如下：

```html {.line-numbers}
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1,shrink-to-fit=no" name="viewport">
    <title th:text="#{login.title}"></title>
    <link rel="stylesheet" th:href="@{/css/bootstrap.min.css}">
    <link rel="stylesheet" th:href="@{/css/signin.css}">
</head>
<body class="text-center">
<!--region login form  -->
<form class="form-signin">
    <img class="mb-4" height="72" th:src="@{/img/login.jpg}" width="72">
    <h1 class="h3 mb-3 font-weight-normal" th:text="#{login.tip}">请登录</h1>
    <input autofocus="" class="form-control"
           required="" th:placeholder="#{login.username}" type="text">
    <input class="form-control" required=""
           th:placeholder="#{login.password}" type="password">

    <div class="checkbox mb-3">
        <input type="checkbox" value="remember-me">
        <label th:text="#{login.remember.me}"></label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" th:text="#{login.button}" type="submit">登录</button>
    <p class="mt-5 mb-3 text-muted">&copy; <span th:text="${currentYear}">2018</span>-<span
        th:text="${currentYear}+1">2019</span> develop by <span th:text="#{fn_and_en}">张三68</span></p>
    <a class="btn btn-sm" th:href="@{/login(l='zh_CN')}">中文</a>
    <a class="btn btn-sm" th:href="@{/login(l='en_US')}">English</a>
</form>
<!--endregion login form-->
</body>
</html>
```

编写启动类，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

编写区域解析器，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@Configuration
public class MyLocaleResolver implements LocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        String l = httpServletRequest.getParameter("l");
        String header = httpServletRequest.getHeader("Accept-Language");
        Locale locale=null;
        if(!StringUtils.isEmpty(l)){
            String[] split = l.split("_");
            locale=new Locale(split[0],split[1]);
        }else {
            String[] splits = header.split(",");
            String[] split = splits[0].split("-");
            locale=new Locale(split[0],split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, @Nullable
        HttpServletResponse httpServletResponse, @Nullable Locale locale) {
    }

    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
}
```

编写拦截器，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

@Component
public class CommonInterceptor implements HandlerInterceptor {

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        request.setAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }
}
```

新建并编写WebMvcConfigurer，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.zhangsan68.interceptor.CommonInterceptor;

@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    private CommonInterceptor interceptor;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor)
            .addPathPatterns("/**");
    }
}
```

运行（`mvn spring-boot:run`）并使用浏览器访问<http://localhost:8082/login>

### 内容03：整合`Java Web`三大件

新建并编写`servlet`，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlet/my-servlet")
public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doPost(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().println("hello MyServlet...");
    }
}
```

新建并编写`filter`，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter({"/servlet/my-servlet"})
public class MyFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        System.out.println("hello " + this.getClass().getCanonicalName() + "...");
        response.getWriter().println("before chain.doFilter()...");
        chain.doFilter(request, response);
        response.getWriter().println("after chain.doFilter()...");
    }
}
```

新建并编写`listener`，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("context initialized ...");
    }
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("context destroyed ...");
    }
}
```

启动类增加`@ServletComponentScan`，参考如下（❗ **"zhangsan68"** 请替换成您的信息 ❗）：

```java {.line-numbers}
package org.zhangsan68;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

运行（`mvn spring-boot:run`）并使用浏览器访问<http://localhost:8082/servlet/my-servlet>，观察页面访问结果和控制台输出。
