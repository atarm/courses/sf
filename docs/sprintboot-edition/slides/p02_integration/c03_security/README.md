---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Spring Boot Security_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Spring Boot Security

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [学习目标与概要](#学习目标与概要)
2. [`Spring Security`介绍](#spring-security介绍)
3. [`Spring Security`入门](#spring-security入门)
4. [`MVC Security`安全配置](#mvc-security安全配置)
5. [自定义用户认证](#自定义用户认证)
6. [自定义用户授权管理](#自定义用户授权管理)
7. [CSRF防护功能](#csrf防护功能)
8. [`security`管理前端页面](#security管理前端页面)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 学习目标与概要

---

1. 理解`Spring Boot`的默认安全管理
1. 掌握自定义用户认证的实现
1. 掌握自定义用户授权管理的实现
1. 掌握`Security`实现页面控制

---

1. 应用必须考虑安全问题
1. 有些请求需要认证被授权用户方可访问

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring Security`介绍

---

1. `Spring Security`是`Spring ecosystem`中用于提供安全访问控制的框架
1. `Spring Security`的两个重要概念：`authentication认证`和`authorization授权`

---

### `Spring Boot`整合`Spring Security`实现的安全管理功能

1. `MVC Security`是`Spring Boot`整合`Spring MVC`框架搭建的安全管理
1. `WebFlux Security`是`Spring Boot`整合`Spring WebFlux`框架搭建的安全管理
1. `OAuth2`实现第三方认证、单点登录等功能
1. `Actuator Security`实现运行环境的监控

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring Security`入门

---

1. 创建项目，引入`web`和`thymeleaf`依赖启动器
1. 创建`thymeleaf`模板文件
    1. `classpath:/templates/detail/common/*`：普通用户可访问的页面
    1. `classpath:/templates/detail/vip/*`：VIP用户方可访问的页面
    1. `classpath:/templates/index.html`：首页
1. 编写`controller`：`com.itheima.controller.FilmController`

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@Controller
public class FilmController {
    @GetMapping("/detail/{type}/{path}")
    public String toDetail(@PathVariable("type")String type, @PathVariable("path")String path) {
        return "detail/"+type+"/"+path;
    }
}
```

---

### 开启安全管理

1. 添加`spring-boot-starter-security`依赖启动器
1. 启动后，访问<http://localhost:8087>访问首页，项目中未创建用户登录页面，当添加了`security`依赖后，`security`会默认提供一个登录页面

![height:300](.assets/image/image-20221105095439181.png)

---

1. `security`默认提供一个可登录的用户，其中
    1. username: user
    1. password: 随机生成（随应用启动时随机生成并输出到`stdout`）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MVC Security`安全配置

---

1. 默认安全配置：
    1. `SecurityAutoConfiguration`：导入并自动化配置安全管理
    1. `UserDetailsServiceAutoConfiguration`：配置用户身份信息
1. 关闭`security`提供的默认安全配置：自定义`WebSecurityConfigurerAdapter`、`UserDetailsService`、`AuthenticationProvider`、`AuthenticationManager`

---

### `WebSecurityConfigurerAdapter`

1. `configure(AuthenticationManagerBuilder auth)`：定制用户认证管理器来实现用户认证
1. `configure(HttpSecurity http)`：定制基于HTTP请求的用户访问控制

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 自定义用户认证

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 内存身份认证

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@EnableWebSecurity  // 开启MVC security安全支持
public class SecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        auth.inMemoryAuthentication().passwordEncoder(encoder)
            .withUser("shitou").password(encoder.encode("123456")).roles("common")
            .and()
            .withUser("李四").password(encoder.encode("123456")).roles("vip");
    }
}
```

---

1. `@EnableWebSecurity`是一个组合注解，包括:
    1. `@Configuration`
    1. `@Import({WebSecurityConfiguration.class, SpringWebMvcImportSelector.class})`
    1. `@EnableGlobalAuthentication`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### JDBC身份认证

---

1. 数据库表
    1. `t_customer`：用户表
    1. `t_authority`：用户权限表
    1. `t_customer_authority`：用户权限关联表
1. 添加`JDBC`连接数据库的依赖启动器
    1. `spring-boot-starter-jdbc`
    1. `mysql-connector-java`
1. 数据库连接配置

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@EnableWebSecurity  // 开启MVC security安全支持
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String userSQL ="select username,password,valid from t_customer " +
                "where username = ?";
        String authoritySQL="select c.username,a.authority from t_customer c,t_authority a,"+
                "t_customer_authority ca where ca.customer_id=c.id " +
                "and ca.authority_id=a.id and c.username =?";
        auth.jdbcAuthentication().passwordEncoder(encoder)
                .dataSource(dataSource)
                .usersByUsernameQuery(userSQL)
                .authoritiesByUsernameQuery(authoritySQL);
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### UserDetailsService 身份认证

---

1. 定义用户及角色信息的查询服务接口
1. 定义`UserDetailsService`用于封装认证用户信息
1. 使用`UserDetailsService`进行身份认证

---

#### 定义用户信息查询服务接口

---

```java {.line-numbers}
package com.itheima.service;
@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private RedisTemplate redisTemplate;
    public Customer getCustomer(String username){
        Customer customer=null;
        Object o = redisTemplate.opsForValue().get("customer_"+username);
        if(o!=null){
            customer=(Customer)o;
        }else {
            customer = customerRepository.findByUsername(username);
            if(customer!=null){
                redisTemplate.opsForValue().set("customer_"+username,customer);
            }
        }
        return customer;
    }
}
```

---

#### 定义用户角色信息查询服务接口

---

```java {.line-numbers}
package com.itheima.service;
@Service
public class CustomerService {
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private RedisTemplate redisTemplate;
    public List<Authority> getCustomerAuthority(String username){
        List<Authority> authorities=null;
        Object o = redisTemplate.opsForValue().get("authorities_"+username);
        if(o!=null){
            authorities=(List<Authority>)o;
        }else {
            authorities=authorityRepository.findAuthoritiesByUsername(username);
            if(authorities.size()>0){
                redisTemplate.opsForValue().set("authorities_"+username,authorities);
            }
        }
        return authorities;
    }
}
```

---

#### 定义UserDetailsService封装认证用户信息

---

```java {.line-numbers}
package com.itheima.service;

// omit imports

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private CustomerService customerService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerService.getCustomer(username);
        List<Authority> authorities = customerService.getCustomerAuthority(username);
        List<SimpleGrantedAuthority> list = authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getAuthority())).collect(Collectors.toList());
        if (customer != null) {
            UserDetails userDetails = new User(customer.getUsername(), customer.getPassword(), list);
            return userDetails;
        } else {
            throw new UsernameNotFoundException("当前用户不存在！");
        }
    }
}
```

---

#### 配置使用`UserDetailsService`进行身份认证

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@EnableWebSecurity  // 开启MVC security安全支持
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
    }
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 自定义用户授权管理

---

### 自定义用户访问控制

`com.itheima.config.SecurityConfig.configure(HttpSecurity http)`

```java {.line-numbers}
http.authorizeRequests()
    .antMatchers("/").permitAll()
    // 需要对static文件夹下静态资源进行统一放行
    .antMatchers("/login/**").permitAll()
    .antMatchers("/detail/common/**").hasRole("common")
    .antMatchers("/detail/vip/**").hasRole("vip")
    .anyRequest().authenticated();
```

---

### 自定义用户登录

1. 自定义用户登录页面：新建`classpath:/templates/login.html >>> <form>`

---

`com.itheima.config.SecurityConfig.configure(HttpSecurity http)`

```java {.line-numbers}
http.formLogin()
    .loginPage("/userLogin").permitAll()
    .usernameParameter("name").passwordParameter("pwd")
    .defaultSuccessUrl("/")
    .failureUrl("/userLogin?error");
```

---

### 自定义用户退出

1. 在`index.html`中添加自定义用户退出链接

```html {.line-numbers}
<form method="post" th:action="@{/mylogout}">
    <input th:type="submit" th:value="注销"/>
</form>
```

---

`com.itheima.config.SecurityConfig.configure(HttpSecurity http)`

```java {.line-numbers}
http.logout()
    .logoutUrl("/mylogout")
    .logoutSuccessUrl("/");
```

---

### 登录用户信息获取

---

#### 使用`HttpSession`获取用户信息

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@Controller
public class FilmController {
    @GetMapping("/getuserBySession")
    @ResponseBody
    public void getUserBySession(HttpSession session) {
        Enumeration<String> names = session.getAttributeNames();
        while (names.hasMoreElements()) {
            String element = names.nextElement();
            SecurityContextImpl attribute = (SecurityContextImpl) session.getAttribute(element);
            System.out.println("element: " + element);
            System.out.println("attribute: " + attribute);
            Authentication authentication = attribute.getAuthentication();
            UserDetails principal = (UserDetails) authentication.getPrincipal();
            System.out.println(principal);
            System.out.println("username: " + principal.getUsername());
        }
    }
}
```

---

#### 使用`SecurityContextHolder`获取用户信息

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@Controller
public class FilmController {
    @GetMapping("/getUserByContext")
    @ResponseBody
    public void getUserByContext() {
        // 获取应用上下文
        SecurityContext context = SecurityContextHolder.getContext();
        System.out.println("userDetails: " + context);
        // 获取用户相关信息
        Authentication authentication = context.getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        System.out.println(principal);
        System.out.println("username: " + principal.getUsername());
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### "记住我"功能

---

#### 基于简单加密`Token`方式

---

1. 在`login.html`中添加

```html {.line-numbers}
<div class="checkbox mb-3">
    <label>
        <input type="checkbox" name="rememberme"> 记住我
    </label>
</div>
```

---

`com.itheima.config.SecurityConfig.configure(HttpSecurity http)`

```java {.line-numbers}
http.rememberMe()
    .rememberMeParameter("rememberme")
    .tokenValiditySeconds(200);
```

---

#### 基于持久化`Token`方式

---

```sql {.line-numbers}
create table persistent_logins
(
    username  varchar(64) not null,
    series    varchar(64) primary key,
    token     varchar(64) not null,
    last_used timestamp   not null
);
```

---

`com.itheima.config.SecurityConfig.configure(HttpSecurity http)`

```java {.line-numbers}
http.rememberMe()
    .rememberMeParameter("rememberme")
    .tokenValiditySeconds(200)
    .tokenRepository(tokenRepository());
```

---

```java {.line-numbers}
@Bean
public JdbcTokenRepositoryImpl tokenRepository() {
    JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
    tokenRepository.setDataSource(dataSource);
    return tokenRepository;
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## CSRF防护功能

---

1. `CSRF(Cross Site Request Forgery)`
1. `CSRF`防护功能关闭
1. 针对`from`表单数据修改的`CSRF Token`配置
1. 针对`ajax`数据修改请求的`CSRF Token`配置

---

```html {.line-numbers}
<!--classpath:/templates/csrf/csrfTest.html-->

<form method="post" th:action="@{/updateUser}">
    用户名: <input type="text" name="username" /><br />
    密&nbsp;&nbsp;码: <input type="password" name="password" /><br />
    <button type="submit">修改</button>
</form>
```

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@Controller
public class CSRFController {
    @GetMapping("/toUpdate")
    public String toUpdate() {
        return "csrf/csrfTest";
    }

    @ResponseBody
    @PostMapping(value = "/updateUser")
    public String updateUser(@RequestParam String username, @RequestParam String password,
                             HttpServletRequest request) {
        System.out.println(username);
        System.out.println(password);
        String csrfToken = request.getParameter("_csrf");
        System.out.println(csrfToken);
        return "ok";
    }
}
```

---

1. 整合`Spring Security`后，项目默认启用了`CSRF`安全防护功能，项目中所有涉及到数据修改方式的请求都会被拦截
1. 两种处理方式：
    1. 关闭`Security`默认开启的`CSRF`
    1. 配置`Security`需要的`CSRF Token`

---

### 关闭`CSRF`

---

```java {.line-numbers}
@Override
protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable();
}
```

---

### 配置`CSRF token`

1. 针对Form表单数据修改的CSRF Token配置
1. 针对Ajax数据修改请求的CSRF Token配置

---

#### `form csrf token`

---

```html {.line-numbers}
<form method="post" th:action="@{/updateUser}">
    <input type="hidden" th:name="${_csrf.parameterName}" th:value="${_csrf.token}"/>
    用户名: <input type="text" name="username" /><br />
    密&nbsp;&nbsp;码: <input type="password" name="password" /><br />
    <button type="submit">修改</button>
</form>
```

---

#### `ajax csrf token`

---

```html {.line-numbers}
<head>
    <meta name="_csrf" th:content="${_csrf.token}"/>
    <meta name="_csrf_header" th:content="${_csrf.headerName}"/>
</head>

<script type="javascript">
$(function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});
</script>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `security`管理前端页面

---

1. 添加thymeleaf-extras-springsecurity5依赖启动器
1. 修改前端页面，使用Security相关标签进行页面控制

---

```xml {.line-numbers}
<dependency>
    <groupId>org.thymeleaf.extras</groupId>
    <artifactId>thymeleaf-extras-springsecurity5</artifactId>
</dependency>
```

---

```html {.line-numbers}
<!-- classpath:/templates/index.html-->

<html xmlns="http://www.w3.org/1999/xhtml" 
        xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity5"
        xmlns:th="http://www.thymeleaf.org">
<div sec:authorize="isAnonymous()"></div>
<div sec:authorize="isAuthenticated()"></div>
<div sec:authorize="hasRole('common')"></div>
<div sec:authorize="hasAuthority('ROLE_vip')"></div>
</html>
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
