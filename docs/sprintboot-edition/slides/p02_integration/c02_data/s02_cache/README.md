---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Spring Boot Caching_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Spring Boot缓存Spring Boot Caching

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [学习目标](#学习目标)
2. [缓存的基本概念](#缓存的基本概念)
3. [环境搭建](#环境搭建)
4. [`Spring Boot`默认缓存管理](#spring-boot默认缓存管理)
5. [`Spring Boot`整合`Redis`实现缓存](#spring-boot整合redis实现缓存)
6. [`Spring Boot`的`Redis`自定义缓存序列化](#spring-boot的redis自定义缓存序列化)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 学习目标

1. 了解`Spring Boot`的默认缓存
1. 熟悉`Spring Boot`中`Redis`的缓存机制及其实现
1. 掌握`Spring Boot`整合`Redis`缓存

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 缓存的基本概念

---

1. 缓存是在高速存储器中存储一部分低速存储器的数据以减少对低速存储器的访问频率，从而加快访问速度
1. 缓存是一种以空间换时间的技术
1. 这里的缓存指的是应用于用户访问量较大的系统的数据库的内存缓存，对高频热点数据有频繁的读访问，减少对数据库的高并发访问
1. `Spring Boot`继承了`Spring Framework`的缓存管理功能，支持透明地向应用程序添加缓存并对缓存进行管理

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 环境搭建

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 创建项目

---

1. `maven`
1. `spring initializr`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 准备环境和初始数据

---

1. `mysql`
1. `redis`
    1. 🌟 [`RedisInsight`](https://redis.com/redis-enterprise/redis-insight/)
    1. ⭐ [`AnotherRedisDesktopManager`](https://github.com/qishibo/AnotherRedisDesktopManager)
    1. [`RESP.app (formerly Redis Desktop Manager)`](https://resp.app/)
1. [`postman`](https://www.postman.com/)

```bash {.line-numbers}
./start_test_env.sh

docker compose up -d
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 配置依赖和插件

---

```xml {.line-numbers}
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```

---

```xml {.line-numbers}
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

---

```xml {.line-numbers}
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <excludes>
                    <exclude>
                        <groupId>org.projectlombok</groupId>
                        <artifactId>lombok</artifactId>
                    </exclude>
                </excludes>
            </configuration>
        </plugin>
    </plugins>
</build>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写实体类

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

@Entity(name = "t_comment")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@NoArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String content;
    private String author;
    @Column(name = "a_id")
    private Integer aId;
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`dao`类

---

```java {.line-numbers}
package com.itheima.dao.repository;

// omit imports

public interface CommentRepository extends JpaRepository<Comment,Integer> {

    @Transactional
    @Modifying
    @Query("UPDATE t_comment c SET c.author= ?1 WHERE  c.id = ?2")
    public int updateComment(String author,Integer id);
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`service`类

---

```java {.line-numbers}
package com.itheima.service;

// omit imports

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public Comment findCommentById(int id) {
        Optional<Comment> optional = commentRepository.findById(id);
        return optional.isPresent()? optional.get() : null;
    }

    public Comment updateComment(Comment comment) {
        commentRepository.updateComment(comment.getAuthor(), comment.getAId());
        return comment;
    }

    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`controller`类

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@RestController
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/get/{id}")
    public Comment findById(@PathVariable("id") int id) {
        Comment comment = commentService.findCommentById(id);
        return comment;
    }

    @PutMapping("/update/{id}/{author}")
    public Comment updateComment(@PathVariable("id") int id,
                                 @PathVariable("author") String author) {
        Comment comment = commentService.findCommentById(id);
        comment.setAuthor(author);
        Comment updateComment = commentService.updateComment(comment);
        return updateComment;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteComment(@PathVariable("id") int id) {
        commentService.deleteCommentById(id);
    }
}
```

---

#### `@RestController`

1. `@RestController` is a convenience annotation that is itself annotated with `@Controller` and `@ResponseBody`
1. instruct a `Controller` to return a formatted response as `JSON` or as another data-oriented format such as `XML` simply by adding an `@ResponseBody` annotation to the `class` or `method` (`JSON` by default)
1. results in the `return value` of a method being the entire body of the response to a web request, instead of being returned as a part of the `Model`.

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `RESTful`

1. [阮一峰. 理解RESTful架构. 2011-09-12.](https://www.ruanyifeng.com/blog/2011/09/restful.html)
1. [阮一峰. RESTful API 设计指南. 2014-05-22.](https://www.ruanyifeng.com/blog/2014/05/restful_api.html)
1. [Roy Thomas Fielding. Architectural Styles and the Design of Network-based Software Architectures. 2000.](https://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm) 
1. [陈皓. 一把梭：REST API 全用 POST. 2022-02-13.](https://coolshell.cn/articles/22173.html)

---

![height:600](.assets/image/bg2011091201.jpg)

---

1. `Roy Thomas Fielding`于2000的博士论文提出`REST`
1. `Roy Thomas Fielding`是`HTTP1.0`和`HTTP1.1`的主要设计者、`Apache HTTP Server`的作者之一、`Apache Software Foundation`的第一任主席
1. `REST(Representational State Transfer)`，如果一个架构符合`REST`原则，就称它为`RESTful`架构

---

#### `Roy Thomas Fielding`

1. born: 1965, Laguna Beach, California
1. Alma mater: University of California, Irvine（加州大学欧文分校）

---

![height:600](.assets/image/0*GN2n0rtrVwEN5XlM.jpeg)

---

1. `GET`（`select`）：获取资源，数据在`uri`
1. `POST`（`insert`）：新建资源（也可以用于更新资源），数据在`request-body`
1. `PUT`（`update`）：更新资源（提供更新后的完整信息），数据在`uri`
1. `PATCH`（`update`）：更新资源（提供更新后的局部信息或更新的运算），数据在`uri`
1. `DELETE`（`delete`）：删除资源，数据在`uri`

<!-- ❗ `uri`中不能有动词，只能有名词，并且是复数名词 ❗ -->

---

#### Request

1. `GET` `/zoos`：列出所有动物园
1. `POST` `/zoos`：新建一个动物园
1. `GET` `/zoos/{id}`：获取某个指定动物园的信息
1. `PUT` `/zoos/{id}?key1=value1&key2=value2`：更新某个指定动物园的信息（提供该动物园的全部信息）
1. `PATCH` `/zoos/{id}?key=value`：更新某个指定动物园的信息（提供该动物园的部分信息）
1. `DELETE` `/zoos/{id}`：删除某个动物园
1. `GET` `/zoos/{id}/animals`：列出某个指定动物园的所有动物
1. `DELETE` `/zoos/{id}/animals/{id}`：删除某个指定动物园的指定动物

---

#### Response

1. `GET` `/collection`：返回资源对象的列表
1. `GET` `/collection/resource`：返回单个资源对象
1. `POST` `/collection`：返回新生成的资源对象
1. `PUT` `/collection/resource`：返回完整的资源对象
1. `PATCH` `/collection/resource`：返回完整的资源对象
1. `DELETE` `/collection/resource`：返回一个删除资源的数量

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 配置`application.yml`

---

```yaml {.line-numbers}
server:
    port: 8086
spring:
    datasource:
        url: "jdbc:mysql://localhost:3306/springbootdata?serverTimezone=Asia/Shanghai&useSSL=false&useUnicode=true&characterEncoding=utf8"
        username: "root"
        password: "root"
    jpa:
        show-sql: true
```

- `spring.jpa.show-sql`：开启`terminal`输出`sql`，方便`debug`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 运行

---

1. `mvn spring-boot:run`
1. <http://localhost:8086/get/1>：每请求一次，都会执行一次`sql`访问数据库

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring Boot`默认缓存管理

---

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `@EnableCaching`开启基于注解的缓存支持

---

```java {.line-numbers}
package com.itheima;

// omit imports

@EnableCaching
@SpringBootApplication
public class Chapter06Application {
    public static void main(String[] args) {
        SpringApplication.run(Chapter06Application.class, args);
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `@Cacheable`对数据操作方法进行缓存

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@RestController
@RequestMapping("/annotation")
public class AnnotationCommentController {
    @Autowired
    private AnnotationCommentService annotationCommentService;

    @GetMapping("/get/{id}")
    public Comment findById(@PathVariable("id") int id) {
        Comment comment = annotationCommentService.findCommentById(id);
        return comment;
    }
}
```

---

1. <http://localhost:8086/annotation/get/1>
    1. 首次访问：执行`sql`
    1. 后续访问：读取缓存

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Spring Boot`缓存注解

---

#### `@EnableCaching`

```java {.line-numbers}
@Target(value=TYPE)
@Retention(value=RUNTIME)
@Documented
@Import(value=CachingConfigurationSelector.class)
public @interface EnableCaching
```

---

1. 用于开启基于注解的缓存支持
1. `Spring Framework`提供
1. 标注在`class`（在`Spring Boot`中，通常配置在项目启动类上）

---

#### `@Cacheable`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
@Retention(value=RUNTIME)
@Inherited
@Documented
public @interface Cacheable
```

---

1. 用于对方法结果进行缓存存储
1. `Spring Framework`提供
1. 标注在`class`或`method`（通常标注在数据查询方法）
1. 执行逻辑：先进行缓存查询，如果为空则进行方法查询，并将结果进行缓存；如果缓存中有数据，不进行方法查询，而是直接使用缓存数据

---

|property|description|remark|
|--|--|--|
`value`/`cacheNames`|指定缓存空间的名称，必配属性|二选一|
`key`|指定缓存数据的`key`，默认使用方法参数值，可以使用SpEL表达式|----|
`keyGenerator`|指定缓存数据的key的生成器|与`key`二选一|
`cacheManager`|指定缓存管理器|----|
`cacheResolver`|指定缓存解析器|与`cacheManager`二选一|
`condition`|在符合某条件下，进行数据缓存|----|
`unless`|在符合某条件下，不进行数据缓存|----|
`sync`|指定是否使用异步缓存，默认`false`|

---

#### `@CachePut`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
@Retention(value=RUNTIME)
@Inherited
@Documented
public @interface CachePut
```

---

1. 用于更新缓存数据
1. `Spring Framework`提供
1. 标注在`class`或`method`（通常标注在数据更新方法）
1. 默认执行逻辑：先进行数据更新，然后将数据更新到缓存中
1. `@CachePut`的属性与`@Cacheable`相同

---

#### `@CacheEvict`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
@Retention(value=RUNTIME)
@Inherited
@Documented
public @interface CacheEvict
```

---

1. 用于删除缓存数据
1. `Spring Framework`提供
1. 标注在`class`或`method`（通常标注在数据删除方法）
1. 执行逻辑：先进行方法调用，然后将缓存进行清除
1. `@CachePut`比`@Cacheable`多两个属性`allEntries`和`beforeInvocation`

---

1. `allEntries`：是否清除指定缓存空间中的所有缓存数据，默认值为`false`（即默认只删除指定`key`对应的缓存数据）
1. `beforeInvocation`：是否在方法执行之前进行缓存清除，默认值为`false`（即默认在执行方法后再进行缓存清除）

---

#### `@Caching`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
@Retention(value=RUNTIME)
@Inherited
@Documented
public @interface Caching
```

---

1. `@Caching`注解内部包含有`Cacheable`、`put`和`evict`三个属性，分别对应于`@Cacheable`、`@CachePut`和`@CacheEvict`三个注解

<!--TODO: @Caching怎样使用-->

---

#### `@CacheConfig`

```java {.line-numbers}
@Target(value=TYPE)
@Retention(value=RUNTIME)
@Documented
public @interface CacheConfig
```

---

1. 用于统筹管理类中所有使用`@Cacheable`、`@CachePut`和`@CacheEvict`注解标注中的公共属性，这些公共属性包括有`cacheNames`、`keyGenerator`、`cacheManager`和`cacheResolver`
1. 标注在`class`上

---

```java {.line-numbers}
package com.itheima.service;

@CacheConfig(cacheNames = "comment")
@Service
public class CommentService{
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring Boot`整合`Redis`实现缓存

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Spring Boot`支持的缓存

---

1. `Generic`
1. `JCache` (`JSR-107`) (`EhCache 3`、`Hazelcast`、`Infinispan`等)
1. `EhCache 2.x`
1. `Hazelcast`
1. `Infinispan`
1. `Couchbase`
1. `Redis`
1. `Caffeine`
1. `Simple`（默认）

---

1. 如果`application`中没有定义类型为`org.springframework.cache.CacheManager`或`org.springframework.cache.interceptor.CacheResolver`的`bean`组件，将尝试依次启用上述的缓存组件
1. 如果没有任何缓存组件，将使用`Simple`缓存组件进行管理
1. `Simple`使用内存中的`ConcurrentHashMap`进行缓存存储

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Spring Boot`添加`Redis`

---

```xml {.line-numbers}
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

---

```yaml {.line-numbers}
spring:
    redis:
        host: "127.0.0.1"
        port: "6379"
        password: null
    cache:
        redis:
            time-to-live: 60000
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 基于注解的`Redis`缓存实现

---

#### 编写`AnnotationCommentService`

---

```java {.line-numbers}
package com.itheima.service;

// omit imports

@CacheConfig(cacheNames = "comment")
@Service
public class AnnotationCommentService {
    @Autowired
    private CommentRepository commentRepository;

    @Cacheable(unless = "#result==null")
    public Comment findCommentById(int id) {
        Optional<Comment> optional = commentRepository.findById(id);
        return optional.isPresent()? optional.get() : null;
    }

    @CachePut(key = "#result.id")
    public Comment updateComment(Comment comment) {
        commentRepository.updateComment(comment.getAuthor(), comment.getAId());
        return comment;
    }

    @CacheEvict()
    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
    }
}
```

---

#### `Comment`添加实现`Serializable`

```java {.line-numbers}
java.lang.IllegalArgumentException: \
    DefaultSerializer requires a Serializable \
    payload but received an object of type
```

```java {.line-numbers}
public class Comment implements Serializable{
}
```

---

1. 默认情况下，`redis`进缓存存储时需要被存储对象先实现`Serializable`以使用默认的`JdkSerializationRedisSerializer`序列化
1. 默认的`JdkSerializationRedisSerializer`的序列化结果不方便对数据进行可视化查看，因为，通常会自定义序列化格式

---

1. `postman`:
    1. `get`: <http://localhost:8086/annotation/get/1>
    1. `put`: <http://localhost:8086/annotation/update/1/shitou>
    1. `get`: <http://localhost:8086/annotation/get/1>
    1. `delete`: <http://localhost:8086/annotation/delete/1>
    1. `get`: <http://localhost:8086/annotation/get/1>

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 基于`api`的`Redis`缓存实现

1. [SpringBoot集成Redis - 基于RedisTemplate+Jedis的数据操作.](https://www.pdai.tech/md/spring/springboot/springboot-x-redis-jedis.html)
1. [SpringBoot集成Redis - 基于RedisTemplate+Lettuce数据操作.](https://www.pdai.tech/md/spring/springboot/springboot-x-redis-lettuce.html)

---

![height:560](.assets/image/c3c1c0e8-3a78-4426-961f-b46dd0879dd8.png)

---

```java {.line-numbers}
package com.itheima.service;

// omit imports

@Service
public class ApiCommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private RedisTemplate redisTemplate;

    public Comment findCommentById(int id) {
        Object object = redisTemplate.opsForValue().get("comment_" + id);
        if (object != null) {
            return (Comment) object;
        } else {
            Optional<Comment> optional = commentRepository.findById(id);
            if (optional.isPresent()) {
                Comment comment = optional.get();
                redisTemplate.opsForValue().set("comment_" + id, comment, 1, TimeUnit.DAYS);
                return comment;
            } else {
                return null;
            }
        }
    }

    public Comment updateComment(Comment comment) {
        commentRepository.updateComment(comment.getAuthor(), comment.getAId());
        redisTemplate.opsForValue().set("comment_" + comment.getId(), comment);
        return comment;
    }

    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
        redisTemplate.delete("comment_" + id);
    }
}
```

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@RestController
@RequestMapping("/api")
public class ApiCommentController {
    @Autowired
    private ApiCommentService apiCommentService;

    @GetMapping("/get/{id}")
    public Comment findById(@PathVariable("id") int id) {
        return apiCommentService.findCommentById(id);
    }

    @PutMapping("/update/{id}/{author}")
    public Comment updateComment(@PathVariable("id") int id,
                                 @PathVariable("author") String author) {
        Comment comment = apiCommentService.findCommentById(id);
        comment.setAuthor(author);
        return apiCommentService.updateComment(comment);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteComment(@PathVariable("id") int id) {
        apiCommentService.deleteCommentById(id);
    }
}
```

---

1. 基于`Redis API`缓存实现不需要`@EnableCaching`注解支持
1. 基于`Redis API`进行缓存管理更加灵活，但相应的代码量也更多

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring Boot`的`Redis`自定义缓存序列化

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 自定义基于`api`的序列化机制

---

#### 默认的`RedisTemplate`序列化机制

1. 如果没有设置`serializer`，则`RedisTemplate`默认使用`JdkSerializationRedisSerializer`，因此，进行数据缓存的实体须`implements Serializable`（`Serializable`是个空接口）
1. `RedisAutoConfiguration`类中`redisTemplate(RedisConnectionFactory)`返回`RedisTemplate`，该方法标注`@ConditionalOnMissingBean(name = "redisTemplate")`，即，如果自定义了一个名为`redisTemplate`的`Bean`，则将覆盖该默认的`RedisTemplate`

---

```java {.line-numbers}
public class RedisAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean(name = "redisTemplate")
    @ConditionalOnSingleCandidate(RedisConnectionFactory.class)
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }
}
```

>[Spring Boot 2.7.x - RedisAutoConfiguration.java](https://github.com/spring-projects/spring-boot/blob/2.7.x/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisAutoConfiguration.java)

---

#### 自定义`RedisTemplate`序列化机制

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object> template = new RedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        // 使用JSON格式序列化对象，对缓存数据key和value进行转换
        Jackson2JsonRedisSerializer jacksonSeial = new Jackson2JsonRedisSerializer(Object.class);
        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jacksonSeial.setObjectMapper(om);

        // 设置RedisTemplate模板API的序列化方式为JSON
        template.setDefaultSerializer(jacksonSeial);
        return template;
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 自定义基于`annotation`的序列化机制

---

#### 默认的`annotation`序列化机制

1. `RedisCacheConfiguration`在定制`RedisCacheManager`，也默认使用`JdkSerializationRedisSerializer`
1. 如果使用自定义序列化方式的`RedisCacheManager`进行数据缓存操作，可以创建名为`cacheManager`的`Bean`组件，并在该组件中设置序列化方式

>[Spring Boot 2.7.x - RedisCacheConfiguration.java](https://github.com/spring-projects/spring-boot/blob/2.7.x/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/RedisCacheConfiguration.java)

---

#### 自定义基于`annotation`的默认序列化机制

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class RedisConfig {

    @Bean
    public RedisCacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        // 分别创建String和JSON格式序列化对象，对缓存数据key和value进行转换
        RedisSerializer<String> strSerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer jacksonSeial = new Jackson2JsonRedisSerializer(Object.class);
        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jacksonSeial.setObjectMapper(om);

        // 定制缓存数据序列化方式及时效
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
            .entryTtl(Duration.ofMinutes(1))
            .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(strSerializer))
            .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jacksonSeial))
            .disableCachingNullValues();

        RedisCacheManager cacheManager = RedisCacheManager.builder(redisConnectionFactory).cacheDefaults(config).build();
        return cacheManager;
    }
}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
