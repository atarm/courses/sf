---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Data Access_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Data Access

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [学习目标](#学习目标)
2. [`spring boot`数据访问](#spring-boot数据访问)
3. [数据环境准备](#数据环境准备)
4. [`druid` Integration](#druid-integration)
5. [`mybatis` Integration](#mybatis-integration)
6. [`spring data jpa` Integration](#spring-data-jpa-integration)
7. [`redis` Integration](#redis-integration)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 学习目标

1. 理解`spring data`
1. 掌握`druid`的整合
1. 掌握`mybatis`的整合
1. 掌握`jpa`的整合
1. 掌握`redis`的整合

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring boot`数据访问

---

1. 开发应用程序，一般都需要使用数据库存储数据：业务数据、缓存数据等
1. 数据访问层也称为持久层、`DAO(Data Access Object)`层等
1. 从原生的`JDBC(Java Database Connectivity)`中衍生出其他持久层框架

---

### 常用的`data-starter`

|name|orm/database|
|:--|:--|
|`spring-boot-starter-data-jpa`|`hibernate`|
|`spring-boot-starter-data-mongodb`|`mongodb`|
|`spring-boot-starter-data-redis`|`redis`|
|`spring-boot-starter-data-neo4j`|`neo4j`|
|`mybatis-spring-boot-starter`|`mybatis`|

❗ `mybatis`不在`spring data`框架内 ❗

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据环境准备

---

```yaml {.line-numbers}
# src/test/resources/test_env/docker-compose.yml
services:
    mysql-db:
        image: "mysql:5.7"
        environment:
            MYSQL_USER: "user"
            MYSQL_PASSWORD: "user"
            MYSQL_ROOT_PASSWORD: "root"
            TZ: "Asia/Shanghai"
            LANG: "C.UTF-8"
        ports:
            - "3306:3306"
        expose:
            - "3306"
        volumes:
            - "mysql-db-data:/var/lib/mysql"
            - "./initdb.d:/docker-entrypoint-initdb.d/"
        command:
            - "--character-set-server=utf8mb4"
            - "--collation-server=utf8mb4_general_ci"
            - "--skip-character-set-client-handshake"
```

---

```yaml {.line-numbers}
# src/test/resources/test_env/docker-compose.yml
services:
    redis:
        image: "redis:7.0-alpine"
        restart: "no"
        environment:
            TZ: "Asia/Shanghai"
            LANG: "C.UTF-8"
        ports:
            - "6379:6379"
        expose:
            - "6379"
        volumes:
            - "redis-data:/data"
```

---

```bash {.line-numbers}
cd ${docker-compose.yml_directory}

docker compose up -d

docker compose down --volumes
```

---

### 创建数据库`springbootdata`

```sql {.line-numbers}
CREATE DATABASE springbootdata;
```

---

### 创建数据表`t_article`

```sql {.line-numbers}
CREATE TABLE `t_article` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(200) DEFAULT NULL COMMENT '文章标题',
  `content` longtext COMMENT '文章内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
```

---

### 创建数据表`t_comment`

```sql {.line-numbers}
  CREATE TABLE `t_comment` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `content` longtext COMMENT '评论内容',
  `author` varchar(200) DEFAULT NULL COMMENT '评论作者',
  `a_id` int(20) DEFAULT NULL COMMENT '关联的文章id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
```

---

### 插入数据`t_article`

```sql {.line-numbers}
INSERT INTO `t_article` VALUES ('1', 'Spring Boot基础入门', '从入门到精通讲解...');
INSERT INTO `t_article` VALUES ('2', 'Spring Cloud基础入门', '从入门到精通讲解...');
```

---

### 插入数据`t_comment`

```sql {.line-numbers}
INSERT INTO `t_comment` VALUES ('1', '很全、很详细', '狂奔的蜗牛', '1');
INSERT INTO `t_comment` VALUES ('2', '赞一个', 'tom', '1');
INSERT INTO `t_comment` VALUES ('3', '很详细', 'kitty', '1');
INSERT INTO `t_comment` VALUES ('4', '很好，非常详细', '张三', '1');
INSERT INTO `t_comment` VALUES ('5', '很不错', '张杨', '2');
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `druid` Integration

>[alibaba/druid.](https://github.com/alibaba/druid)

---

1. `spring boot 2.x`默认的数据库连接池是`HikariCP`

---

```xml {.line-numbers}
<!-- itheima-springboot/pom.xml -->
<project>
    <properties>
        <druid-spring-boot-starter.version>1.2.11</druid-spring-boot-starter.version>
    </properties>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-starter</artifactId>
                <version>${druid-spring-boot-starter.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
```

---

```xml {.line-numbers}
<!-- chapter03/pom.xml -->
<project>
   <dependencies>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
        </dependency>
    </dependencies>
</project>
```

---

```properties {.line-numbers}
# druid
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.initialSize=20
spring.datasource.minIdle=10
spring.datasource.maxActive=100
```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class DataSourceConfig {
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getDruid(){
        return new DruidDataSource();
    }
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `mybatis` Integration

---

1. 引入依赖：引入`mysql`、`mybatis`依赖
1. 编写实体类
1. 配置数据库连接参数
1. 映射`mybatis`
    1. 注解方式
    1. 配置文件方式

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 引入依赖

---

```xml {.line-numbers}
<!-- itheima-springboot/pom.xml -->
<project>
    <properties>
        <mybatis-spring-boot-starter.version>2.2.2</mybatis-spring-boot-starter.version>
    </properties>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis-spring-boot-starter.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
```

---

```xml {.line-numbers}
<!-- chapter03/pom.xml -->
<project>
   <dependencies>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>
    </dependencies>
</project>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写实体类

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

public class Comment {
    private Integer id;
    private String content;
    private String author;
    private Integer aId;
    
    // omit getters and setters

    // omit toString
}
```

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

public class Article {
    private Integer id;
    private String title;
    private String content;
    private List<Comment> commentList;
    
    // omit getters and setters

    // omit toString
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编辑配置文件

---

```properties {.line-numbers}
# application.properties

# mysql
spring.datasource.url=jdbc:mysql://localhost:3306/springbootdata?serverTimezone=Asia/Shanghai&useSSL=false&useUnicode=true&characterEncoding=utf8
spring.datasource.username=root
spring.datasource.password=root
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 映射`mybatis`

---

#### `annotation-mode`

1. 创建并编写`Mapper`接口文件：`@Mapper`

---

```java {.line-numbers}
package com.itheima.mapper;

// omit imports

@Mapper
public interface CommentMapper {
    @Select("SELECT * FROM t_comment WHERE id =#{id}")
    Comment findById(Integer id);

    @Insert("INSERT INTO t_comment(content,author,a_id) " +
        "values (#{content},#{author},#{aId})")
    int insertComment(Comment comment);

    @Update("UPDATE t_comment SET content=#{content} WHERE id=#{id}")
    int updateComment(Comment comment);

    @Delete("DELETE FROM t_comment WHERE id=#{id}")
    int deleteComment(Integer id);
}
```

---

```java {.line-numbers}
package com.itheima.mapper;

// omit imports

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CommentMapperTest {

    @Autowired
    private CommentMapper commentMapper;

    @Test
    void findById() {
        assertThat(commentMapper.findById(1).toString(),
            is(equalTo("Comment{id=1, content='很全、很详细', author='狂奔的蜗牛', aId=1}")));
    }
}
```

---

#### `config-file-mode`

1. 创建`mapper`接口文件：`@Mapper`
1. 创建`xml`映射文件
1. 配置`xml`映射文件路径

---

```java {.line-numbers}
package com.itheima.mapper;

// omit imports

@Mapper
public interface ArticleMapper {
    Article selectArticle(Integer id);

    int updateArticle(Article article);
}
```

---

```xml {.line-numbers}
<mapper namespace="com.itheima.mapper.ArticleMapper">
    <select id="selectArticle" resultMap="articleWithComment">
        SELECT a.*, c.id AS c_id, c.content AS c_content, c.author, c.a_id AS c_aid
        FROM t_article AS a,
             t_comment AS c
        WHERE a.id = c.a_id
          AND a.id = #{id}
    </select>
    <resultMap id="articleWithComment" type="com.itheima.domain.Article">
        <id property="id" column="id"/>
        <result property="title" column="title"/>
        <result property="content" column="content"/>
        <collection property="commentList" ofType="com.itheima.domain.Comment">
            <id property="id" column="c_id"/>
            <result property="content" column="c_content"/>
            <result property="author" column="author"/>
            <result property="aId" column="c_aid"/>
        </collection>
    </resultMap>

    <update id="updateArticle" parameterType="com.itheima.domain.Article">
        UPDATE t_article
        <set>
            <if test="title !=null and title !=''">
                title=#{title},
            </if>
            <if test="content !=null and content !=''">
                content=#{content}
            </if>
        </set>
        WHERE id=#{id}
    </update>
</mapper>
```

---

```properties {.line-numbers}
# application.properties

mybatis.mapper-locations=classpath:mapper/*.xml
```

---

```java {.line-numbers}
package com.itheima.mapper;

// omit imports

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ArticleMapperTest {

    @Autowired
    private ArticleMapper articleMapper;

    @Test
    void selectArticle() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Article{");
        stringBuilder.append("id=1, title='Spring Boot基础入门', content='从入门到精通讲解...'");
        stringBuilder.append(", ");
        stringBuilder.append("commentList=[");
        stringBuilder.append("Comment{id=1, content='很全、很详细', author='狂奔的蜗牛', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=2, content='赞一个', author='tom', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=3, content='很详细', author='kitty', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=4, content='很好，非常详细', author='张三', aId=1}");
        stringBuilder.append("]");
        stringBuilder.append("}");
        String expected = stringBuilder.toString();
        assertThat(articleMapper.selectArticle(1).toString(), is(equalTo(expected)));
    }
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring data jpa` Integration

>1. [tutorialspoint. Spring Boot JPA Tutorial.](https://www.tutorialspoint.com/spring_boot_jpa/index.htm)

---

1. `jpa(Java Persistence API)`：是一种`orm`规范，`jpa`定义接口规范和`jpql(Java Persistence Query Language)`
1. `spring data jpa`是对`jpa`的进一步封装，底层使用`hibernate`框架（`jpa`的一个实现）
1. `spring data jpa`支持多种方式与数据库交互
    1. 方法名关键字自动生成查询语句：[spring.io. Repository query keywords.](https://docs.spring.io/spring-data/jpa/docs/2.7.2/reference/html/#repository-query-keywords)
    1. `jpql`：`@Query`
    1. `native sql`：`@Query(nativeQuery = true)`
    1. `QBE(Query By Example)`：[spring.io. Query by Example.](https://docs.spring.io/spring-data/jpa/docs/2.7.2/reference/html/#query-by-example)

---

### `spring data jpa`的基本使用步骤

1. 添加依赖
1. 编写实体类：实现实体类与数据表的映射关系
1. 编写自定义接口（继承`Repository`接口）：实现数据操作方法

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 添加`spring-boot-starter-data-jpa`依赖

---

```xml {.line-numbers}
<!-- chapter03/pom.xml -->
<project>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
    </dependencies>
</project>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`jpa`实体类

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

@Entity(name = "t_comment")
public class Discuss {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String content;
    private String author;
    @Column(name = "a_id")
    private Integer aId;

    // omit getters and setters
    
    // omit toString
}
```

---

1. `@Entity`：类，与数据表相对应，当首字母小写后的类名与数据表相同时可省略
1. `@Column`：属性或`getter`，与数据字段相对应，当属性名与字段名相同时可省略
1. `@Id`：属性，与数据表主键相对应
1. `@GenerateValue`：成员变量，自动生成策略

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`jpa-repository`接口

---

```java {.line-numbers}
org.springframework.data.repository.query.QueryByExampleExecutor<T>
    org.springframework.data.jpa.repository.JpaRepository<T,ID>

org.springframework.data.repository.Repository<T,ID>
    org.springframework.data.repository.CrudRepository<T,ID>
        org.springframework.data.repository.PagingAndSortingRepository<T,ID>
            org.springframework.data.jpa.repository.JpaRepository<T,ID>
```

---

```java {.line-numbers}
package com.itheima.repository;

// omit imports

public interface DiscussRepository extends JpaRepository<Discuss, Integer> {

    List<Discuss> findByAuthorNotNull();

    @Query("SELECT c FROM t_comment c WHERE  c.aId = ?1")
    List<Discuss> getDiscussPaged(Integer aid, Pageable pageable);

    @Query(value = "SELECT * FROM t_comment  WHERE  a_Id = ?1", nativeQuery = true)
    List<Discuss> getDiscussPaged2(Integer aid, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE t_comment c SET c.author = ?1 WHERE  c.id = ?2")
    int updateDiscuss(String author, Integer id);

    @Transactional
    @Modifying
    @Query("DELETE t_comment c WHERE  c.id = ?1")
    int deleteDiscuss(Integer id);
}
```

---

1. `@Query`
1. `@Modifying`
1. `@Transactional`
1. `@Rollback`
1. `xxRepository<T, ID>`：`T`代表实体类，`ID`代表主键数据类型

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 测试`jpa`整合

---

```java {.line-numbers}
package com.itheima.repository;

// omit imports

@SpringBootTest
@ExtendWith(SpringExtension.class)
class DiscussRepositoryTest {

    @Autowired
    private DiscussRepository repository;

    @Test
    void findById() {
        assertThat(repository.findById(1).isPresent(),is(true));
    }

    @Test
    @Modifying
    @Transactional
    @Rollback(true)
    void deleteDiscuss() {
        assertThat(repository.deleteDiscuss(3), is(equalTo(1)));
    }
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `redis` Integration

---

1. `redis`是一种内存数据库，也提供了额外的持久化方案
1. `redis`主要应用于`memory cache`，也可以应用于`message queue`
1. `redis`的优点：
    1. 存取速度快
    1. 数据类型丰富
    1. 操作原子性

---

### `redis`整合步骤

1. 添加依赖
1. 编写实体类
1. 编写`Repository`接口
1. 配置连接参数

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 添加`spring-boot-starter-data-redis`依赖

---

```xml {.line-numbers}
<!-- chapter03/pom.xml -->
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>
</dependencies>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`redis`实体类

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

@RedisHash("persons")
public class Person {
    @Id
    private String id;
    @Indexed
    private String firstname;
    @Indexed
    private String lastname;
    private Address address;
    private List<Family> familyList;

    // omit getters and setters
    // omit toString
}
```

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

public class Address {
    @Indexed
    private String city;
    @Indexed
    private String country;

    // omit getters and setters
    // omit toString
}
```

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

public class Family {
    @Indexed
    private String type;
    @Indexed
    private String username;

    // omit getters and setters
    // omit toString
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写`redis-repository`接口

---

```java {.line-numbers}
package com.itheima.repository;

// omit imports

public interface PersonRepository extends CrudRepository<Person, String> {
    List<Person> findByLastname(String lastname);

    Page<Person> findPersonByLastname(String lastname, Pageable page);

    List<Person> findByFirstnameAndLastname(String firstname, String lastname);

    List<Person> findByAddress_City(String city);

    List<Person> findByFamilyList_Username(String username);
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 配置`redis`连接参数

---

```properties {.line-numbers}
# application.properties

spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.password=
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 测试`redis`整合

---

```java {.line-numbers}
package com.itheima.repository;

// omit imports

@SpringBootTest
@ExtendWith(SpringExtension.class)
class PersonRepositoryTest {
    @Autowired
    private PersonRepository repository;

    @BeforeEach
    void setUp() {
        Person person = new Person("张", "有才");
        person.setAddress(new Address("北京", "China"));
        List<Family> list = new ArrayList<>();
        list.add(new Family("父亲", "张良"));
        list.add(new Family("母亲", "李香君"));
        person.setFamilyList(list);
        Person person2 = new Person("James", "Harden");
        repository.save(person);
        repository.save(person2);
    }

    @AfterEach
    void tearDown() {
        this.repository.deleteAll();
    }

    @Test
    void findByAddress_City() {
        assertThat(repository.findByAddress_City("北京").size(), is(equalTo(1)));
    }

    @Test
    public void updatePerson() {
        Person person = repository.findByFirstnameAndLastname("张", "有才").get(0);
        person.setLastname("小明");
        assertThat(repository.save(person).getLastname(), is(equalTo("小明")));
    }

    @Test
    public void deletePerson() {
        repository.delete(repository.findByFirstnameAndLastname("张", "有才").get(0));
        assertThat(repository.findByFirstnameAndLastname("张", "有才").size(), is(equalTo(0)));
    }
}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
