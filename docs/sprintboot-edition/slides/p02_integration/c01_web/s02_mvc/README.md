---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Spring Boot Web_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Spring Boot Web

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [学习目标](#学习目标)
2. [`Spring MVC`](#spring-mvc)
3. [`spring boot`整合`java web`三大件](#spring-boot整合java-web三大件)
4. [文件上传下载](#文件上传下载)
5. [`spring boot`应用的打包与部署](#spring-boot应用的打包与部署)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 学习目标

---

1. 掌握`Spring Boot`整合`Spring MVC`
1. 掌握`Spring Boot`整合`Servlet`三大组件
1. 掌握`Spring Boot`的文件上传下载功能实现
1. 掌握`Spring Boot`的打包和部署

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring MVC`

---

1. `Spring Application`实现`MVC`的一种方法
1. `Spring MVC`从一开始就包含在`Spring Framework`中，正式名称是`Spring Web MVC`
1. `Spring MVC`基于`servlet`，对`servlet api`进行封装
    - `servlet container`将请求转发到`DispatcherServlet`，并调用相应的`controller`（也称为"后置控制器"）
    - `DispatcherServlet`是`Spring MVC`中唯一的内置`servlet`，也称为"`front-controller前置控制器`"
    - `DispatcherServlet`在`servlet container`中，不在`Spring IoC container`中

<!-- 1. `Model interface`, `@Controller`和`view technologies` -->
<!-- 1. `Spring MVC`是阻塞式的 -->

---

1. `handler`是一个术语，不是一个`class`或`interface`
1. `controller`一定是`handler`，`handler`不一定是`controller`

---

![height:540](.assets/image/mvc.png)

>[spring.io. The DispatcherServlet.](https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mvc.html#mvc-servlet)

---

![height:540](.assets/image/flow-of-spring-web-mvc.png)

>[javatpoint.com. Understanding the flow of Spring Web MVC.](https://www.javatpoint.com/spring-mvc-tutorial)

---

![width:1120](.assets/image/spring-springframework-mvc-5.png)

---

### `Spring MVC`常用注解

1. `@Controller`：`class`注解，标识该类为控制器类
1. `@RequestMapping`：`class`和`method`注解，映射请求路径和对应的处理器（可以处理`get`请求和`post`请求）
    1. `@PostMapping`：`method`注解，映射请求路径和对应的处理器（只能处理`post`请求）
    1. `@GetMapping`：`method`注解，映射请求路径和对应的处理器（只能处理`get`请求）

---

1. `get`：请求数据体现在`url`上，请求数据的长度有一定限制（`http`协议本身没有做限制，`browser`和`server`进行了限制）
1. `post`：请求数据不会体现在`url`上，体现在`http body`上，请求数据的长度较大（`server`进行配置限制大小）

---

>Browsers do support PUT and DELETE, but it is HTML that doesn't.
>
>This is because HTML 4.01 and **the final W3C HTML 5.0 spec** both say that the only HTTP methods that their form elements should allow are GET and POST.
>>[Why don't browsers support PUT and DELETE requests ?](https://programmertoday.com/why-dont-browsers-support-put-and-delete-requests/)

---

### `spring boot`整合`spring mvc`的自动配置

1. 引入`spring-boot-starter-web`依赖后，`WebMvcAutoConfiguration`实现的默认配置在启动后即生效，一般情况下使用默认配置即可

<!--

1. 无需配置`DispatcherServlet`

-->

<!-- ---

1. 内置视图解析器`ContentNegotiatingViewResolver`和`BeanNameViewResolver`
1. 支持静态资源以及`WebJars`
1. 自动注册了转换器和格式化器
1. 支持`http`消息转换器
1. 自动注册了消息代码解析器
1. 支持静态项目首页`index.html`
1. 支持定制应用图标`favicon.ico`
1. 自动初始化Web数据绑定器`ConfigurableWebBindingInitializer` -->

---

### `spring boot`整合`spring mvc`

1. 引入依赖
1. 配置视图管理器
1. 编写并配置拦截器

---

#### 引入依赖

---

```xml {.line-numbers}
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

---

#### 编写视图管理器

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class MyMVCconfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/toLoginPage").setViewName("login");
        registry.addViewController("/login.html").setViewName("login");
    }
```

---

- access <http://localhost:8085/toLoginPage> and <http://localhost:8085/login.html>

---

#### 编写拦截器

---

1. `preHandle`：`/admin*`未登录情况下重定向到`/toLoginPage`
1. `postHandle`：对`currentYear`进行赋值

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Component
public class MyInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception 
    { 
        String uri = request.getRequestURI();
        Object loginUser = request.getSession().getAttribute("loginUser");
        if (uri.startsWith("/admin") && null == loginUser) {
            response.sendRedirect("/toLoginPage");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        request.setAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse
        response, Object handler, @Nullable Exception ex) throws Exception {
    }
}
```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class MyMVCconfig implements WebMvcConfigurer {
    @Autowired
    private MyInterceptor myInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(myInterceptor)
            .addPathPatterns("/**")
            .excludePathPatterns("/login.html");
    }
}
```

---

1. `excludePathPatterns("/login.html")`

---

1. `mvn spring-boot:run`
1. access:
    1. <http://localhost:8085/admin>
    1. <http://localhost:8085/toLoginPage.html>
    1. <http://localhost:8085/login.html>：注意观察页面的年份信息

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring boot`整合`java web`三大件

>1. [WEB三大组件-servlet.](https://zhuanlan.zhihu.com/p/138438747)

---

1. `servlet`：处理客户端的请求
    1. 接收请求数据
    1. 处理请求
    1. 返回响应
1. `filter`：`Servlet 2.3`新增，过滤客户端的请求
1. `listener`：`Servlet 2.3`新增，监听事件，事件触发处理
    1. `HttpSessionListener`
    1. `ServletRequestListener`
    1. `ServletContextListener`

---

1. `javax.servlet`：独立于具体协议的`class`和`interface`
1. `javax.servlet.http`：具体于`http`协议的`class`和`interface`

>`javax(java extension)`/`jakarta`，`JavaEE`/`JakartaEE`的`namespace`
>>[百度百科. javax.](https://baike.baidu.com/item/javax/6413567)

---

1. 组件注册方式/配置方式
    1. `Servlet`: `ServletRegistrationBean`
    1. `Filter`: `FilterRegistrationBean`
    1. `Listener`: `ServletListenerRegistrationBean`
1. 路径扫描方式/注解方式
    1. `servlet`：`@WebServlet(...)`
    1. `filter`：`@WebFilter({...})`
    1. `listener`：`@WebListener`
    1. `main class`: `@ServletComponentScan`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `filter` VS `interceptor`

>1. [HandlerInterceptors vs Filters in Spring MVC. 2021-05-25.](https://www.baeldung.com/spring-mvc-handlerinterceptor-vs-filter)
>    - [code](https://github.com/eugenp/tutorials/tree/master/spring-boot-modules/spring-boot-mvc-3)
>1. [Filter vs. Interceptor in Spring Boot. 2021-11-25.](https://senoritadeveloper.medium.com/filter-vs-interceptor-in-spring-boot-2e49089f682e)
>1. [过滤器 和 拦截器 6个区别. 2020-06-04.](https://segmentfault.com/a/1190000022833940)

---

![height:560](.assets/image/0*c3FgKRFyt4DEaQhM.gif)

>[Filter vs. Interceptor in Spring Boot. 2021-11-25.](https://senoritadeveloper.medium.com/filter-vs-interceptor-in-spring-boot-2e49089f682e)

---

![width:1120](.assets/image/filters_vs_interceptors.jpg)

>[HandlerInterceptors vs. Filters in Spring MVC.](https://www.baeldung.com/spring-mvc-handlerinterceptor-vs-filter)

---

1. `browser` --> `servlet container` --> `filter` --> `servlet`(`DispatcherServlet`) --> `interceptor` --> `controller`
1. `filters` are part of the `webserver` and not the Spring framework. For incoming requests, we can use filters to manipulate and even block requests from reaching any servlet. Vice versa, we can also block responses from reaching the client.
1. `interceptors` are part of the Spring MVC framework and sit between the DispatcherServlet and our Controllers. We can intercept requests before they reach our controllers, and before and after the view is rendered.

---

#### `filter`

>1. [Java Servlet中Filter过滤器的原理以及使用方式. 2021-08-27.](https://juejin.cn/post/7000950677409103880)

---

```java {.line-numbers}
public interface Filter {

    default public void init(FilterConfig filterConfig) throws ServletException {}

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException;

    default public void destroy() {}
}
```

---

1. `init()`: invoked only once. It is used to initialize the filter.
1. `doFilter()`: invoked every time a user send a request to any resource, to which the filter is mapped. It is used to perform filtering tasks.
    1. before `chain.doFilter(request, response);`: request direction
    1. `chain.doFilter(request, response);`: allow access, if don't call this method will forbid access
    1. after `chain.doFilter(request, response);`: response direction
1. `destroy()`: invoked only once when filter is taken out of the service.

---

1. `born`：`filter`的实例是在应用被加载时就完成的实例化，并调用`init()`初始化，该方法只会被调用一次。在`filter`执行处理之前，`init()`必须成功完成
1. `alive`：与应用的存活期相同，`filter`在内存中是单例的，针对过滤范围内的资源，每次访问都会调用`doFilter()`进行处理
1. `dead`：应用被卸载时，此时会调用`destroy()`，该方法只会被调用一次

---

1. `client`发送请求到`server`，`server`根据该请求`url`找到对应的`filter`形成`filter-chain`
1. 从第一个`filter`开始进行处理，即，调用`doFilter()`
1. 当请求满足当前`filter`的要求或者是过滤操作完成之后，应在调用`chain.doFilter()`进行放行，该方法调用`filter-chain`中的下一个`filter`的`doFilter()`继续处理
1. 如果当前`filter`是`filter-chain`的最后一个`filter`，那么`chain.doFilter()`方法将会执行资源访问操作，访问完毕之后，将会依照最开始`filter`的调用顺序倒序的返回，接着执行`chain.doFilter()`方法后面的代码
1. 将响应结果提交给`server`，`server`再将响应返回给`client`

---

#### `interceptor`

1. `preHandle()`: executed before the target handler is called
1. `postHandle()`: executed after the target handler but before the DispatcherServlet renders the view
1. `afterCompletion()`: callback after completion of request processing and view rendering

---

1. 相同点：都起到过滤/预处理功能，都是链式结构
1. 不同点：
    1. `filter`属于`servlet container`，`interceptor`属于`Spring MVC`
    1. `filter`在`DispatcherServlet`之前，`interceptor`在`DispatcherServlet`之后
    1. `filter`能处理所有进入`servlet container`的请求，`interceptor`只处理对`@Controller`和`static/`下的请求（❗ 待进一步确认 ❗）

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 组件注册方式整合`java web`三大件

---

#### 编写`servlet`

---

```java {.line-numbers}
package com.itheima.servletComponent;

// omit imports

@Component
public class MyServletComponent extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        this.doPost(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.getWriter().write("hello MyServletComponent...");
    }
}
```

---

#### 编写`filter`

---

```java {.line-numbers}
package com.itheima.servlet.component;

// omit imports

@Component
public class MyFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        System.out.println("hello MyFilter...");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
```

---

#### 编写`listener`

---

```java {.line-numbers}
package com.itheima.servlet.component;

// omit imports

@Component
public class MyListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("contextInitialized ...");
    }
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("contextDestroyed ...");
    }
}
```

---

#### 注册`servlet`、`filter`、`listener`

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class ServletConfig {
    @Bean
    public ServletRegistrationBean getServlet(MyServlet servlet) {
        ServletRegistrationBean registrationBean =
            new ServletRegistrationBean(servlet, "/servlet/component/my-servlet");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean getFilter(MyFilter filter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(filter);
        registrationBean.setUrlPatterns(Arrays.asList("/toLoginPage", "/servlet/component/my-filter"));
        return registrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean getServletListener(MyListener listener) {
        ServletListenerRegistrationBean registrationBean = new ServletListenerRegistrationBean(listener);
        return registrationBean;
    }
}
```

---

#### 运行

1. `mvn spring-boot:run`
1. access through the browser
    1. <http://localhost:8085/toLoginPage>
    1. <http://localhost:8085/servlet/component/my-servlet>
    1. <http://localhost:8085/servlet/component/my-filter>
1. check console output

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 路径扫描方式整合`java web`三大件

---

1. `com.itheima.servlet.annotation`
1. access through the browser
    1. <http://localhost:8085/servlet/annotation/my-servlet>
    1. <http://localhost:8085/servlet/annotation/my-filter>
1. check console output

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件上传下载

>[spring.io. Handling Multipart File Uploads.](https://docs.spring.io/spring-boot/docs/2.7.2/reference/htmlsingle/#howto.spring-mvc.multipart-file-uploads)

---

```properties {.line-numbers}
# post limit, default is 2M
server.tomcat.max-http-form-post-size=-1
# 单个上传文件大小限制（默认1MB）
spring.servlet.multipart.max-file-size=10MB
# 总上传文件大小限制（默认10MB）
spring.servlet.multipart.max-request-size=50MB

# upload location, change to your environment location
file.upload.location=/users/roamwonder/downloads/spring-test/upload/
# download location, change to your environment location
file.download.location=/users/roamwonder/downloads/spring-test/download/
```

---

1. 部署到外置的`tomcat`时，上传下载的文件可以直接放置在应用的目录中
1. 使用嵌入式`tomcat`时，上传下载的文件应放置在应用之外的目录中

---

### 文件上传

1. 一般通过`html form`以`post`形式上传至服务器

---

1. 编写文件上传页面：`templates/upload.html`
1. 编写文件上传类：`com.itheima.controller.file.UploadController`
1. access: <http://localhost:8085/file/upload>

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 文件下载

---

1. 引入依赖：`commons-io:commons-io`
1. 编写文件下载页面：`templates/download.html`
1. 编写文件下载类：`com.itheima.controller.file.DownloadController`
1. 非英文文件名处理：`com.itheima.controller.file.FileControllerUtils.getFilename()`
1. access: <http://localhost:8085/file/download>

---

#### 引入`commons-io`依赖

```xml {.line-numbers}
<properties>
    <commons-io.version>2.11.0</commons-io.version>
</properties>

<dependencies>
    <dependency>
        <groupId>commons-io</groupId>
        <artifactId>commons-io</artifactId>
        <version>${commons-io.version}</version>
    </dependency>
</dependencies>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring boot`应用的打包与部署

---

1. `jar`
1. executable `jar`
1. `war`
1. `container image`

---

```xml {.line-numbers}
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `jar`

---

```bash {.line-numbers}
mvn package

java -jar target/${jar-filename}
```

---

1. `lib`：依赖
1. `classes`：项目`class`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Executable `jar`

---

```xml {.line-numbers}
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <executable>true</executable>
            </configuration>
        </plugin>
    </plugins>
</build>
```

---

```bash {.line-numbers}
$ mvn package

$ ls -l target | grep jar

-rwxr--r--

$ target/${jar-filename}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `war`

---

1. `packaging`修改为`war`
1. 引入`spring-boot-starter-tomcat`依赖
1. `SpringBootServletInitializer`：原`web.xml`的替代

>[Spring中WebApplicationInitializer的理解.](https://blog.csdn.net/zq17865815296/article/details/79464403)

---

```xml {.line-numbers}
<packaging>war</packaging>

<dependencies>
    <!-- external tomcat -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-tomcat</artifactId>
        <scope>provided</scope>
    </dependency>
</dependencies>
```

---

```java {.line-numbers}
package com.itheima;

// omit imports

@ServletComponentScan
@SpringBootApplication
public class Chapter05Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Chapter05Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Chapter05Application.class, args);
    }
}
```

---

```bash {.line-numbers}
mvn package

cp target/${war-file} ${tomcat_home}/webapps/

${tomcat_home}/bin/startup.sh
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `container image`

---

```xml {.line-numbers}
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <image>
                    <name>arm/${project.artifactId}</name>
                </image>
            </configuration>
        </plugin>
    </plugins>
</build>
```

---

```bash {.line-numbers}
mvn spring-boot:build-image

docker container run --rm -d -p 8085:8085 <spring-boot-app>
```

---

1. [spring.io. Spring Boot with Docker.](https://spring.io/guides/gs/spring-boot-docker/)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
