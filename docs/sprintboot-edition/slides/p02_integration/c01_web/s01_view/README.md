---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Spring Boot View_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Spring Boot视图层技术Spring Boot View

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [学习目标](#学习目标)
2. [`spring boot`支持的视图技术](#spring-boot支持的视图技术)
3. [`spring boot`整合`thymeleaf`](#spring-boot整合thymeleaf)
4. [`thymeleaf`基本语法](#thymeleaf基本语法)
5. [静态资源与国际化](#静态资源与国际化)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 学习目标

---

1. 熟悉`thymeleaf`模板引擎基本语法
1. 熟悉`spring boot`模板配置
1. 掌握`spring boot`静态资源映射与国际化功能
1. 掌握`spring boot`整合`thymeleaf`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring boot`支持的视图技术

---

1. 模板引擎为服务端应用程序提供一种生成最终页面的方法，这些页面将在终端用户的`browser`上显示和执行
1. 视图技术通常提供以下功能
    1. 一种模板语言或一组标记：定义模板引擎如何使用输入数据生成输出结果
    1. 一个视图解析器：将逻辑视图名解析为具体的视图对象

---

![width:1120](./.assets/image/freemarker_overview.png)

---

1. 🌟 `Thymeleaf`: <https://www.thymeleaf.org/>
1. `Apache Velocity`: <https://velocity.apache.org/>
1. `Apache FreeMaker`: <https://freemarker.apache.org/>
1. `Apache Groovy Markup`: <http://groovy-lang.org/templating.html>
1. `Mustache`: <https://github.com/spullara/mustache.java>
1. `...`

---

### `Thymeleaf`的优势

1. 官方推荐：`Thymeleaf`是`Spring Boot`官方推荐的视图层技术
1. 整合良好：`Thymeleaf`为`Spring MVC`和`Spring WebFlux`提供了良好的支持
1. 离线查看：`Thymeleaf`使用`natural templates`，可以静态页面言式直接打开和查看

>Thymeleaf is a modern server-side Java template engine for both web and standalone environments.
>><https://www.thymeleaf.org/>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring boot`整合`thymeleaf`

---

1. 引入`Thymeleaf`依赖
1. 配置`Thymeleaf`属性
1. 创建并编写控制类`@Controller`
1. 创建并编写模板页面
1. 添加静态资源和国际化

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 引入`Thymeleaf`依赖

---

```xml {.line-numbers}
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 配置`Thymeleaf`属性

---

```properties {.line-numbers}
# application.properties
spring.thymeleaf.cache=false
spring.thymeleaf.encoding=UTF-8
spring.thymeleaf.mode=HTML5
spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 创建并编写控制类`@Controller`

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@Controller
public class LoginController {

    @GetMapping("/toLoginPage")
    public String toLoginPage(Model model) {
        model.addAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        return "login";
    }
}
```

---

1. 🌟 `@GetMapping`注解的方法返回一个`String`，该值加上`suffix`是模板文件名
1. 🌟 添加进`model`的属性可以被`template engine`访问（即`model`对象将被自动传递给`template engine`）
1. 默认情况下，模板文件位于`src/main/resources/templates/`（即：`classpath:/templates/`）

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 创建并编写模板页面

---

```html {.line-numbers}
<!-- login.html -->
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1,shrink-to-fit=no" name="viewport">
    <title>用户登录界面</title>
    <link rel="stylesheet" th:href="@{/login/css/bootstrap.min.css}">
    <link rel="stylesheet" th:href="@{/login/css/signin.css}">
</head>
```

---

```html {.line-numbers}
<p class="mt-5 mb-3 text-muted">&copy;
    <span th:text="${currentYear}">{current year}</span>
    -
    <span th:text="${currentYear}+1">{next year}</span>
</p>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 添加静态资源和国际化

---

#### 语言文件

---

```properties {.line-numbers}
# application.properties
spring.messages.basename=i18n.login
```

---

```properties {.line-numbers}
# i18n/login.properties && i18n/login_zh_CN.properties
login.tip=请登录
login.username=用户名
login.password=密码
login.rememberme=记住我
login.button=登录
```

---

```properties {.line-numbers}
# i18n/login_en_US.properties
login.tip=Please sign in
login.username=Username
login.password=Password
login.rememberme=Remember me
login.button=Login
```

---

#### `locale`解析器

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class MyLocalResovel implements LocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        String l = httpServletRequest.getParameter("l");
        String header = httpServletRequest.getHeader("Accept-Language");
        Locale locale=null;
        if(!StringUtils.isEmpty(l)){
            String[] split = l.split("_");
            locale=new Locale(split[0],split[1]);
        }else {
            // Accept-Language: en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7
            String[] splits = header.split(",");
            String[] split = splits[0].split("-");
            locale=new Locale(split[0],split[1]);
        }
        return locale;
    }
    @Override
    public void setLocale(HttpServletRequest httpServletRequest, @Nullable
            HttpServletResponse httpServletResponse, @Nullable Locale locale) {
    }
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocalResovel();
    }
}
```

---

```html {.line-numbers}
<h1 class="h3 mb-3 font-weight-normal" th:text="#{login.tip}">请登录</h1>
<input autofocus="" class="form-control"
        required="" th:placeholder="#{login.username}" type="text">
<input class="form-control" required=""
        th:placeholder="#{login.password}" type="password">
<div class="checkbox mb-3">
    <label>
        <input type="checkbox" value="remember-me"> [[#{login.rememberme}]]
    </label>
</div>
<button class="btn btn-lg btn-primary btn-block" th:text="#{login.button}" type="submit">登录</button>
```

---

```html {.line-numbers}
<a class="btn btn-sm" th:href="@{/toLoginPage(l='zh_CN')}">中文</a>
<a class="btn btn-sm" th:href="@{/toLoginPage(l='en_US')}">English</a>
```

---

1. <http://localhost:8080/toLoginPage>
1. `<right click> >>> inspect >>> network`
1. reload page

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `thymeleaf`基本语法

>1. [玩转 SpringBoot 2 快速整合 | Thymeleaf 篇.](https://zhuoqianmingyue.blog.csdn.net/article/details/90690542)
>1. [史上最详 Thymeleaf 使用教程.](https://blog.csdn.net/ljk126wy/article/details/90735989)
>    1. <https://github.com/zhuoqianmingyue/spring-boot-examples/tree/master/spring-boot-2.x-thymeleaf>

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `thymeleaf`常用标签

---

```html {.line-numbers}
<!DOCTYPE html>
<!-- namespace -->
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <!-- th:href -->
    <link rel="stylesheet" type="text/css" media="all"
          href="../../css/gtvg.css" th:href="@{/css/gtvg.css}" />
    <title>Title</title>
</head>
<body>
    <!-- th:text -->
    <p th:text="#{hello}">hello thymeleaf...</p>
</body>
</html>
```

---

1. 声明`Thymeleaf`名字空间：`<html xmlns:th="http://www.thymeleaf.org">`

---

tag|description
:--|:--
`th:insert`|页面片段包含（类似JSP中的include标签）
`th:replace`|页面片段包含（类似JSP中的include标签）
`th:each`|元素遍历（类似JSP中的c:forEach标签）
`th:if`|条件判断，如果为真
`th:unless`|条件判断，如果为假
`th:switch`|条件判断，进行选择性匹配
`th:case`|条件判断，进行选择性匹配
`th:object`|变量声明
`th:with`|变量声明

---

tag|description
:--|:--
`th:attr`|通用属性修改
`th:attrprepend`|通用属性修改，将计算结果追加前缀到现有属性值
`th:attrappend`|通用属性修改，将计算结果追加后缀到现有属性值
`th:value`|属性值修改，指定标签属性值
`th:href`|用于设定链接地址
`th:src`|用于设定链接地址

---

tag|description
:--|:--
`th:text`|用于指定标签显示的文本内容
`th:utext`|用于指定标签显示的文本内容，对特殊标签不转义
`th:fragment`|声明片段
`th:remove`|移除片段

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `thymeleaf`表达式

---

expression|description
:--|:--
`${...}`|变量表达式
`*{...}`|选择表达式
`#{...}`|消息表达式
`@{...}`|链接表达式
`~{...}`|片段表达式

---

#### 内置对象Internal Objects

|object|description|
|:--|:--|
|`#ctx`|上下文对象|
|`#vars`|上下文变量|
|`#locale`|上下文区域设置|
|`#request`|（`Web`）`HttpServletRequest`对象|
|`#response`|（`Web`）`HttpServletResponse`对象|
|`#session`|（`Web`）`HttpSession`对象|
|`#servletContext`|（`Web`）`ServletContext`对象|

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 静态资源与国际化

---

### 静态资源

1. 对于`compiler`或`web container`或`backend engine`不需要解析处理的资源
    1. `browser`能直接处理的资源：`html`, `css`, `javascript`
    1. 供用户下载的资源：`word`, `pdf`等
    1. 其他不需要后端解析处理的资源

---

1. 静态资源查找路径及优先级
    1. `classpath:/META-INF/resources/`
    1. `classpath:/resources`
    1. 🌟 `classpath:/resources/static/`(`src/main/resources/static`)
    1. `classpath:/resources/public/`(`src/main/resources/public`)

---

### 国际化

1. `i18n(internationalization)`
1. `spring boot`默认将`classpath:/resources/messages.properties`为默认语言文件，其他语言文件名为`默认语言文件名_语言代码_国家代码.properties`
1. 自定义语言配置文件路径时在全局配置文件中配置`spring.messages.basename`属性

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
