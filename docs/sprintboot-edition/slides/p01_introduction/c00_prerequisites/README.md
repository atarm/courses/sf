---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Prerequisites of SpringBoot_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Prerequisites of SpringBoot

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Maven](#maven)
2. [Java Reflection and Annotation](#java-reflection-and-annotation)
3. [Spring Framework](#spring-framework)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Maven

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Java Reflection and Annotation

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Reflection

---

1. `reflection`: allows an **_executing Java program_** to **_examine or "introspect"_** upon itself, and manipulate internal properties of the program
1. `public final class Class<T> extends Object implements Serializable, GenericDeclaration, Type, AnnotatedElement, TypeDescriptor.OfField<Class<?>>, Constable`

>1. [oracle.com. Using Java Reflection.](https://www.oracle.com/technical-resources/articles/java/javareflection.html)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Annotation

---

1. `annotation`: a form of syntactic **_metadata_** that can be added to **_Java source code_**
1. 从处理者的角度看，分为三类`annotation`
    1. `compiler annotation`: 由编译器使用的注解，这类注解不会被编译进`.class`文件
    1. `loader annotation`: 由加载器等底层工具使用的注解，这类注解会被编译进`.class`文件，但加载结束后不存在于`JVM`中
    1. `application annotation`/`@Retention(value=RUNTIME)`: 由应用程序运时时使用的注解，加载后一直存在于`JVM`中

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Spring Framework

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### IoC

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### AOP

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
