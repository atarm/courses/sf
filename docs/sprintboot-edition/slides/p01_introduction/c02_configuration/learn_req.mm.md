# configuration

## config file and properties

### config file

#### global config file

1. location
1. format
    1. `.properties`
    1. `.yml`
1. how to load: auto load when startup

#### custom config file

1. location
1. format
    1. `.properties`
    1. `.yml`
1. how to load
    1. `@PropertySource`: load `.properties` or `.yml`
    1. `@ImportResource`: load `.xml`
    1. `@Configuration`: `configuration class`, means config in `class` not in `file`

### properties injection

#### pre-defined properties

1. auto inject

#### custom properties

1. `@ConfigurationProperties`: batch injection
1. `@Value`: single injection

## multi environment

1. how to config
    1. profile file
    1. profile annotation
1. how to activate
    1. `spring.profiles.active` property in global config file
    1. `spring.profiles.active` argument when start application

## misc

1. random: `${random.xxx}`
1. variable/placeholder: `${xxx}`
