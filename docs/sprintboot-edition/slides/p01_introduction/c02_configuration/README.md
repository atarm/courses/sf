---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Configuration_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Configuration

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [学习目标Goals](#学习目标goals)
2. [配置文件与属性注入](#配置文件与属性注入)
3. [多环境配置](#多环境配置)
4. [随机数与变量引用](#随机数与变量引用)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 学习目标Goals

1. 熟悉全局配置文件以及自定义配置方法
1. 掌握配置文件属性注入
1. 掌握`profile`多环境配置
1. 了解随机值和变量引用

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 配置文件与属性注入

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 全局配置文件

>1. [Spring.io. Externalized Configuration.](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config)

---

1. `src/main/resources/`
    1. `application.properties`
    1. `application.yaml`

>1. [spring.io. Common Application Properties.](https://docs.spring.io/spring-boot/docs/2.7.2/reference/html/application-properties.html)
>1. [Spring.io. External Application Properties.](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config.files)

---

#### `application.properties`

```properties {.line-numbers}
# application.properties

server.port=8443
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
```

---

#### `application.yaml`

>1. [yaml cheat-sheet.](https://quickref.me/yaml)

```yaml {.line-numbers}
# application.yaml

server:
    prot: 8443

spring:
    datasource:
        driver-class-name: com.mysql.jdbc.Driver
```

---

![height:520](./.assets/image/spring-boot-yaml-example.png)

>[Spring Boot YAML example.](https://mkyong.com/spring-boot/spring-boot-yaml-example/)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 配置文件属性注入

---

1. 属性注入：
    1. `spring`本身的配置属性：自动注入
    1. 用户自定义的配置属性：自定义注入
1. `IDE`提示：
    1. `spring`内置的配置元素：由依赖包的`xxx-metadata.json`提供`lint`
    1. 自定义的配置元素：添加`spring-boot-configuration-processor`提供`lint`

>1. [spring-boot-configuration-processor 的作用.](https://www.jianshu.com/p/ca22783b0a35)

---

```xml {.line-numbers}
<!-- pom.xml -->
<!-- configuration lint support -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
```

---

#### `@ConfigurationProperties`属性注入

---

```java {.line-numbers}
@Component
@ConfigurationProperties(prefix = "person")
public class Person {
    private int id;
    public void setId(int id) {
        this.id = id;
    }
}
```

<!--❗ 配置文件的属性名与类的成员名应一致 ❗-->

---

```java {.line-numbers}
package com.itheima.domain;

public class Pet {
    private String type;
    private String name;

    // omit getters and setters
    // omit toString(), equals() and hashCode()
}
```

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

@Component
@ConfigurationProperties(prefix = "person")
public class Person {
    private int id;
    private String name;
    private ArrayList<String> hobby;
    private String[] family;
    private LinkedHashMap<String, String> map;
    private Pet pet;

    // omit getters and setters
    // omit toString()
```

---

```java {.line-numbers}
package com.itheima.domain;

// omit imports

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PersonTest {
    @Autowired
    private Person person;

    @Test
    void getPet() {
        Pet expected=new Pet();
        expected.setType("dog");
        expected.setName("kity");
        assertThat(person.getPet(),is(equalTo(expected)));
    }

    // omit other test-cases
}
```

---

```yaml {.line-numbers}
person:
    id: 2
    name: 张三
    hobby:
        - play
        - read
        - sleep
    family:
        - father
        - mother
    map:
        k1: v1
        k2: v2
    pet:
        type: dog
        name: kity
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `@Value`属性注入

---

```java {.line-numbers}
@Component
public class Person {
    @Value("${person.id}")
    private int id;
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `@ConfigurationProperties` VS `@Value`

---

|对比点|`@ConfigurationProperties`|`@Value`|
|--|--|--|
|所属框架|`Spring Boot`|`Spring`|
|功能|批量注入|单个注入|
|`setXX()`方法|需要|不需要|
|复杂类型属性注入|支持|不支持|
|松散绑定|支持|不支持|
|`JSR303`数据校验|支持|不支持|
|`SpEL`表达式|不支持|支持|

---

#### 松散绑定

```properties {.line-numbers}
person.firstName=james
person.first-name=james
person.first_name=james
person.FIRST_NAME=james
```

---

#### `JSR303`

```xml {.line-numbers}
<!-- pom.xml -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-validation</artifactId>
</dependency>
```

---

```java {.line-numbers}

// omit imports

@Component
@ConfigurationProperties(prefix = "person")
@Validated
public class Person {
    @Email
    private String email;

    // omit others
}
```

---

#### `SpEL`

```java {.line-numbers}
@Value("#{5*2}")
private int id;
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 自定义配置文件

---

#### `@PropertySource`加载自定义`properties`配置文件

---

```properties {.line-numbers}
# src/resources/my-properties.properties

test.id=110
test.name=test
```

---

1. `@PropertySource`：指定自定义配置文件的路径，标注在配置类上

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
@PropertySource("classpath:my-properties.properties") // load customized config
public class MyPropertiesConfigFile {
}
```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MyPropertiesConfigFileTest {

    @Value("${test.id}")
    private int id;

    @Value("${test.name}")
    private String name;

    @Test
    public void testMyProperties() {
        int expectedId = 110;
        String expectedName = "test";
        Assertions.assertEquals(expectedId, this.id);
        Assertions.assertEquals(expectedName, this.name);
    }
}
```

---

### `@ImportResource`加载自定义`xml`配置文件

---

```xml {.line-numbers}
<!-- src/main/resources/my-service-bean.xml -->

<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="myService" class="com.itheima.config.MyService"/>
</beans>
```

---

```java {.line-numbers}
package com.itheima.config;

public class MyService {
}
```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@ImportResource("classpath:my-service-bean.xml") // load customized config
@Configuration
public class MyXmlConfigFile {
}

```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@SpringBootTest
@ExtendWith(SpringExtension.class)
class MyXmlConfigFileTest {
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testMyService() {
        assertThat(applicationContext.containsBean("myService"), is(true));
    }
}
```

---

#### `@Configuration`定义自定义配置类

---

1. `@Configuration`：定义为配置类
1. `@Bean`：返回已配置的组件

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@Configuration
public class MyConfig {

    @Bean
    public MyOtherService myOtherService(){
        return new MyOtherService();
    }
}
```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MyConfigClassTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testMyOtherService() {
        assertThat(applicationContext.containsBean("myOtherService"), is(true));
    }
}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 多环境配置

---

1. `application`需要部署到不同的环境以实现不同的目的，如：`dev-env`，`test-env`/`uat-env`, `prod-env`
1. 不同的环境可能需要不同的配置

---

### 配置`profile`的方法

1. `profile-file`（环境配置文件）的文件名格式：`application-{profile}.properties`
1. `@Profile`：作用于`class`，`value`属性指定`profile`（作用等同于`profile-file`的`{profile}`）

---

### 激活`profile`的方法

1. 全局配置文件设置`spring.profiles.active`激活指定的`profile`
1. 启动参数激活指定的`profile`（覆盖全局配置文件的设置）
    1. `java -jar {app-jar} --spring.profiles.active=test`
    1. `mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.profiles.active=test"`

---

```properties {.line-numbers}
# src/main/resources/application-dev.properties

server.port=8081
```

```properties {.line-numbers}
# src/main/resources/application-test.properties

server.port=8082
```

```properties {.line-numbers}
# src/main/resources/application-prod.properties

server.port=8083
```

```properties {.line-numbers}
# src/main/resources/application.properties

spring.profiles.active=dev
```

---

```java {.line-numbers}
package com.itheima.config.profiles;

public interface DBConnector {
    String configure();
}
```

---

```java {.line-numbers}
package com.itheima.config.profiles;

// omit imports

@Configuration
@Profile("dev")
public class DevDBConnector implements DBConnector {
    @Override
    public String configure() {
        String ret = "数据库配置环境 --> dev";
        System.out.println(ret);
        return ret;
    }
}
```

---

```java {.line-numbers}
package com.itheima.config.profiles;

// omit imports

@Configuration
@Profile("test")   // 指定多环境配置类标识
public class TestDBConnector implements DBConnector {
    @Override
    public String configure() {
        String ret = "数据库配置环境 --> test";
        System.out.println(ret);
        return ret;
    }
}
```

---

```java {.line-numbers}
package com.itheima.config.profiles;

// omit imports

@Configuration
@Profile("prod")   // 指定多环境配置类标识
public class ProdDBConnector implements DBConnector {
    @Override
    public String configure() {
        String ret = "数据库配置环境 --> prod";
        System.out.println(ret);
        return ret;
    }
}
```

---

```java {.line-numbers}
package com.itheima.controller;

// omit imports

@RestController
public class DBController {
    @Autowired
    private DBConnector dbConnector;

    @GetMapping("/show-db")
    public String showDB() {
        return dbConnector.configure();
    }
}
```

---

### Test with Automation

---

```java {.line-numbers}
package com.itheima.config.profiles;

//omit imports

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ExtendWith(SpringExtension.class)
public class DevProfileTest {

    @BeforeEach
    public void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8081;
    }

    @AfterEach
    public void tearDown() {
        RestAssured.reset();
    }

    @Test
    public void testDevProfile() {
        String actual = get("/show-db").then().extract().body().asString();
        assertThat(actual, is(equalTo("数据库配置环境 --> dev")));
    }
}
```

---

#### Test with Manual

---

```bash {.line-numbers}
$ mvn spring-boot:run
```

<http://localhost:8081/show-db>

---

```bash {.line-numbers}
mvn spring-boot:run \
    -Dspring-boot.run.jvmArguments="-Dspring.profiles.active=test"
```

<http://localhost:8082/show-db>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 随机数与变量引用

---

```properties {.line-numbers}
my.secret=${random.value}
my.number=${random.int}
my.bignumber=${random.long}
my.uuid=${random.uuid}
my.number.less.than.ten=${random.int(10)}
my.number.in.range=${random.int[1024,65536]}
```

---

```properties {.line-numbers}
app.name=MyApp
app.description=${app.name} is a Spring Boot application
```

---

```xml {.line-numbers}
# src/main/resources/application.properties

tom.age=${random.int[10,20]}
tom.description=tom's age maybe is: ${tom.age}
```

---

```java {.line-numbers}
package com.itheima.config;

// omit imports

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class RandomAndVariableTest {

    @Value("${tom.age}")
    private int tomAge;

    @Value("${tom.description}")
    private String description;

    @Value("${system.env.path}")
    private String systemPath;

    @Test
    public void testRandom() {
        assertThat(tomAge, both(greaterThanOrEqualTo(10)).and(lessThanOrEqualTo(20)));
    }

    @Test
    public void testVariable() {
        System.out.println(description);
        System.out.println(systemPath);
    }
}
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
