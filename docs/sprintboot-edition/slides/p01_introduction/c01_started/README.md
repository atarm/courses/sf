---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Getting Started_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Getting Started

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [目标Goals](#目标goals)
2. [SpringBoot概要Outline of SpringBoot](#springboot概要outline-of-springboot)
3. [`Hello SpringBoot`](#hello-springboot)
4. [`unit testing` and `hot deployment`](#unit-testing-and-hot-deployment)
5. [SpringBoot原理简单分析Basic Analysis of SpringBoot Principle](#springboot原理简单分析basic-analysis-of-springboot-principle)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## 目标Goals

---

- **知识目标**：
    1. 了解`SpringBoot`的发展和优点
    1. 掌握`SpringBoot`的`setup`和`building`
    1. 掌握`SpringBoot`的`unit testing`和`hot deployment`
    1. 了解`SpringBoot auto-configuration`的原理及其流程
- **能力目标**：
    1. 能够使用`SpringBoot`快速搭建简单的`Spring-based application`
- **素养目标**：
    1. 养成良好的科学分析意识、结构思维意识、问题提出意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## SpringBoot概要Outline of SpringBoot

---

### What is SpringBoot

1. `spring ecosystem`是目前`javaee`事实上的主流
1. `spring boot`的目标是简化`spring ecosystem`的配置、提高开发效率
    1. 2013年开始始发，2014-04发布第一个版本
    1. 整合`spring ecosystem`和`third-part libraries`的配置，期望达到`out-of-box`的效果
<!-- 1. `spring ecosystem`从轻量级变得越来越全面，也越来越重 -->

---

### Strengths of SpringBoot

1. `initializr`: 快速创建`spring-based application`的`pom.xml`和`project structure`
1. `easy-configuration`: 简化`application configuration`
    1. 核心思想："convention over configuration"
    1. 核心技术：
        1. `starters`
        1. `annotation`
        1. `yaml` and `properties`

---

1. `dependency-management`: 统一管理`spring ecosystem`和`third-part libraries`的版本
1. `easy-testing`: 更方便的`unit testing`和`interface testing`
1. `easy-debug`: 更方便的`hot-deployment`
1. `easy-deployment`: 更方便的`jar`部署，嵌入`tomcat`、`jetty`、`undertow`等`web container`

>[spring.io. Getting Started.](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html)

---

### Develop with SpringBoot

1. languages supported: `java`, `kotlin`, `groovy`
1. building-tool supported: `maven`, `gradle`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Hello SpringBoot`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Use `maven`

---

1. 创建`maven`项目
1. 在`pom.xml`中添加`springboot`相关依赖
1. 编写主程序启动类
1. 创建用于`web`访问的`controller`
1. `run application`

---

#### 在`pom.xml`中添加`springboot`相关依赖

```xml {.line-numbers}
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.2</version>
</parent>

<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```

---

#### 编写主程序启动类

```java {.line-numbers}
@SpringBootApplication
public class MavenChapter01Application {

    public static void main(String[] args){
        SpringApplication.run(MavenChapter01Application.class,args);
    }
 }
```

---

#### 创建用于`web`访问的`controller`

```java {.line-numbers}
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello Spring Boot";
    }
}
```

---

#### `run application`

1. run application
    1. `ide-mode`: run the `main()`
    1. `maven-mode`
        1. 🌟 `mvn spring-boot:run`
        1. `mvn exec:java --projects ${project}`
    1. 🌟 `jar-mode`:`java -jar ${application_jar}`
1. access application: <http://localhost:8080/hello>

---

```xml {.line-numbers}
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

---

>1. [baeldung.com. Running a Spring Boot App with Maven vs an Executable War/Jar.](https://www.baeldung.com/spring-boot-run-maven-vs-executable-jar)
>1. [spring.io. Spring Boot Maven Plugin Documentation.](https://docs.spring.io/spring-boot/docs/2.7.2/maven-plugin/reference/htmlsingle/)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Use `spring initializr`

><https://start.spring.io/>

---

1. `spring initializr`是一个`application`，使用时需`online`
1. `spring initializr`帮助快速创建一个`springboot`项目
    1. `wizard`引导方式
    1. 自动生成基于场景的一些基础的目录和文件

---

1. 创建`springboot`项目
1. 创建用于`web`访问的`controller`
1. `run application`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `unit testing` and `hot deployment`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `unit testing`

---

1. `SpringBoot 2.2.0`: `junit5` as the default library for `unit testing`
1. `SpringBoot 2.4.0`: `spring-boot-starter-test` no longer includes the `vintage-engine` dependency

>1. <https://stackoverflow.com/a/65564514/6516920>
>1. [Migrating from JUnit 4 to JUnit 5 with Spring Boot 2.3.x](https://dev.to/ecirilo/migrating-from-junit-4-to-junit-5-with-spring-boot-2-3-x-1e02)

---

1. `pom.xml`中添加`spring-boot-starter-test`测试启动器
1. 编写测试类
1. 编写测试方法
1. 运行测试

---

```xml {.line-numbers}
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
```

---

```xml {.line-numbers}
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.2</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

---

```java {.line-numbers}
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class Chapter01ApplicationTest {

    @Autowired
    private HelloController helloController;

    @Test
    public void testHelloController() {
        String actual = helloController.hello();
        String expect = "hello，setup spring boot project with initializr...";
        Assertions.assertEquals(expect, actual);
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `hot deployment`

---

1. dependency: `pom.xml`中添加`spring-boot-devtools`依赖
1. run application
    1. `ide-mode`: run the `main()`
    1. `maven-mode`: `mvn spring-boot:run`(`mvn exec:java` or `java xxx` would not trigger the `spring-boot-devtools`)
1. modify the code
1. hot deployment
    1. `maven-mode`: `mvn compile`
    1. `ide-mode`: `build project`

---

#### `IDEA` Auto Build Project

1. `idea's compiler` setting: `build, execution, deployment >>> compiler`: check `build project automatically`
1. `automake` setting
    1. (`idea <= 2020`)`<ctrl + shift + alt + />` open `maintenance` dialog and open `registry`: check `compiler.automake.allow.when.app.running`
    1. (`idea >= 2021`)`advanced settings`: check `allow auto-make to start even if developed application is currently running`

---

```xml {.line-numbers}
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## SpringBoot原理简单分析Basic Analysis of SpringBoot Principle

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 依赖管理和插件管理Dependency Management and Plugin Management

---

#### Using Spring Boot with the Parent POM

---

##### the features of the starter parent project

1. **common dependencies**: The dependency management feature manages the versions of common dependencies.
1. **compiler level**: Provide the default compiler level as Java 1.8 and UTF-8 source encoding.
1. **plugin configuration**: Provides a default configuration for Maven plugins such as maven-surefire-plugin, maven-jar-plugin, and maven-failsafe-plugin.

<!--
1. Executes a repackage goal with a repackage execution id.
1. Resource filtering and configuring profile-specific files.
-->

>1. [spring.io. Inheriting the Starter Parent POM.](https://docs.spring.io/spring-boot/docs/2.7.2/maven-plugin/reference/htmlsingle/#using.parent-pom)
>1. [geeksforgeeks.org. Spring Boot – Starter Parent.](https://www.geeksforgeeks.org/spring-boot-starter-parent/)

---

```bash {.line-numbers}
$ mvn dependency:display-ancestors

[INFO] Ancestor POMs: 
    org.springframework.boot:spring-boot-starter-parent:2.7.2 
    <- org.springframework.boot:spring-boot-dependencies:2.7.2
```

---

```xml {.line-numbers}
<!-- spring-boot-dependencies-2.7.2.pom -->
<dependencyManagement>
    <dependencies>
        ......
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.7.2</version>
        </dependency>
        ......
    </dependencies>
</dependencyManagement>

<build>
    <pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.2</version>
            </plugin>
        </plugins>
    </pluginManagement>
</build>
```

---

```bash {.line-numbers}
# https://github.com/ExampleDriven/hierarchy-maven-plugin
$ mvn hierarchy:tree
```

>1. [stackoverflow. How do I show the Maven POM hierarchy?. 2012-02-02.](https://stackoverflow.com/questions/9120294/how-do-i-show-the-maven-pom-hierarchy)

---

```bash {.line-numbers}
[INFO] Displaying hierarchy. Set level=full to display dependencies in dependencyManagement
[INFO]  PARENT org.springframework.boot:spring-boot-starter-parent:2.7.2
[INFO]    PARENT org.springframework.boot:spring-boot-dependencies:2.7.2
[INFO]      IMPORT com.datastax.oss:java-driver-bom:4.14.1
[INFO]      IMPORT io.dropwizard.metrics:metrics-bom:4.2.10
[INFO]        PARENT io.dropwizard.metrics:metrics-parent:4.2.10
[INFO]      IMPORT org.codehaus.groovy:groovy-bom:3.0.11
[INFO]      IMPORT org.infinispan:infinispan-bom:13.0.10.Final
[INFO]        PARENT org.infinispan:infinispan-build-configuration-parent:13.0.10.Final
[INFO]          PARENT org.jboss:jboss-parent:36
[INFO]      IMPORT com.fasterxml.jackson:jackson-bom:2.13.3
[INFO]        PARENT com.fasterxml.jackson:jackson-parent:2.13
[INFO]          PARENT com.fasterxml:oss-parent:43
[INFO]      IMPORT org.glassfish.jersey:jersey-bom:2.35
[INFO]        PARENT org.eclipse.ee4j:project:1.0.6
[INFO]      IMPORT org.eclipse.jetty:jetty-bom:9.4.48.v20220622
[INFO]      IMPORT org.junit:junit-bom:5.8.2
[INFO]      IMPORT org.jetbrains.kotlin:kotlin-bom:1.6.21
[INFO]      IMPORT org.jetbrains.kotlinx:kotlinx-coroutines-bom:1.6.4
[INFO]      IMPORT org.apache.logging.log4j:log4j-bom:2.17.2
[INFO]        PARENT org.apache.logging:logging-parent:5
[INFO]          PARENT org.apache:apache:24
[INFO]      IMPORT io.micrometer:micrometer-bom:1.9.2
[INFO]      IMPORT org.mockito:mockito-bom:4.5.1
[INFO]      IMPORT io.netty:netty-bom:4.1.79.Final
[INFO]        PARENT org.sonatype.oss:oss-parent:7
[INFO]      IMPORT com.squareup.okhttp3:okhttp-bom:4.9.3
[INFO]      IMPORT com.oracle.database.jdbc:ojdbc-bom:21.5.0.0
[INFO]      IMPORT io.prometheus:simpleclient_bom:0.15.0
[INFO]        PARENT io.prometheus:parent:0.15.0
[INFO]      IMPORT com.querydsl:querydsl-bom:5.0.0
[INFO]      IMPORT io.r2dbc:r2dbc-bom:Borca-SR1
[INFO]      IMPORT io.projectreactor:reactor-bom:2020.0.21
[INFO]      IMPORT io.rsocket:rsocket-bom:1.1.2
[INFO]      IMPORT org.springframework.data:spring-data-bom:2021.2.2
[INFO]      IMPORT org.springframework:spring-framework-bom:5.3.22
[INFO]      IMPORT org.springframework.integration:spring-integration-bom:5.5.14
[INFO]      IMPORT org.springframework.security:spring-security-bom:5.7.2
[INFO]      IMPORT org.springframework.session:spring-session-bom:2021.2.0
```

---

#### Using Spring Boot without the Parent POM

>you can still keep the benefit of the dependency management ( **_but not the plugin management_** ) by using an import scoped dependency
>
>>1. [spring.io. Using Spring Boot without the Parent POM.](https://docs.spring.io/spring-boot/docs/2.7.2/maven-plugin/reference/htmlsingle/#using.import)

>1. [stackoverflow. Spring Boot - parent pom when you already have a parent pom.](https://stackoverflow.com/questions/21317006/spring-boot-parent-pom-when-you-already-have-a-parent-pom)

---

```xml {.line-numbers}
<dependencyManagement>
    <dependencies>
        <dependency>
            <!-- Import dependency management from Spring Boot -->
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-dependencies</artifactId>
            <version>{spring-boot.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### SpringBoot Auto Configuration

---

1. `SpringBoot`的启动入口是`@SpringBootApplication`注解标注类的`main()`方法
1. `@SpringBootApplication`扫描`Spring component`并自动配置`SpringBoot`
1. `@SpringBootApplication`是一个组合注解，包含三个核心注解：
    1. `@SpringBootConfiguration`：indicates that a class provides Spring Boot application `@Configuration`
    1. `@EnableAutoConfiguration`: enable auto-configuration
    1. `@ComponentScan`：Configures component scanning directives for use with `@Configuration` classes

---

```java {.line-numbers}
org.springframework.boot.autoconfigure
Annotation Type SpringBootApplication

@Target(value=TYPE)
 @Retention(value=RUNTIME)
 @Documented
 @Inherited
 @SpringBootConfiguration
 @EnableAutoConfiguration
 @ComponentScan(excludeFilters=
    {@ComponentScan.Filter(type=CUSTOM,classes=TypeExcludeFilter.class),})
public @interface SpringBootApplication

Indicates a configuration class that declares one or more @Bean methods \
    and also triggers auto-configuration and component scanning. \
    This is a convenience annotation that is equivalent to declaring \
    @SpringBootConfiguration, @EnableAutoConfiguration and @ComponentScan.
```

---

```java {.line-numbers}
org.springframework.boot
Annotation Type SpringBootConfiguration

@Target(value=TYPE)
 @Retention(value=RUNTIME)
 @Documented
 @Configuration
 @Indexed
public @interface SpringBootConfiguration

Indicates that a class provides Spring Boot application @Configuration. \
    Can be used as an alternative to the Spring's standard @Configuration \
    annotation so that configuration can be found automatically (for example in tests).

Application should only ever include one @SpringBootConfiguration \
    and most idiomatic Spring Boot applications \
    will inherit it from @SpringBootApplication.
```

---

```java {.line-numbers}
org.springframework.boot.autoconfigure
Annotation Type EnableAutoConfiguration

@Target(value=TYPE)
 @Retention(value=RUNTIME)
 @Documented
 @Inherited
 @AutoConfigurationPackage
 @Import(value=AutoConfigurationImportSelector.class)
public @interface EnableAutoConfiguration

Enable auto-configuration of the Spring Application Context, \
    attempting to guess and configure beans that you are likely to need. \
    Auto-configuration classes are usually applied based on your classpath \
    and what beans you have defined. For example, if you have \
    tomcat-embedded.jar on your classpath you are likely to want a \
    TomcatServletWebServerFactory \
    (unless you have defined your own ServletWebServerFactory bean).
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### SpringBoot执行流程

---

1. 初始化`SpringApplication`实例
1. 启动`SpringApplication`实例

---

#### 初始化`SpringApplication`实例

1. 设置`WebApplicationType`
1. 设置`SpringApplication`的初始化器：`setInitializers()`
1. 设置`SpringApplication`的监听器：`setListeners()`
1. 设置主程序启动类

---

#### 启动`SpringApplication`实例

1. 运行`SpringApplication`的监听器
1. 对运行环境进行预处理
1. 应用程序上下文组装配置
1. 启动应用程序上下文
1. 运行应用程序自定义执行器
1. 循环运行应用程序上下文

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
