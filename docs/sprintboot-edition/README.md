# Software Framework - SpringBoot Edition

## Based On

1. [spring.io. Spring Boot 2.7.2 GA.](https://github.com/spring-projects/spring-boot/releases/tag/v2.7.2): release at 2022-07-21, end of support at 2023-11-18, end of commercial support at 2025-02-18
1. [spring.io. Spring Boot 2.7.2 available now.](https://spring.io/blog/2022/07/21/spring-boot-2-7-2-available-now)

## Futhermore

### Books and Monographs

1. Mark Heckler. _"Spring Boot: Up and Running"_ .

### Courses and Tutorials

<!-- {contents here} -->

1. 👍 [Java全栈知识体系-SpringBoot2.5.x系列.](https://www.pdai.tech/md/spring/springboot/springboot.html)
1. [logicbig.com. Spring Boot Tutorials.](https://www.logicbig.com/tutorials/spring-framework/spring-boot.html) ❗ using `spring boot 1.x` ❗
1. [mkyong. Spring Boot Tutorials.](https://mkyong.com/tutorials/spring-boot-tutorials/)

### Papers and Articles

<!-- {contents here} -->

1. The "Testing with Spring Boot" Series
    1. [Unit Testing with Spring Boot. 2019-01-12.](https://reflectoring.io/unit-testing-spring-boot/)
    1. [Testing MVC Web Controllers with Spring Boot and @WebMvcTest. 2019-01-19.](https://reflectoring.io/spring-boot-web-controller-test/)
    1. [Testing JPA Queries with Spring Boot and @DataJpaTest. 2019-02-03.](https://reflectoring.io/spring-boot-data-jpa-test/)
    1. [Testing with Spring Boot and @SpringBootTest. 2019-07-22.](https://reflectoring.io/spring-boot-test/)

### Playgrounds and Exercises

<!-- {contents here} -->

1. 🌟 [thombergs/code-examples/spring-boot](https://github.com/thombergs/code-examples/tree/master/spring-boot)
1. 🏛️ [thymeleaf.org. The Thymeleaf Interactive Tutorial.](http://itutorial.thymeleaf.org/)

### Documents and Supports

1. 👍 [javadoc.io](https://javadoc.io)
1. 🏛️ [spring.io. Spring Boot 2.7.2 GA docs.](https://docs.spring.io/spring-boot/docs/2.7.2/)
    1. [`Spring Boot 2.7.2 GA api docs`](https://docs.spring.io/spring-boot/docs/2.7.2/api/)
    1. [`Spring Boot 2.7.2 GA reference docs`](https://docs.spring.io/spring-boot/docs/2.7.2/reference/html/)
    1. [`maven plugin api docs`](https://docs.spring.io/spring-boot/docs/2.7.2/maven-plugin/api/)
    1. [`maven plugin reference docs`](https://docs.spring.io/spring-boot/docs/2.7.2/maven-plugin/reference/htmlsingle/)
1. 🏛️ [spring.io. Spring Framework 5.3.22 docs.](https://docs.spring.io/spring-framework/docs/5.3.22/reference/html/index.html)
    1. [`Spring Framework 5.3.22 api docs`](https://docs.spring.io/spring-framework/docs/5.3.22/javadoc-api/)
    1. [`Spring Framework 5.3.22 reference docs`](https://docs.spring.io/spring-framework/docs/5.3.22/reference/html/index.html)
1. 🏛️ [spring.io. Spring Data - JPA 2.7.2 docs.](https://docs.spring.io/spring-data/data-jpa/docs/2.7.2/)
    1. [`Spring Data JPA 2.7.2 api docs`](https://docs.spring.io/spring-data/jpa/docs/2.7.2/api/)
    1. [`Spring Data JPA 2.7.2 reference docs`](https://docs.spring.io/spring-data/data-jpa/docs/2.7.2/reference/html/)
1. 🏛️ [spring.io. Spring Data - Redis 2.7.2 docs.](https://docs.spring.io/spring-data/data-redis/docs/2.7.2/)
    1. [`Spring Data - Redis 2.7.2 api docs`](https://docs.spring.io/spring-data/data-redis/docs/2.7.2/api/)
    1. [`Spring Data - Redis 2.7.2 reference docs`](https://docs.spring.io/spring-data/data-redis/docs/2.7.2/reference/html/)
1. 🏛️ [thymeleaf.org. Thymeleaf Documentation.](https://www.thymeleaf.org/documentation.html)

### Manuals and CheatSheets

1. 🌟 [Spring Annotations Cheat Sheet.](https://www.jrebel.com/blog/spring-annotations-cheat-sheet)
    1. <https://www.jrebel.com/sites/rebel/files/pdfs/cheatsheet-jrebel-spring-annotations.pdf>

### Standards and Specifications

<!-- {contents here} -->

### Softwares and Tools

<!-- {contents here} -->

### Tutorials and Articles

1. 👍 [Spring Boot 中文索引.](http://springboot.fun)
1. 🌟 [Spring Boot 系列文章.](http://www.ityouknow.com/spring-boot.html)
    1. [ityouknow/spring-boot-examples](https://github.com/ityouknow/spring-boot-examples)
1. [tutorialspoint. Spring Boot Tutorial.](https://www.tutorialspoint.com/spring_boot/index.htm)
1. [javatpoint. Spring Boot Tutorial.](https://www.javatpoint.com/spring-boot-tutorial)

### Miscellaneous
