# Data Access

## Questions

1. 以下选项中，关于Spring Boot整合MyBatis实现的说法，正确的是（C）  
A. Spring Boot整合Mybatis必须提供mybatis-spring-boot-starter依赖  
B. @MapperScan("xxx")注解的作用和@Mapper注解一样  
C. 在全局配置文件中配置MyBatis的XML映射文件位置使用mybatis.mapper-locations属性  
D. 使用注解方式编写MyBatis接口文件数据修改方法时，还需要使用@Transactional注解
2. 以下选项中，关于Spring Data JPA映射的实体类中相关注解的说法，正确的是（D）  
A. 使用Spring Data JPA进行数据查询映射时，需要在配置文件中开启驼峰命名映射  
B. 使用@Entity注解时，还要使用name属性指定具体映射的表名  
C. @Id注解必须标注在类属性上，表示主键对应的属性  
D. @Transient注解表示指定属性不是到数据库表的字段的映射，ORM框架将忽略该属性  
3. 以下选项中，关于Spring Data JPA编写Repository接口时，下列说法错误的是（BC）。(多选)  
A. 自定义Repository接口可以继承CurdRepository接口  
B. 可以在方法上添加@Update注解结合SQL语句实现数据修改  
C. 可以在方法上添加@Delete注解结合SQL语句实现数据删除  
D. 进行数据变更操作时，必须在Repository接口方法上添加@Transactional注解
4. Redis是一个开源内存中的数据结枃存储系统，可以用作（ABCD）。(多选)  
A. 数据库
B. 缓存中间件
C. 消息中间件
D. 以上都正确
5. 当Redis作为数据库时，下列与Spring Boot整合使用的相关说明，正确的是（A）。
A. @RedisHash(" persons")用于指定操作实体类对象在Redis数据库中的存储空间
B. @Id用于标识实体类主键，需要手动指定id生成策略
C. 使用Redis数据库，必须为实体类属性添加@Indexed属性生成二级索引
D. 编写操作Redis 3数据库的Repository接口文件时，需要继承JpaRepository接口

## Keys

1. C
1. D
1. BC，`@Update`和`@Delete`是`mybatis-spring-boot-starter`
1. ABCD
1. A
