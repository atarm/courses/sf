# Configuration

## Questions

1. 以下选项中，关于spring boot全局配置文件的说法，不正确的是（ ）  
A. 支持application.properties全局配置文件  
B. 支持application.yaml全局配置文件  
C. 支持application.yml全局配置文件  
D. 全局配置文件必须位于项目resources的根目录下
1. 以下选项中，关于@ConfigurationProperties和@Value注解的说法，正确的是（ ）
A. 都来源于spring boot框架
B. 都必须设置属性的setter方法
C. @ConfigurationProperties支持JSR303数据校验
D. @Value支持松散绑定语法

## Keys

1. D
1. C
