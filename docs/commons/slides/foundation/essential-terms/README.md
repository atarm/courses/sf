---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Foundations of Software Framework_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Foundations of Software Framework

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`Design Principle` VS `Design Pattern` VS `Architecture` VS `Framework`](#design-principle-vs-design-pattern-vs-architecture-vs-framework)
3. [`Language` VS `Context`/`Environment`/`Ecosystem`](#language-vs-contextenvironmentecosystem)
4. [`Programming` VS `Engineering`](#programming-vs-engineering)
5. [`Compile` VS `Build`](#compile-vs-build)
6. [`Project` VS `Product`](#project-vs-product)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Goals目标

- **知识目标**：理解与框架相关的术语之间的联系与区别
- **能力目标**：能辨析类似的相互关联的术语
- **素养目标**：养成良好的科学分析的精神

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Design Principle` VS `Design Pattern` VS `Architecture` VS `Framework`

---

<!-- ### 定义 -->

1. `Design Principle设计原则`：在进行软件系统设计时所要遵循的一些经验准则，应用该准则的目的通常是为了避免某些经常出现的设计缺陷。目前，较为公认的设计原则包括：开放-封闭原则、依赖倒置原则、里氏替换原则、接口隔离原则等
1. `Design Pattern设计模式`：对设计经验的显式表示。每个设计模式描述了一个反复出现的问题及其解法的核心内容，它命名、抽象并标识了一个通用设计结构的关键部分，使之可用来创建一个可复用的设计
1. `Software Architecture软件架构`/`Architecture架构`：内容包括软件的组成元素、元素的外部的可见特性以及与其他元素的相互关系
1. `Software Framework软件框架`/`Framework框架`：通常指的是为了实现某个业界标准或完成特定基本任务的软件组件规范，也指为了实现某个软件组件规范时，提供规范所要求之基础功能的软件产品

---

<!-- ### 区别与联系 -->

1. `Design Principle`：设计的准则、目的
1. `Design Pattern`：与具体编程语言无关的、可复用的设计解决方案
    - 狭义指的是1994年由`GoF`在《设计模式：可复用面向对象软件的基础》提出的3类23个设计模式（以`C++`作为示例代码语言）
    - 广义指的是任何可复用的设计解决方案
1. `Architecture`：作为名称表示软件的结构，作为动词表示制造软件结构的过程和方法
    - 狭义指的是软件级别的结构
    - 广义指的是任意级别的结构

---

1. `Framework`：符合一定的`design principle`的、应用了大量的`design pattern`的、具有良好的`architecture`的、针对 **某类场景的** 、 **与具体业务无关** 的 **半成品**

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Language` VS `Context`/`Environment`/`Ecosystem`

---

1. `Programming Language编程语言`/`Language语言`：用来定义计算机程序的`formal language形式化语言`（精确的数学或机器可处理的公式定义的语言）
1. `Context上下文`：语言（自然语言或人工语言）上下文环境。它包括语言因素，也包括非语言因素（特别是当涉及自然语言的时候）。

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Programming` VS `Engineering`

---

1. `Programming编程`：编制程序的行为
1. `Engineering工程`：由 **一群（个）人** 为达到某种目的，在 **一个较长时间周期内** **有组织地** 将各种资源转化为产品或服务 的 **过程**

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Compile` VS `Build`

---

1. `Compile编译`：将高级语言程序变换成与之等价的汇编语言程序或机器代码程序的过程
1. `Build构建`：编译以及与编译相关任务的编排和调度（"先"）

>代码变成可执行文件，叫做编译（compile）；先编译这个，还是先编译那个（即编译的安排），叫做构建（build）。
>>阮一峰. Make 命令教程. <https://www.ruanyifeng.com/blog/2015/02/make.html>. 2015-02-20.

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Project` VS `Product`

---

1. `Project项目`：在时间、成本和资源等约束条件下，为了某个明确的目的而进行的一次性（明确的开始和结束时间）活动
1. `Product产品`：可提供给一个市场以供关注、获取、使用或消费，从而满足一定需要的事物



---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
