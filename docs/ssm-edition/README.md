# Software Framework - SSM Edition

## Futhermore

### Documents and Supports

<!-- {trusted documents goes here} -->

1. 🏛️ [mybatis.org. Introduction.](https://mybatis.org/mybatis-3/)
1. 🏛️ [spring.io. Spring Framework.](https://spring.io/projects/spring-framework)
    1. [Core Technologies.](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html)
    1. [Web on Servlet Stack.](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html)
    1. [Testing.](https://docs.spring.io/spring-framework/docs/current/reference/html/testing.html)
1. 🏛️ [Spring Framework Artifacts.](https://github.com/spring-projects/spring-framework/wiki/Spring-Framework-Artifacts)

### Manuals and CheatSheets

<!-- {manuals goes here} -->

1. [mybatis javadocs.](https://javadoc.io/doc/org.mybatis/mybatis/latest/org/apache/ibatis/session/SqlSessionFactory.html)
1. [spring framework 5.3.23 javadocs.](https://docs.spring.io/spring-framework/docs/5.3.23/javadoc-api/)

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses goes here} -->

1. 👍 [Spring框架知识体系详解.](https://pdai.tech/md/spring/spring.html)
1. 👍 [廖雪峰. Java教程.](https://www.liaoxuefeng.com/wiki/1252599548343744)
    1. [廖雪峰. Java教程 - 反射.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945147512512)
    1. [廖雪峰. Java教程 - 注解.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945389098144)
    1. [廖雪峰. Java教程 - 单元测试.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945269146912)
    1. [廖雪峰. Java教程 - Maven基础.](https://www.liaxuefeng.com/wiki/1252599548343744/1255945359327200)
    1. [廖雪峰. Java教程 - XML与JSON.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945389334784)
    1. [廖雪峰. Java教程 - JDBC编程.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255943820274272)
    1. [廖雪峰. Java教程 - 设计模式.](https://www.liaoxuefeng.com/wiki/1252599548343744/1264742167474528)
    1. [廖雪峰. Java教程 - Web开发.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945497738400)
    1. [廖雪峰. Java教程 - Spring开发.](https://www.liaoxuefeng.com/wiki/1252599548343744/1266263217140032)
    1. [廖雪峰. Java教程 - SpringBoot开发.](https://www.liaoxuefeng.com/wiki/1252599548343744/1266265175882464)
    1. [廖雪峰. Java教程 - SpringCloud开发.](https://www.liaoxuefeng.com/wiki/1252599548343744/1266263401691296o)

### Papers and Articles

<!-- {topics goes here} -->

### Playgrounds and Exercises

<!-- {exercises here} -->

### Examples and Templates

<!-- {examples here} -->

### Standards and Specifications

<!-- {standards here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

### Miscellaneous

<!-- {misc here} -->
