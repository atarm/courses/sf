# MyBatis

## Futhermore

### Standards and Specifications

<!-- {standards, specifications, conventions goes here} -->

### Documents and Supports

<!-- {trusted or official documents goes here} -->

### Manuals and CheatSheets

<!-- {manuals goes here} -->

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses and tutorials goes here} -->

### Papers and Articles

<!-- {special topics goes here} -->

1. [Marco Behler. Java & Databases: An Overview of Libraries & APIs. 2022-08-24.](https://www.marcobehler.com/guides/java-databases)

### Playgrounds and Exercises

<!-- {exercises here} -->

### Examples and Templates

<!-- {examples here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

### Miscellaneous

<!-- {misc here} -->
