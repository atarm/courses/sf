---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Dynamic SQL_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 动态SQL Dynamic SQL

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`Dynamic Sql` Introduction](#dynamic-sql-introduction)
3. [`<if>`](#if)
4. [`<choose>-<when>-<otherwise>`](#choose-when-otherwise)
5. [`<where>`, `<set>` and `<trim>`](#where-set-and-trim)
6. [`<foreach>`](#foreach)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 掌握`dynamic sql`
- **能力目标**：
    1. 能使用`dynamic sql`编写较复杂的`sql`
- **素养目标**：
    1. 养成良好的结构思维意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Dynamic Sql` Introduction

>[mybatis.org. Dynamic SQL.](https://mybatis.org/mybatis-3/dynamic-sql.html)

---

1. `dynamic sql`是`mybatis`主要功能之一，用于根据不同的条件运行时动态地构建`sql`
1. `dynamic sql`主要解决了传统的字符串拼接`sql`的以下问题
    1. 代码冗余：传统的字符串拼接方式需要使用大量的`if`语句和字符串拼接，代码复杂且维护困难
    1. `sql injection`：传统的字符串拼接方式容易受到`sql injection`攻击，导致数据泄露或系统崩溃
    <!-- 1. 性能问题：传统的字符串拼接方式每次都需要重新拼接`sql`，效率较低 -->

---

1. `<if>`: 基本的条件选择
1. `<choose>-<when>-<otherwise>`: 多路条件选择
1. `<where>`, `<set>` and `<trim>`
    - `<where>`: 简化`sql`中的`where`
    - `<set>`: 简化`sql`中的`set`
    - `<trim>`: `sql`子句的首尾的添加与删除
1. `<foreach>`: 集合迭代，常用于`sql`的`in`子句

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `<if>`

>[mybatis.org. dynamic sql - if.](https://mybatis.org/mybatis-3/dynamic-sql.html#if-1)

---

```xml {.line-numbers}
<select id="findActiveBlogLike" resultType="Blog">
    SELECT * FROM BLOG WHERE state = 'ACTIVE'
    <if test="title != null">
        AND title like #{title}
    </if>
    <if test="author != null and author.name != null">
        AND author_name like #{author.name}
    </if>
</select>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `<choose>-<when>-<otherwise>`

>[mybatis.org. dynamic sql - choose when otherwise](https://mybatis.org/mybatis-3/dynamic-sql.html#choose-when-otherwise)

---

1. choose **_only one case_** among many options
1. familiar with `if...else if...else` or `switch...case break...default`

---

```xml {.line-numbers}
<select id="findActiveBlogLike" resultType="Blog">
    SELECT * FROM BLOG WHERE state = 'ACTIVE'
    <choose>
        <when test="title != null">
            AND title like #{title}
        </when>
        <when test="author != null and author.name != null">
            AND author_name like #{author.name}
        </when>
        <otherwise>
            AND featured = 1
        </otherwise>
    </choose>
</select>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `<where>`, `<set>` and `<trim>`

>[mybatis.org. dynamic sql - trim where set.](https://mybatis.org/mybatis-3/dynamic-sql.html#trim-where-set)

---

### Without `<where>`

```xml {.line-numbers}
<select id="findActiveBlogLike" resultType="Blog">
    SELECT * FROM BLOG
    WHERE
    <if test="state != null">
        state = #{state}
    </if>
    <if test="title != null">
        AND title like #{title}
    </if>
    <if test="author != null and author.name != null">
        AND author_name like #{author.name}
    </if>
</select>
```

---

#### None of the Conditions were Met

```sql {.line-numbers}
SELECT * FROM BLOG
WHERE
```

---

#### Only the Second Condition was Met

```sql {.line-numbers}
SELECT * FROM BLOG
WHERE
AND title like 'someTitle'
```

---

### With `<where>`

```xml {.line-numbers}
<select id="findActiveBlogLike" resultType="Blog">
  SELECT * FROM BLOG
    <where>
        <if test="state != null">
            state = #{state}
        </if>
        <if test="title != null">
            AND title like #{title}
        </if>
        <if test="author != null and author.name != null">
            AND author_name like #{author.name}
        </if>
    </where>
</select>
```

---

1. `<where>` element knows to only insert `"WHERE"` if there is any content returned by the containing tags
1. if that content begins with `"AND"` or `"OR"`, it knows to strip it off.

---

### `<set>`

```xml {.line-numbers}
<update id="updateAuthorIfNecessary">
    update Author
    <set>
        <if test="username != null">username=#{username},</if>
        <if test="password != null">password=#{password},</if>
        <if test="email != null">email=#{email},</if>
        <if test="bio != null">bio=#{bio}</if>
    </set>
    where id=#{id}
</update>
```

---

### `<trim>`

```xml {.line-numbers}
<trim prefix="" prefixOverrides="" suffix="" suffixOverrides="">
    ${sql script here}
</trim>
```

1. `prefix`: 待添加的前缀，当`<trim>`内有返回值时添加该前缀
1. `prefixOverrides`: 待删除的前缀，即，如果前缀出现定义的值，则删除
1. `suffix`: 待添加的后缀，当`<trim>`内有返回值时添加该后缀
1. `suffixOverrides`: 待删除的后缀，即，如果后缀出现定义的值，则删除

---

#### 使用`<trim>`实现`<where>`

```xml {.line-numbers}
<select id="findActiveBlogLike" resultType="Blog">
    SELECT * FROM BLOG
    <!-- whitespace is relevant. -->
    <trim prefix="WHERE" prefixOverrides="AND |OR ">
        <if test="state != null">
            state = #{state}
        </if>
        <if test="title != null">
            AND title like #{title}
        </if>
        <if test="author != null and author.name != null">
            AND author_name like #{author.name}
        </if>
    </trim>
</select>
```

<!--

---

```xml {.line-numbers}
<select id="getUserList" resultType="User">
    SELECT * FROM users
    <trim prefix="WHERE" prefixOverrides="AND|OR">
        <if test="ANDD != null">
        ANDD LIKE #{ANDD}
        </if>
        <if test="status != null">
        OR status = #{status}
        </if>
    </where>
    </select>
```

-->

---

#### 使用`<trim>`实现`<set>`

```xml {.line-numbers}
<update id="updateAuthorIfNecessary">
    update Author
    <trim prefix="SET" suffixOverrides=",">
        <if test="username != null">username=#{username},</if>
        <if test="password != null">password=#{password},</if>
        <if test="email != null">email=#{email},</if>
        <if test="bio != null">bio=#{bio}</if>
    </trim>
    where id=#{id}
</update>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `<foreach>`

>1. [mybatis.org. dynamic sql - foreach.](https://www.imooc.com/wiki/mybatis/foreach.html)

---

```xml {.line-numbers}
<foreach collection="" item="" index="" open="" close="" separator="">
    ...
</foreach>
```

1. `collection`: 待遍历的集合
1. `item`: 遍历过程中，集合中元素的别名
1. `index`: 遍历过程中，集合中元素的索引值
1. `open`: 开头字符串
1. `close`: 结尾字符串
1. `separator`: 遍历过程中的分隔符

---

```java {.line-numbers}
List<User> findUsersIn(int[] ids);
```

```xml {.line-numbers}
<select id="findUsersIn" resultType="User">
    select *
    from `t_user`
    <where>
        <foreach collection="array" item="id" open="id in (" close=")" separator=",">
            ${id}
        </foreach>
    </where>
</select>
```

---

### `collection`

1. 匿名参数（未通过`@Param`指定参数名）：`array`类型的参数名为`array`，`List`类型的参数名为`list`，`Set`类型的参数名为`collection`，`Map`类型的参数名为`map`
1. 具名参数（通过`@Param`指定参数名）：值为指定的参数名

---

```java {.line-numbers}
List<User> findUsersIn(@Param("ids") int[] ids);
```

```java {.line-numbers}
<select id="findUsersIn" resultType="User">
    select *
    from `t_user`
    <where>
        <foreach collection="ids" item="id" open="id in (" close=")" separator=",">
            #{id}
        </foreach>
    </where>
</select>
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
