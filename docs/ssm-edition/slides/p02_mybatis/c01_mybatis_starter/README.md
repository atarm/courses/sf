---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_MyBatis Starter_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# MyBatis入门MyBatis Starter

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [数据持久化与`ORM`](#数据持久化与orm)
3. [`MyBatis`概述](#mybatis概述)
4. [`MyBatis`开发的基本步骤](#mybatis开发的基本步骤)
5. [`MyBatis`配置文件](#mybatis配置文件)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 理解数据持久化概念和`ORM`原理
    1. 了解`mybatis`的诞生和演变、优缺点及适用场景
    1. 理解`mybatis`核心类的作用域和生命周期
    1. 掌握全局配置文件的结构和内容
- **能力目标**：
    1. 能完成`mybatis`环境的搭建及基本配置
    1. 能完成对数据表的简单查询操作
- **素养目标**：
    1. 养成良好的结构思维意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据持久化与`ORM`

---

1. 数据持久化：简称"持久化"，指数据从瞬时状态到持久状态转换的过程
1. `ORM(Object-Relational Mapping, 对象-关系映射)`：a programming technique for converting data between a `relational database` and the heap of an `object-oriented programming language`.
    1. 提供一个对象关系映射工具
    1. 提供一种操作语言或者API
    1. 提供事务交互、执行检查、延迟加载以及其他优化功能
1. `ORM`的核心需求：编程源码与`SQL`语句的相分离

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MyBatis`概述

---

### `MyBatis`的诞生、演变、优缺点及适用场景

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MyBatis`开发的基本步骤

---

1. 引入`mybatis`依赖
1. 编写`mybatis`配置文件
1. 编码实现
1. 编码使用：业务逻辑或测试脚本

><https://mybatis.org/mybatis-3/getting-started.html>

---

### 引入`mybatis`依赖

```xml {.line-numbers}
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>${mybatis.version}</version>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>${mysql-connector-java.version}</version>
    <scope>runtime</scope>
</dependency>
```

---

### 编写`mybatis`配置文件

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "https://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
  <environments default="development">
    <environment id="development">
      <transactionManager type="JDBC"/>
      <dataSource type="POOLED">
        <property name="driver" value="${driver}"/>
        <property name="url" value="${url}"/>
        <property name="username" value="${username}"/>
        <property name="password" value="${password}"/>
      </dataSource>
    </environment>
  </environments>
  <mappers>
    <mapper resource="${mapper-path}"/>
  </mappers>
</configuration>
```

---

### 编码实现

1. 编写实体类`entity`/`pojo(Plain Old Java Object)`
1. 编写接口类`mapper interface`/`mapper`/`dao(Data Access Object)`
1. 编写映射文件`mapper file`

---

#### 编写实体类

```java {.line-numbers}
import lombok.Data;

@Data
public class User {
    private Integer id;
    private String name;
    private String password;
}
```

---

>Project Lombok is a java library that automatically plugs into your editor and build tools, spicing up your java.
>
>Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.
>><https://projectlombok.org/>

---

#### 编写接口类`mapper interface`

```java {.line-numbers}
public interface UserMapper {
    List<User> findAllUsers();

    User findUserById(int id);

    int updateUser(User user);

    int addUser(User user);

    int removeUserById(int id);
}
```

---

#### 编写映射文件`mapper file`

>by default, `maven` will look for your project's resources under `src/main/resources`.
>><https://maven.apache.org/plugins/maven-resources-plugin/examples/resource-directory.html>

---

```xml {.line-numbers}
 <build>
    <resources>
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.xml</include>
            </includes>
        </resource>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*</include>
            </includes>
        </resource>
    </resources>
</build>
```

---

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="org.arm.course.sf.ssm.mybatis.dao.user.UserMapper">
    <select id="findAllUsers" resultType="org.arm.course.sf.ssm.mybatis.pojo.User">
        select *
        from `t_user`
    </select>
</mapper>
```

---

### 编码使用：测试脚本

1. 获取`SqlSession`对象
1. 获取`Mapper`对象
1. 使用`Mapper`对象
1. 关闭`SqlSession`对象

---

#### 获取与关闭`SqlSession`对象

1. `SqlSessionFactoryBuilder.build()`：`SqlSessionFactoryBuilder` object be `method-scope` will best
1. `SqlSessionFactory.openSession()`：`SqlSessionFactory` object be `application-scope` will best, `singleton` will be nice
1. `SqlSession.close()`: `SqlSession` object should be `thread-scope`/`request-scope`(`SqlSession` is not `thread-safe`), and must be `close()`

>1. [SqlSessionFactoryBuilder.](https://javadoc.io/doc/org.mybatis/mybatis/latest/org/apache/ibatis/session/SqlSessionFactoryBuilder.html)
>1. [Scope and Lifecycle.](https://mybatis.org/mybatis-3/getting-started.html#scope-and-lifecycle)

---

```java {.line-numbers}
try (SqlSession session = sqlSessionFactory.openSession()) {
  // do work
}// will automatic call session.close() at the end of try block
```

>[Oracle. The try-with-resources Statement.](https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html)

---

#### 获取与使用`Mapper`对象

1. `SqlSession.getMapper(Class<T> type)`: the same scope as `SqlSession`
1. `${your-mapper}.${your-method}()`

---

```java {.line-numbers}
try (SqlSession session = sqlSessionFactory.openSession()) {
  ConcreteMapper mapper = session.getMapper(ConcreteMapper.class);
  // do work
}
```

---

### Unit Testing

```xml {.line-numbers}
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter</artifactId>
    <version>${junit.version}</version>
    <scope>test</scope>
</dependency>
```

---

```java {.line-numbers}
//junit5
@BeforeEach
@AfterEach
@Test

//hamcrest
class org.hamcrest.MatcherAssert{
    public static <T> void assertThat(T actual, Matcher<? super T> matcher)
}
```

---

### Logging

>1. <https://mybatis.org/mybatis-3/logging.html>
>1. <https://logging.apache.org/log4j/2.x/>

---

#### 引入依赖

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-api</artifactId>
        <version>${log4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>${log4j.version}</version>
    </dependency>
</dependencies>
```

---

>Log4j 2 is broken up in an API and an implementation (core), where the API provides the interface that applications should code to. Strictly speaking Log4j core is only needed at runtime and not at compile time.
>
>However, below we list Log4j core as a compile time dependency to improve the startup time for custom plugins as it provides an annotation processor that generates a metadata file to cache plugin information as well as the necessary code to compile against to create custom plugins.
>><https://logging.apache.org/log4j/2.x/maven-artifacts.html>

---

#### 配置`log4j2.xml`

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %l %msg%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="error">
            <AppenderRef ref="Console"/>
        </Root>
        <Logger name="org.arm.course.sf.ssm.mybatis.dao" level="TRACE" additivity="false">
            <AppenderRef ref="Console"/>
        </Logger>
    </Loggers>
</Configuration>
```

---

#### 配置`mybatis-config.xml`

```xml {.line-numbers}
<settings>
    <!-- log4j 2 -->
    <setting name="logImpl" value="LOG4J2"/>
</settings>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MyBatis`配置文件

>1. <https://mybatis.org/mybatis-3/configuration.html>
>1. <http://mybatis.org/dtd/mybatis-3-config.dtd>

---

1. 配置文件习惯命名为`mybatis-config.xml`（非强制、亦非默认名称）
1. 配置文件有时也称为"核心配置文件"或"全局配置文件"
1. 配置文件是使用`mybatis`的入口，主要用于配置数据库连接环境、全局配置信息和`mapper file`的寻址方法等

---

- `configuration`
    1. ⭐ `properties`: 外部`properties`或内部`<property>`
    1. ⭐ `settings`: 运行时行为，一般为全局属性
    1. ⭐ `typeAliases`: 类型别名/简称
    1. `typeHandlers`: 类型处理器
    1. `objectFactory`: 对象工厂
    1. `plugins`: 插件
    1. ⭐ `environments`: 环境配置
    1. `databaseIdProvider`: 数据库厂商标识
    1. ⭐ `mappers`: 映射文件

⚠️ 元素节点有顺序要求 ⚠️

---

```xml {.line-numbers}
<!ELEMENT configuration (properties?, settings?, typeAliases?,
                            typeHandlers?, objectFactory?, objectWrapperFactory?, 
                            reflectorFactory?, plugins?, environments?, 
                            databaseIdProvider?, mappers?)>
```

---

### `<properties>`

```xml {.line-numbers}
<properties resource="org/mybatis/example/config.properties">
    <property name="username" value="dev_user"/>
    <property name="password" value="F2Fa3!33TYyg"/>
</properties>

<dataSource type="POOLED">
    <property name="driver" value="${driver}"/>
    <property name="url" value="${url}"/>
    <property name="username" value="${username}"/>
    <property name="password" value="${password}"/>
</dataSource>
```

```java {.line-numbers}
SqlSessionFactoryBuilder.build(InputStream inputStream, Properties properties)
```

---

>1. Properties specified in the body of the properties element are **_read first_**,
>1. Properties loaded from the classpath resource or url attributes of the properties element are **_read second_**, and **_override any duplicate properties_** already specified,
>1. Properties passed as a method parameter are **_read last_**, and **_override any duplicate properties_** that may have been loaded from the properties body and the resource/url attributes.
>
>>[mybatis.org. configuration - properties](https://mybatis.org/mybatis-3/configuration.html#properties)

---

### `<settings>`

>[mybatis.org. configuration - settings.](https://mybatis.org/mybatis-3/configuration.html#settings)

---

### `typeAliases`

>A type alias is simply a shorter name for a Java type. It's only relevant to the XML configuration and simply exists to reduce redundant typing of fully qualified classnames.
>>[mybatis.org. configuration - type aliases.](https://mybatis.org/mybatis-3/configuration.html#typealiases)

❗ `alias`不区分大小写 ❗

---

```xml {.line-numbers}
<typeAliases>
    <typeAlias alias="Author" type="domain.blog.Author"/>
    <typeAlias alias="Blog" type="domain.blog.Blog"/>
</typeAliases>
```

```xml {.line-numbers}
<!-- That is domain.blog.Author will be registered as author.
    If the @Alias annotation is found its value will be used as an alias. -->
<typeAliases>
    <package name="domain.blog"/>
</typeAliases>
```

---

### `<environments>`/`<environment>`

- `environments`
    - `environment`: 数据库连接环境
        1. `transactionManager`: 事务管理器
        1. `dataSource`: 数据源

---

1. `<environments>`须指定一个`default="xxx"`
1. `<transactionManager>`: 事务管理器，`type="[JDBC|MANAGED]"`
    1. `JDBC`: 使用`JDBC`提供的原生事务管理机制
    1. `MANAGED`: 使用`application container`提供的事务管理机制
<!-- 1. 一个`SqlSessionFactory`对象只能属于一个`<environment>` -->

```java {.line-numbers}
/**environment: the id of <environment> in mybatis-config.xml*/
SqlSessionFactoryBuilder.build(InputStream inputStream, String environment)
```

---

- `<dataSource>`: `type="[UNPOOLED|POOLED|JNDI]"`
    1. `UNPOOLED`: This implementation of DataSource simply opens and closes a connection **_each time_** it is requested.
    1. `POOLED`: This implementation of DataSource pools JDBC Connection objects to avoid the initial connection and authentication time required to create a new Connection instance.
    1. `JNDI`: This implementation of DataSource is intended for use with containers such as **_EJB or Application Servers_** that may configure the DataSource centrally or externally and place a reference to it in a JNDI context.

---

### `<mappers>`

1. 告诉`mybatis`在哪里寻找`mapper file`，以便`mapper interface`与`mapper file`相关联
1. 4种方式配置寻址

---

#### Using Classpath Relative Resources

```xml {.line-numbers}
<!-- Using classpath relative resources -->
<mappers>
    <mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
    <mapper resource="org/mybatis/builder/BlogMapper.xml"/>
    <mapper resource="org/mybatis/builder/PostMapper.xml"/>
</mappers>
```

---

#### Using URL Fully Qualified Paths

```xml {.line-numbers}
<!-- Using url fully qualified paths -->
<mappers>
    <mapper url="file:///var/mappers/AuthorMapper.xml"/>
    <mapper url="file:///var/mappers/BlogMapper.xml"/>
    <mapper url="file:///var/mappers/PostMapper.xml"/>
</mappers>
```

---

#### Using Mapper Interface Classes

```xml {.line-numbers}
<!-- Using mapper interface classes -->
<mappers>
    <mapper class="org.mybatis.builder.AuthorMapper"/>
    <mapper class="org.mybatis.builder.BlogMapper"/>
    <mapper class="org.mybatis.builder.PostMapper"/>
</mappers>
```

❗ 运行时，`mapper interface`与`mapper file`需在同一个目录下且与文件名（不含后缀）一致 ❗

---

#### Scan Mappers in a Package

```xml {.line-numbers}
<!-- Register all interfaces in a package as mappers -->
<mappers>
    <package name="org.mybatis.builder"/>
</mappers>
```

❗ 运行时，`mapper interface`与`mapper file`需在同一个目录下且与文件名（不含后缀）一致 ❗

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
