---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Mapping SQL_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# SQL映射Mapping SQL

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [Mapping Introduction](#mapping-introduction)
3. [`mapper file`](#mapper-file)
4. [`MyBatis`缓存机制](#mybatis缓存机制)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 掌握`mapper file`的主要元素
    1. 了解`mybatis`的缓存机制
- **能力目标**：
    1. 能通过编写`mapper file`进行`CRUD`操作
- **素养目标**：
    1. 养成良好的结构思维意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Mapping Introduction

---

1. `xml-based`: <https://mybatis.org/mybatis-3/sqlmap-xml.html>
1. `annotation-based`: <https://mybatis.org/mybatis-3/java-api.html#mapper-annotations>

---

>Java annotations are **_unfortunately limited_** in their **_expressiveness_** and **_flexibility_**. Despite a lot of time spent in investigation, design and trials, **_the most powerful MyBatis mappings simply cannot be built with annotations: without getting ridiculous that is_**. C# Attributes (for example) do not suffer from these limitations, and thus MyBatis.NET will enjoy a much richer alternative to XML. That said, the Java annotation-based configuration is not without its benefits.
>><https://mybatis.org/mybatis-3/java-api.html#mapper-annotations>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `mapper file`

>1. [mybatis.org. Mapper XML Files.](https://mybatis.org/mybatis-3/sqlmap-xml.html)
>1. [mybatis.org. mybatis-3-mapper.dtd.](http://mybatis.org/dtd/mybatis-3-mapper.dtd)

---

- `mapper`
    1. `cache`: Configuration of the cache for a given namespace.
    1. `cache-ref`: Reference to a cache configuration from another namespace.
    1. ⭐ `resultMap`: The most complicated and powerful element that describes how to load your objects from the database result sets.

---

- `mapper`
    1. `sql`: A reusable chunk of SQL that can be referenced by other statements.
    1. ⭐ `insert`: A mapped `INSERT` statement.
    1. ⭐ `update`: A mapped `UPDATE` statement.
    1. ⭐ `delete`: A mapped `DELETE` statement.
    1. ⭐ `select`: A mapped `SELECT` statement.

---

### `<mapper>`

1. `namespace="xxx.yyy.zzz"`: 与`mapper interface`的全限定类名一一对应

---

### `<sql>`

```xml {.line-numbers}
<sql id="sometable">
    ${prefix}Table
</sql>

<sql id="someinclude">
    from
    <include refid="${include_target}"/>
</sql>

<select id="select" resultType="map">
    select field1, field2, field3
    <include refid="someinclude">
        <property name="prefix" value="Some"/>
        <property name="include_target" value="sometable"/>
    </include>
</select>
```

---

### `<select>`/`<insert>`/`<update>`/`<delete>`

1. `id="xxx"`: 与`mapper interface`的`method name`一一对应
1. `parameterType="xxx"`: 形参数型
1. `<select>`查询结果
    1. `resultType`: 自动映射
    1. `resultMap`: 自定义映射

---

#### 参数传递与参数访问

1. `mapper interface`中传递过来的实参，在`mapper file`，以 **_什么形参名_**、**_什么语法规则_** 访问
1. 匿名参数：未通过`@Param`指定参数名
1. 具名参数：通过`@Param`指定参数名

---

##### 参数访问`#{}`与`${}`

1. `#{}`: `JDBC`的`preparedStatement`进行占位符赋值，即，在`sql`语句中均表现为字符串（添加单引号`''`）
1. `${}`: 字符串原样拼接，主要使用场景为传递数据库的`identifier`（如表名、字段名等）时

`#{}` == `'${}'`

---

>By default, using the #{} syntax will cause MyBatis to generate PreparedStatement properties and set the values safely against the PreparedStatement parameters (e.g. ?). While this is **_safer, faster and almost always preferred_**, sometimes you just want to directly inject an unmodified string into the SQL Statement.
>>[mybatis.org. String Substitution.](https://mybatis.org/mybatis-3/sqlmap-xml.html#string-substitution)

---

```sql {.line-numbers}
# if pass id = "2"
select * from `t_table` where id=#{id}
select * from `t_table` where id='2'

select * from `t_table` where id=${id}
select * from `t_table` where id=2
```

---

##### `parameterType`

1. `parameterType`: 可选，`mybatis`可以自动判断

```xml {.line-numbers}
<select id="selectPerson" parameterType="int" resultType="hashmap">
    SELECT * FROM PERSON WHERE ID = #{id}
</select>

<insert id="insertUser" parameterType="user">
    insert into users (id, username, password)
        values (#{id}, #{username}, #{password})
</insert>
```

<!-- ---

##### `mybatis`参数封装

1. 匿名单参数（即未使用`@Param`且单参数）：
1. ： -->

---

##### 匿名单参数传递

1. 基本数据类型或`String`：可通过`#{xxx}`等任意名称访问，建议通过`#{parameter-name}`访问
1. 复合数据类型或`map`：通过`#{property-name}`或`#{key-name}`读取

---

##### `@Param`具名单参数传递

1. 封装成`map`，两个`key-value`
    1. `param-name`
    1. `param1`
1. 两个`key`对应的`value`一样

---

##### 匿名多参数传递

1. 封装成`map`，两套`key-value`
    1. `arg0, arg1...`
    1. `param1, param2...`
1. 两套`key-value`都可访问

---

##### `@Param`具名多参数传递

1. 封装成`map`，两套`key-value`
    1. `param-name1, param-name2...`
    1. `param1, param2...`
1. 两套`key-value`都可访问

---

```java {.line-numbers}
int updatePasswordById(@Param("userId") int id,
                        @Param("userPassword") String password);
```

```xml {.line-numbers}
<update id="updatePasswordById">
    update `t_user`
    set password=#{userPassword}
    where id = #{userId}
</update>
```

---

##### 参数传递小结

1. 建议优先使用具名参数传递，单个实体类时，匿名参数传递和具名参数传递均可
1. 单个实体类使用具名参数传递时，使用`#{param-name.property-name}`或`${param-name.property-name}`

>[Mybatis参数传递介绍与源码分析. 2021-10-12.](https://blog.csdn.net/qq_41860497/article/details/120724821)

---

### 返回值

1. `<select>`
    1. 通过`resultType`或`resultMap`定义
    1. 一条记录可以映射成实体类或`map`（`key`为`column name`，`value`为`record value`）
    1. 若方法定义返回单条记录或标量，实际返回多条记录或矢量时，抛出`TooManyResultsException`异常
1. `<insert>`/`<update>`/`<delete>`: 返回`int`，表示影响行数

---

#### `resultMap` and `<resultMap>`

1. 自定义映射`sql`返回结果，字段名和属性名可以不一样，也可以指定要显示的列
1. 常用于处理字段名与属性名不一样以及多表连接查询的场景

>1. <https://mybatis.org/mybatis-3/sqlmap-xml.html#result-maps>

---

```xml {.line-numbers}
<resultMap id="userResultMap" type="User">
    <id property="id" column="user_id" />
    <result property="name" column="user_name"/>
    <result property="password" column="hashed_password"/>
</resultMap>

<select id="selectUsers" resultMap="userResultMap">
    select user_id, user_name, hashed_password
    from some_table
    where id = #{id}
</select>
```

---

#### `<resultMap>`的属性

1. `id`: 该`resultMap`的`id`
1. `type`: 对应的`java class`

---

#### `<resultMap>`的子标签

1. `<id>`：an ID result; flagging results as ID will help improve overall performance
1. `<result>`: a normal result injected into a field or JavaBean property（基本数据类型）
1. `<association>`: a complex type association; many results will roll up into this type（复合数据类型或另一个`resultMap`）
1. `<collection>`: a collection of complex types（集合数据类型）

---

1. `<id>`: 设置主键的映射关系
1. `<result>`: 设置普通字段的映射关系
1. `<association>`: 设置多对一的映射关系（实体类成员变量）
1. `<collection>`: 设置一对多的映射关系（集合成员变量）

---

#### `<resultMap>`的自动映射

1. `<resultMap>`自动映射在`<configuration><settings><setting name="autoMappingBehavior" value="[NONE|PARTIAL|FULL]">`进行设置（前提：`column name`和`attribute name`一样）
    1. `NONE`: 不自动映射
    1. `PARTIAL`(default): 自动映射，有内部嵌套（`<association>`和`<collection>`）除外
    1. `FULL`: 自动映射

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MyBatis`缓存机制

>1. [美团技术团队. 聊聊MyBatis缓存机制.](https://tech.meituan.com/2018/01/19/mybatis-cache.html)

---

1. 一级缓存：`SqlSession-scope`，默认启用一级缓存，`mybatis`没有提供全局的开关配制，`mapper file`中标签的`flushCache="true"`可关闭该`sql`执行时的一级缓存
    1. 一级缓存命中时返回同一个实例对象
1. 二级缓存：`SqlSessionFactory-scope`/`cross-SqlSession-scope`/`mapper-namespace-scope`
    1. `SqlSession.close()`后才将查询结果从一级缓存保存到二级缓存
    1. 实体类需可序列化
    1. 通过`readOnly`属性配置二级缓存命中时返回同一个缓存实例对象的只读引用，还是缓存实例对象的拷贝

---

![width:1100](./.assets/image/6e38df6a.jpg)

---

![width:1100](./.assets/image/28399eba.png)

---

### 二级缓存配置

```xml {.line-numbers}
<!-- mybatis-config.xml -->
<settings>
    <setting name="cacheEnabled" value="true"/>
</settings>

<!-- xxxMapper.xml -->
<cache/>

<select id="...">...</select>
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
