# Introduction导论

## Futhermore

### Standards and Specifications

<!-- {standards, specifications, conventions goes here} -->

### Documents and Supports

<!-- {trusted or official documents goes here} -->

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses and tutorials goes here} -->

1. 👍 [廖雪峰. Java教程.](https://www.liaoxuefeng.com/wiki/1252599548343744)
    1. [廖雪峰. Java教程 - 单元测试.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945269146912)
    1. [廖雪峰. Java教程 - Maven基础.](https://www.liaxuefeng.com/wiki/1252599548343744/1255945359327200)
1. 👍 [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600)

### Papers and Articles

<!-- {special topics goes here} -->

### Manuals and CheatSheets

<!-- {manuals goes here} -->

### Playgrounds and Exercises

<!-- {exercises here} -->

### Examples and Templates

<!-- {examples here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

### Miscellaneous

<!-- {misc here} -->
