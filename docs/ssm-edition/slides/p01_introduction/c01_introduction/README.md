---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction导论

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [Course Contents内容](#course-contents内容)
3. [Course Positioning and Goals定位与目标](#course-positioning-and-goals定位与目标)
4. [Course Assessments考核](#course-assessments考核)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

- **知识目标**：
    1. 了解课程学习内容、学习目标、学习考核
- **能力目标**：
    1. 能根据课程内容和课程目标列举市场需求
- **素养目标**：
    1. 养成良好的理性分析判断意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Course Contents内容

---

![height:500](./.assets/image/whatisMyBatis.png)

><https://www.perfomatix.com/hibernate-vs-mybatis/>

---

![height:600](./.assets/diagram/spring_tomcat_runtime.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Course Positioning and Goals定位与目标

---

### Prerequisites前置课程

1. `J2SE`/`JavaSE`
1. `Database`
1. `HTML` + `CSS` + `Javascript`
1. `JavaWeb`

---

### `SSM` --> `SpringBoot` --> `SpringCloud`(Cloud Native)

>SSM就好像开手动挡的汽车，需要踩离合、挂档、给油才能开动。SpringBoot就好像开自动挡的汽车，给油就走，踩刹车就停。
>>[SSM和SpringBoot对比.](https://www.cnblogs.com/wwct/p/12942781.html)

>SpringCloud是一系列用于实现微服务架构应用的框架集合，SpringBoot专注于方便地开发这里面的一个个具体的"微服务"。
>>[SpringBoot和SpringCloud的关系和区别.](https://www.cnblogs.com/wwct/p/12942982.html)

- [spring boot和SSM开发中有什么区别？ - 陈龙的回答 - 知乎](<https://www.zhihu.com/question/284488830/answer/618290880>)

---

### `DevOps` and `Platform Engineering`

![height:520](./.assets/diagram/crse_devops.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Course Assessments考核

---

1. 课堂听讲：20%
1. 实验练习：20%
1. 项目文档：20%
1. 项目作品：40%

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
