---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Automation Tools_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Automation Tools自动化工具

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`Markdown`](#markdown)
3. [`Git`](#git)
4. [`Maven`](#maven)
5. [`JUnit`](#junit)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

- **知识目标**：
    1. 掌握`markdown`等标记语言的使用
    1. 掌握`git`、`maven`、`junit`等自动化工具的使用
- **能力目标**：
    1. 能使用`markdown`编写文档
    1. 能使用`git`进行版本控制和团队协作
    1. 能使用`maven`管理和构建应用程序
    1. 能使用`junit`进行单元测试

---

- **素养目标**：
    1. 养成良好的工程自动化意识
    1. 养成良好的团队协作意识
    1. 养成良好的效率提升意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Markdown`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Git`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Maven`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `JUnit`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
