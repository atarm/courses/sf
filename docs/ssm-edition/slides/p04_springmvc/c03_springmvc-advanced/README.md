---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_SpringMVC Advanced_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# `SpringMVC` Advanced

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [文件上传](#文件上传)
2. [文件下载](#文件下载)
3. [数据校验](#数据校验)
4. [`RESTful`](#restful)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件上传

---

1. `SpringMVC`提供基于`MultipartResolver`的文件上传即插即用的支持

```java {.line-numbers}
org.springframework.web.multipart
Interface MultipartResolver

All Known Implementing Classes:
CommonsMultipartResolver, StandardServletMultipartResolver

There are two concrete implementations included in Spring, as of Spring 3.1:

CommonsMultipartResolver for Apache Commons FileUpload
StandardServletMultipartResolver for the Servlet 3.0+ Part API
```

---

```java {.line-numbers}
java.lang.Object
    org.springframework.web.multipart.commons.CommonsFileUploadSupport
        org.springframework.web.multipart.commons.CommonsMultipartResolver

All Implemented Interfaces:
org.springframework.beans.factory.Aware, ServletContextAware, MultipartResolver
```

```java {.line-numbers}
java.lang.Object
    org.springframework.web.multipart.support.StandardServletMultipartResolver

All Implemented Interfaces:
MultipartResolver
```

---

### 单文件上传

1. `pom.xml`添加依赖
1. 新建`View`：`file/upload/fileupload.jsp`、`file/upload/fileupload.result.jsp`
1. 新建`Controller`： `FileUploadController`
1. 新建`Service`: `FileUploadService`

---

```xml {.line-numbers}
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>${commons-fileupload.version}</version>
</dependency>
```

---

```java {.line-numbers}
//FileUploadController
@RequestMapping("")
public String fileUpload() {
    ...
}

@PostMapping("/fileupload.action")
public ModelAndView upload(@RequestParam("file") MultipartFile multipartFile, 
                                HttpServletRequest request){
    ...
}
```

---

```java {.line-numbers}
//FileUploadService
public void saveFile(MultipartFile multipartFile, String targetDirectory) \
        throws IOException{
    ...
}
```

---

### 多文件上传

```java {.line-numbers}
public ModelAndView upload(
            @RequestParam("files") List<MultipartFile> multipartFiles, 
            HttpServletRequest request)
```

<!-- ---

```java {.line-numbers}
@Target(value=PARAMETER)
 @Retention(value=RUNTIME)
 @Documented
public @interface RequestPart

Annotation that can be used to associate the part of a \
    "multipart/form-data" request with a method argument.
``` -->

---

1. <http://localhost:8080/file/upload>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件下载

---

1. 链接实现下载：存放在`web app`所在目录下，暴露下载文件`web app`的真实路径
1. 编码实现下载：存放在任意路径或数据库，可增加访问控制

---

1. `pom.xml`添加依赖
1. 新建`View`：`file/download/ls.jsp`
1. 新建`Controller`： `FileDownloadController`
1. 新建`Service`: `FileDownloadService`
1. 放置可下载的文件: `downloads/file/`

---

```xml {.line-numbers}
<dependency>
    <groupId>jakarta.servlet.jsp.jstl</groupId>
    <artifactId>jakarta.servlet.jsp.jstl-api</artifactId>
    <version>${jstl.version}</version>
</dependency>
<dependency>
    <groupId>org.apache.taglibs</groupId>
    <artifactId>taglibs-standard-impl</artifactId>
    <version>${taglibs.version}</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.apache.taglibs</groupId>
    <artifactId>taglibs-standard-spec</artifactId>
    <version>${taglibs.version}</version>
</dependency>
```

---

1. <http://localhost:8080/file/download>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据校验

---

1. `SpringMVC`提供两种方式进行数据校验
    1. `JSR-303`标准的`Bean Validation`校验
    1. `Spring`提供的`Validator`校验
1. `JSR-303`定义了一些常用的校验注解，如`@NotNull`、`@Size`、`@Min`、`@Max`、`@Pattern`等
1. `JSR-303`是`Java`提供的一套校验规范，`Hibernate Validator`是其参考实现之一
1. `Hibernate Validator`实现了`JSR-303`中内置`constraint`，并且附加了一些额外的`constraint`

---

### `JSR-303`约束

|约束|说明|
|:--|:--|
|`@Null`|必须为`null`|
|`@NotNull`|必须不为`null`|
|`@AssertTrue`|必须为`true`|
|`@AssertFalse`|必须为`false`|
|`@Min(value)`|必须是一个整数，其值必须大于等于指定的最小值|
|`@Max(value)`|必须是一个整数，其值必须小于等于指定的最大值|

---

|约束|说明|
|:--|:--|
|`@DecimalMin(value)`|必须是一个数字（整数或小数），其值必须大于等于指定的最小值|
|`@DecimalMax(value)`|必须是一个数字（整数或小数），其值必须小于等于指定的最大值|
|`@Size(max, min)`|元素的大小必须在指定的范围内|
|`@Digits(integer, fraction)`|必须是一个数字，其值必须在可接受的范围内|
|`@Past`|必须是一个过去的日期|
|`@Future`|必须是一个将来的日期|

---

### `Hibernate Validator`附加的约束

|约束|说明|
|:--|:--|
|`@Email`|必须是电子邮箱地址|
|`@Length`|字符串的长度必须在指定的范围内|
|`@NotEmpty`|字符串必须非空|
|`@Range`|必须在合适的范围内|

---

### `JSR-303`校验

1. `pom.xml`添加依赖
1. 新建`View`：`user/register.jsp`、`user/register_success.jsp`
1. 新建`Controller`： `UserController`
1. 新建`Service`: `UserService`

---

```xml {.line-numbers}
<dependency>
    <groupId>org.hibernate.validator</groupId>
    <artifactId>hibernate-validator</artifactId>
    <version>${hibernate-validator.version}</version>
</dependency>
```

❗ `hibernate-validator 7.0`对应`Jakarta EE 9` ❗

---

1. `@Valid`: 用于标注在需要校验的`Bean`前面，表示此`Bean`需要校验，`@Valid`后面紧跟`BindingResult`参数
1. `BindingResult`: 用于封装校验结果，如果校验出错，会将错误信息封装到`BindingResult`中
1. `Errors`: `BindingResult`的父接口，`BindingResult`继承自`Errors`

---

1. <http://localhost:8080/user/register>

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `RESTful`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `RESTful`是什么

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 常用注解

---

#### `@RequestBody`

```java {.line-numbers}
@Target(value=PARAMETER)
 @Retention(value=RUNTIME)
 @Documented
public @interface RequestBody

Annotation indicating a method parameter should be bound to the body of the web request. \
    The body of the request is passed through an HttpMessageConverter \
    to resolve the method argument depending on the content type of the request. \
    Optionally, automatic validation can be applied by annotating the argument with @Valid.
```

---

```java {.line-numbers}
@PostMapping(value = "/rest",
             consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
             produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@ResponseBody
public String rest(@RequestBody User user) {
    return "{\"restSupport\":true}";
}
```

---

#### `@ResponseBody`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
 @Retention(value=RUNTIME)
 @Documented
public @interface ResponseBody

Annotation that indicates a method return value should be bound to \
    the web response body. Supported for annotated handler methods.

As of version 4.0 this annotation can also be added on the type level \
    in which case it is inherited and does not need to be \
    added on the method level.
```

---

```java {.line-numbers}
@Controller
@RequestMapping("/restful")
public class DemoRestfulController {
    @RequestMapping(value = "/index", method = RequestMethod.GET, \
        produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody
    String index(HttpServletRequest request) {
        return "url:" + request.getRequestURI() + " can access";
    }
}
```

---

#### `@PathVariable`

```java {.line-numbers}
@Target(value=PARAMETER)
 @Retention(value=RUNTIME)
 @Documented
public @interface PathVariable

Annotation which indicates that a method parameter should be bound to \
    a URI template variable. Supported for RequestMapping annotated \
    handler methods.
If the method parameter is Map<String, String> then the map is \
    populated with all path variable names and values.
```

---

```html {.line-numbers}
<a href="/login3/张三/123456">登录</a>
```

```java {.line-numbers}
@RequestMapping("/login3/{username}/{password}")
public String login3(@PathVariable("username")String username, @PathVariable("password")String password) {
    if(username.equals("张三")&&password.equals("123456")) {
        return "hello";
    }
    else {
        return "error";
    }
}
```

---

#### `@RestController`

```java {.line-numbers}
@Target(value=TYPE)
 @Retention(value=RUNTIME)
 @Documented
 @Controller
 @ResponseBody
public @interface RestController

A convenience annotation that is itself annotated with @Controller and @ResponseBody.

Types that carry this annotation are treated as controllers where \
    @RequestMapping methods assume @ResponseBody semantics by default.

NOTE: @RestController is processed if an appropriate HandlerMapping-HandlerAdapter \
    pair is configured such as the \
    RequestMappingHandlerMapping-RequestMappingHandlerAdapter pair \
    which are the default in the MVC Java config and the MVC namespace.
```

---

#### `@JsonIgnore`

```java {.line-numbers}
com.fasterxml.jackson.annotation
Annotation Type JsonIgnore

@Target(value={ANNOTATION_TYPE,METHOD,CONSTRUCTOR,FIELD})
 @Retention(value=RUNTIME)
public @interface JsonIgnore

Marker annotation that indicates that the logical property that \
    the accessor (field, getter/setter method or Creator parameter \
    [of JsonCreator-annotated constructor or factory method]) is \
    to be ignored by introspection-based \
    serialization and deserialization functionality.
```

>[`@JsonIgnore` api docs.](https://javadoc.io/doc/com.fasterxml.jackson.core/jackson-annotations/2.13.5/com/fasterxml/jackson/annotation/JsonIgnore.html)

---

#### `@JsonProperty`

```java {.line-numbers}
com.fasterxml.jackson.annotation
Annotation Type JsonProperty

@Target(value={ANNOTATION_TYPE,FIELD,METHOD,PARAMETER})
 @Retention(value=RUNTIME)
public @interface JsonProperty

Marker annotation that can be used to define a non-static method \
    as a "setter" or "getter" for a logical property \
    (depending on its signature), or non-static object field \
    to be used (serialized, deserialized) as a logical property.
```

>[`@JsonProperty` api docs.](https://javadoc.io/doc/com.fasterxml.jackson.core/jackson-annotations/2.13.5/com/fasterxml/jackson/annotation/JsonProperty.html)

---

1. `@RequestBody`: 请求体中`JSON`被`Spring`反序列化为`JavaBean`或`Map`
1. `@ResponseBody`: `JavaBean`或`Map`被`Spring`序列化为`JSON`后放入响应体
1. `@PathVariable`: `URL`中的占位符映射到`Controller`方法的参数
1. `@RestController`: `@Controller`和`@ResponseBody`的组合注解
1. `@JsonIgnore`: `JavaBean`序列化为`JSON`时忽略指定属性
1. `@JsonProperty`: 配置`JavaBean`与`JSON`属性的映射关系

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `RESTful`示例

---

1. `pom.xml`添加依赖
1. 编写`Controller`：`ApiUserController`
1. 编写`Service`: `ApiUserService`

---

```xml {.line-numbers}
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>${jackson-databind.version}</version>
</dependency>
```

---

1. <http://localhost:8080/api/users>: `GET`
1. <http://localhost:8080/api/users/1>: `GET`
1. <http://localhost:8080/api/users/2>: `GET`
1. <http://localhost:8080/api/users/-1>: `GET`
1. <http://localhost:8080/api/login>: `POST`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
