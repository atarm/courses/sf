---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_SpringMVC - Spring - MyBatis Integration_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# SpringMVC - Spring - MyBatis Integration

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`SSM`整合步骤](#ssm整合步骤)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 理解`SSM`各个框架在应用程序中的作用
    1. 掌握`SSM`整合的步骤
- **能力目标**：
    1. 能使用`SSM`开发应用程序
- **素养目标**：
    1. 养成良好的科学分析、结构思维和问题提出意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `SSM`整合步骤

---

1. 编写配置文件
1. 编写`entity`/`value object`/`pojo`/`domain`
1. 编写`persistence`/`data access object`
1. 编写`service`/`logic`
1. 编写`presentation`
    1. 编写`controller`
    1. 编写`view`

---

### 编写配置文件

1. `pom.xml`
1. `web.xml`
1. `springmvc-config.xml`/`springmvc.xml`
1. `spring-config.xml`/`spring.xml`
1. `spring-mybatis-config.xml`/`spring-mybatis.xml`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
