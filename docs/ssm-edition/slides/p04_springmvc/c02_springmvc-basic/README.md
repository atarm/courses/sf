---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_SpringMVC Basic_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# `SpringMVC` Basic

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [常用注解](#常用注解)
2. [参数传递](#参数传递)
3. [静态资源映射](#静态资源映射)
4. [拦截器`Interceptor`](#拦截器interceptor)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 常用注解

---

### `@Controller`

```java {.line-numbers}
@Target(value=TYPE)
@Retention(value=RUNTIME)
@Documented
@Component
public @interface Controller

Indicates that an annotated class is a "Controller" (e.g. a web controller).

String value: The value may indicate a suggestion for a logical component name, \
    to be turned into a Spring bean in case of an autodetected component.
```

---

```java {.line-numbers}
@Target(value=TYPE)
@Retention(value=RUNTIME)
@Documented
@Indexed
public @interface Component

Indicates that an annotated class is a "component". \
    Such classes are considered as candidates for auto-detection \
    when using annotation-based configuration and classpath scanning.
```

---

```java {.line-numbers}
@Target(value=TYPE)
@Retention(value=RUNTIME)
@Documented
@Component
public @interface Service

Indicates that an annotated class is a "Service", originally defined \
    by Domain-Driven Design (Evans, 2003) as "an operation offered as \
    an interface that stands alone in the model, with no encapsulated state."

May also indicate that a class is a "Business Service Facade" \
    (in the Core J2EE patterns sense), or something similar. \
    This annotation is a general-purpose stereotype and individual \
    teams may narrow their semantics and use as appropriate.

This annotation serves as a specialization of @Component, \
    allowing for implementation classes to be autodetected \
    through classpath scanning.
```

---

```java {.line-numbers}
@Target(value=TYPE)
@Retention(value=RUNTIME)
@Documented
@Component
public @interface Repository

Indicates that an annotated class is a "Repository", originally defined by \
    Domain-Driven Design (Evans, 2003) as "a mechanism for \
    encapsulating storage, retrieval, and search behavior \
    which emulates a collection of objects".
```

---

1. `@Component`, `@Controller` , `@Service`, `@Repository`功能上基本一致，主要是语义上表达不同的层次
1. `SpringMVC`中将`@Controller`替换成`@Component`不能正常运行

---

### `@RequestMapping`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
@Retention(value=RUNTIME)
@Documented
public @interface RequestMapping

Annotation for mapping web requests onto methods in \
    request-handling classes with flexible method signatures.
```

1. `@RequestMapping`标注在`class`时，表示该`class`中所有方法的公共部分

---

1. `String[] path`: The path mapping URIs (e.g. "/profile").
1. `String[] value`: `@AliasFor(value="path")`, The primary mapping expressed by this annotation.
1. `@RequestMapping("/foo")` is equivalent to `@RequestMapping(path="/foo")`

---

1. `RequestMethod[] method`: The HTTP request methods to map to, narrowing the primary mapping: `GET`, `POST`, `HEAD`, `OPTIONS`, `PUT`, `PATCH`, `DELETE`, `TRACE`.
    1. `GET`：请求数据体现在`url`上，请求数据的长度有限制（`http`协议本身没有做限制，`browser`和`server`进行了限制）
    1. `POST`：请求数据不会体现在`url`上，体现在`http body`上，请求数据的长度较大（`server`进行配置限制大小）

---

>Browsers do support PUT and DELETE, but it is HTML that doesn't.
>
>This is because HTML 4.01 and **the final W3C HTML 5.0 spec** both say that the only HTTP methods that their form elements should allow are GET and POST.
>>[Why don't browsers support PUT and DELETE requests ?](https://programmertoday.com/why-dont-browsers-support-put-and-delete-requests/)

---

1. `HTTP 0.9`(1991, obsolete): 1个方法，`GET`
1. `HTTP 1.0`(1996, obsolete): 3个方法，`GET` `HEAD` `POST`
1. `HTTP 1.1`(1997, standard): 7个方法，`GET` `HEAD` `POST` `OPTIONS` `PUT` `DELETE` `TRACE`
1. `HTTP 2.0`(2015, standard): 9个方法，`GET` `HEAD` `POST` `OPTIONS` `PUT` `DELETE` `TRACE` `CONNECT` `PATCH`
1. `HTTP 3.0`(2022, standard): 9个方法，`GET` `HEAD` `POST` `OPTIONS` `PUT` `DELETE` `TRACE` `CONNECT` `PATCH`

>1. [HTTP.](https://en.wikipedia.org/wiki/HTTP)
>1. <https://www.cnblogs.com/machao/p/5788425.html>

---

1. `String[] consumes`: **_narrows_** the primary mapping by **_media types_** that can be consumed by the mapped handler
1. `String[] produces`: **_narrows_** the primary mapping by **_media types_** that can be produced by the mapped handler
1. `String[] headers`: the **_headers_** of the mapped request, **_narrowing_** the primary mapping
1. `String[] params`: the **_parameters_** of the mapped request, **_narrowing_** the primary mapping
1. `String name`: assign a name to this mapping

---

```java {.line-numbers}
@Controller
public class ExampleController {

    @RequestMapping(
            value = "/example/{id}",
            method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.TEXT_HTML,
            headers = "Accept-Encoding=gzip",
            params = "type=advanced"
    )
    public String exampleMethod(
            @PathVariable("id") String id,
            @RequestParam("type") String type) {
        // call service here
        return "exampleView";
    }
}
```

---

#### `@GetMapping`

```java {.line-numbers}
@Target(value=METHOD)
@Retention(value=RUNTIME)
@Documented
@RequestMapping(method=GET)
public @interface GetMapping

Annotation for mapping HTTP GET requests onto specific handler methods.

Specifically, @GetMapping is a composed annotation that acts \
    as a shortcut for @RequestMapping(method = RequestMethod.GET).
```

---

#### `@PostMapping`

```java {.line-numbers}
@Target(value=METHOD)
 @Retention(value=RUNTIME)
 @Documented
 @RequestMapping(method=POST)
public @interface PostMapping

Annotation for mapping HTTP POST requests onto specific handler methods.

Specifically, @PostMapping is a composed annotation that acts \
    as a shortcut for @RequestMapping(method = RequestMethod.POST).
```

---

### `@ResponseStatus`

```java {.line-numbers}
@Target(value={TYPE,METHOD})
 @Retention(value=RUNTIME)
 @Documented
public @interface ResponseStatus

Marks a method or exception class with the status code() and reason() that should be returned.

The status code is applied to the HTTP response when the handler method is invoked and \
    overrides status information set by other means, like ResponseEntity or "redirect:".
```

---

```java {.line-numbers}
@Controller
@RequestMapping("/example")
public class ExampleController {

    @RequestMapping("/status")
    @ResponseStatus(HttpStatus.OK)
    public void handleResponseStatus() {
        // code logic here
    }
}
```

---

```java {.line-numbers}
package org.springframework.http;

public enum HttpStatus
extends Enum<HttpStatus>

Enumeration of HTTP status codes.

The HTTP status code series can be retrieved via series().
```

><https://docs.spring.io/spring-framework/docs/5.3.25/javadoc-api/>

---

1. `1xx (Informational)`: The request was received, continuing process
1. `2xx (Successful)`: The request was successfully received, understood, and accepted
    1. `200`: OK
1. `3xx (Redirection)`: Further action needs to be taken in order to complete the request
    1. `301`: Moved Permanently
    1. `302`: Found(previously "Moved temporarily")

>[Joshua Hardwick. SEO的301和302重定向: 应该使用哪个？.2020-11-11.](https://ahrefs.com/blog/zh/301-vs-302-redirects/)

---

1. `4xx (Client Error)`: The request contains bad syntax or cannot be fulfilled
    1. `404`: Not Found
1. `5xx (Server Error)`: The server failed to fulfill an apparently valid request
    1. `503`: Service Unavailable

>1. [RFC 9110 HTTP Semantics-Status Codes.](https://www.rfc-editor.org/rfc/rfc9110.html#name-status-codes)
>1. [List of HTTP status codes.](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 参数传递

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `request`向`Controller`传参

---

#### 页面属性名与方法形参名一致时

```html {.line-numbers}
<form action="/login" method="post">
    <p>用户名：<input type="text" name="username"></p>
    <p>密码：<input type="password" name="password"></p>
    <input type="submit" value="登录">
</form>
```

```java {.line-numbers}
@PostMapping("/login")
public String login(String username,String password){
    if(username.equals("张三")&&password.equals("123456")) {
        return "hello";
    }
    else {
        return "error";
    }
}
```

---

#### 页面属性名与方法形参名不一致时

```java {.line-numbers}
@Target(value=PARAMETER)
@Retention(value=RUNTIME)
@Documented
public @interface RequestParam

Annotation which indicates that a method parameter \
    should be bound to a web request parameter.

String defaultValue: The default value to use as a fallback when the \
    request parameter is not provided or has an empty value.
String name: The name of the request parameter to bind to.
boolean required: Whether the parameter is required.
String value: Alias for name().
```

---

```java {.line-numbers}
@PostMapping("/login")
public String login(@RequestParam("username") String name, String password) {
    if(name.equals("张三")&&password.equals("123456")) {
        return "hello";
    }
    else {
        return "error";
    }
}
```

---

#### 方法形参为对象时

```java {.line-numbers}
@PostMapping("/login2")
public String login2(User user) {
    if(user.getUsername().equals("张三")&&user.getPassword().equals("123456")) {
        return "hello";
    }
    else {
        return "error";
    }
}
```

❗ 页面属性名应与类的属性名一致 ❗

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Controller`向`View`传参

---

#### 返回字符串传递`View`

```java {.line-numbers}
@RequestMapping("/hello")
public String hello() {
    return "hello";
}
```

---

#### 返回`ModelAndView`同时传递`View`和`Model`

```java {.line-numbers}
@RequestMapping("/hello2")
public ModelAndView hello2(){
    ModelAndView ret = new ModelAndView("hello");
    modelAndView.addObject("message", "SpringMVC");
    return ret;
}
```

---

```java {.line-numbers}
org.springframework.web.servlet
Class ModelAndView

java.lang.Object
    org.springframework.web.servlet.ModelAndView
```

><https://docs.spring.io/spring-framework/docs/5.3.25/javadoc-api/>

<!-- TODO:

---

#### `@ModelAttribute`

```java {.line-numbers}
@Target(value={PARAMETER,METHOD})
 @Retention(value=RUNTIME)
 @Documented
public @interface ModelAttribute

Annotation that binds a method parameter or method \
    return value to a named model attribute, exposed to a web view. \
    Supported for controller classes with @RequestMapping methods.
```

---

```java {.line-numbers}
//example of `@ModelAttribute`
@ModelAttribute("message")
public String message() {
    return "SpringMVC";
}
``` -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 静态资源映射

---

```xml {.line-numbers}
<mvc:resources mapping="/resources/images/**" location="/WEB-INF/images/"/>
```

1. `DispatcherServlet`捕获所有请求，包括静态资源所在目录请求，由于没有相应的`Controller`导致无法访问
1. 可以将静态资源放到期望的路径上，然后通过静态资源映射提供一致的访问路径

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 异常处理

---

1. `SpringMVC`通过`HandlerExceptionResolver`接口统一处理异常

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 拦截器`Interceptor`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `filter` VS `interceptor`

>1. [HandlerInterceptors vs Filters in Spring MVC. 2021-05-25.](https://www.baeldung.com/spring-mvc-handlerinterceptor-vs-filter)
>    - [code](https://github.com/eugenp/tutorials/tree/master/spring-boot-modules/spring-boot-mvc-3)
>1. [Filter vs. Interceptor in Spring Boot. 2021-11-25.](https://senoritadeveloper.medium.com/filter-vs-interceptor-in-spring-boot-2e49089f682e)
>1. [过滤器 和 拦截器 6个区别. 2020-06-04.](https://segmentfault.com/a/1190000022833940)

---

![height:560](.assets/image/0*c3FgKRFyt4DEaQhM.gif)

>[Filter vs. Interceptor in Spring Boot. 2021-11-25.](https://senoritadeveloper.medium.com/filter-vs-interceptor-in-spring-boot-2e49089f682e)

---

![width:1120](.assets/image/filters_vs_interceptors.jpg)

>[HandlerInterceptors vs. Filters in Spring MVC.](https://www.baeldung.com/spring-mvc-handlerinterceptor-vs-filter)

---

1. `browser` --> `servlet container` --> `filter` --> `servlet`(`DispatcherServlet`) --> `interceptor` --> `controller`
1. `filters` are part of the `webserver` and not the Spring framework. For incoming requests, we can use filters to manipulate and even block requests from reaching any servlet. Vice versa, we can also block responses from reaching the client.
1. `interceptors` are part of the Spring MVC framework and sit between the DispatcherServlet and our Controllers. We can intercept requests before they reach our controllers, and before and after the view is rendered.

---

#### `filter`

>1. [Java Servlet中Filter过滤器的原理以及使用方式. 2021-08-27.](https://juejin.cn/post/7000950677409103880)

---

```java {.line-numbers}
public interface Filter { 
    default public void init(FilterConfig filterConfig) throws ServletException { } 

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException;

    default public void destroy() {}
}
```

---

1. `init()`: invoked only once. It is used to initialize the filter.
1. `doFilter()`: invoked every time a user send a request to any resource, to which the filter is mapped. It is used to perform filtering tasks.
    1. before `chain.doFilter(request, response);`: request direction
    1. `chain.doFilter(request, response);`: allow access, if don't call this method will forbid access
    1. after `chain.doFilter(request, response);`: response direction
1. `destroy()`: invoked only once when filter is taken out of the service.

---

1. `born`：`filter`的实例是在应用被加载时就完成的实例化，并调用`init()`初始化，该方法只会被调用一次。在`filter`执行处理之前，`init()`必须成功完成
1. `alive`：与应用的存活期相同，`filter`在内存中是单例的，针对过滤范围内的资源，每次访问都会调用`doFilter()`进行处理
1. `dead`：应用被卸载时，此时会调用`destroy()`，该方法只会被调用一次

---

1. `client`发送请求到`server`，`server`根据该请求`url`找到对应的`filter`形成`filter-chain`
1. 从第一个`filter`开始进行处理，即，调用`doFilter()`
1. 当请求满足当前`filter`的要求或者是过滤操作完成之后，应在调用`chain.doFilter()`进行放行，该方法调用`filter-chain`中的下一个`filter`的`doFilter()`继续处理
1. 如果当前`filter`是`filter-chain`的最后一个`filter`，那么`chain.doFilter()`方法将会执行资源访问操作，访问完毕之后，将会依照最开始`filter`的调用顺序倒序的返回，接着执行`chain.doFilter()`方法后面的代码
1. 将响应结果提交给`server`，`server`再将响应返回给`client`

---

#### `interceptor`

---

```java {.line-numbers}
package org.springframework.web.servlet;

public interface HandlerInterceptor {
    default boolean preHandle(HttpServletRequest request, HttpServletResponse response,
        Object handler) throws Exception {
        return true;
    }
    default void postHandle(HttpServletRequest request, HttpServletResponse response, 
        Object handler, @Nullable ModelAndView modelAndView) throws Exception {
    }
    default void afterCompletion(HttpServletRequest request, HttpServletResponse response,
        Object handler, @Nullable Exception ex) throws Exception {
    }
}
```

<!--TODO: if(handler instanceof HandlerMethod) -->

---

1. `preHandle()`: interception point before the execution of a handler
1. `postHandle()`: interception point after successful execution of a handler
1. `afterCompletion()`: callback after completion of request processing, that is, after rendering the view

---

1. 相同点：都起到过滤/预处理功能，都是链式结构
1. 不同点：
    1. `filter`属于`servlet container`，`interceptor`属于`Spring MVC`
    1. `filter`在`DispatcherServlet`之前，`interceptor`在`DispatcherServlet`之后
    1. `filter`与`interceptor`可访问的对象不同
    <!-- 1. `filter`能处理所有进入`servlet container`的请求，`interceptor`只处理对`@Controller`和`static/`下的请求（❗ 待进一步确认 ❗） -->

<!-- TODO:
---

`filter -> interceptor -> controllerAdvice -> aspect -> controller` 
https://blog.csdn.net/zzhongcy/article/details/102498081
-->

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编写和配置`Interceptor`

---

```java {.line-numbers}
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String username = (String) request.getSession().getAttribute("loginUser");
        if (username == null) {
            response.sendRedirect("/login");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
```

---

```xml {.line-numbers}
<mvc:interceptors>
    <mvc:interceptor>
        <mvc:mapping path="/**"/>
        <mvc:exclude-mapping path="/login"/>
        <mvc:exclude-mapping path="/logout"/>
        <mvc:exclude-mapping path="/hello">
        <bean class="org.arm.courses.sf.ssm.springmvc.interceptor.LoginInterceptor"/>
    </mvc:interceptor>
</mvc:interceptors>
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
