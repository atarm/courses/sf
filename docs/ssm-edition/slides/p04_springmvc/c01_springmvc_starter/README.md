---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_SpringMVC Starter_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# `SpringMVC` Starter

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [Brief History of `JavaSE` and `JavaEE`/`JakartaEE`](#brief-history-of-javase-and-javaeejakartaee)
3. [`MVC` Pattern , `Three-Layer` Architecture and `Front-Back Separation` in `JavaWeb`](#mvc-pattern--three-layer-architecture-and-front-back-separation-in-javaweb)
4. [`SpringMVC` Introduction](#springmvc-introduction)
5. [The First `SpringMVC` Project](#the-first-springmvc-project)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 了解`Java Web`开发的演变
    1. 理解`MVC`在`web application`中的位置和作用
    1. 了解`SpringMVC`提供的功能
- **能力目标**：
    1. 能通过`IoC`和`AOP`编写解决实际需求的应用程序
- **素养目标**：
    1. 养成良好的科学分析、结构思维和问题提出意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Brief History of `JavaSE` and `JavaEE`/`JakartaEE`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MVC` Pattern , `Three-Layer` Architecture and `Front-Back Separation` in `JavaWeb`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `SpringMVC` Introduction

---

1. 正式名称：`Spring WebMVC`，一般简称为`SpringMVC`
1. `SpringMVC`是基于`IoC`和`AOP`等基础上，遵循`WebMVC`模式的`web`框架，目的是简化`JavaWeb`的开发
1. `SpringMVC`的特点：
    1. 结构清晰：集中调度、耦合度低
    1. 灵活配置
    1. 灵活裁剪：服务端视图、`RESTful`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `SpringMVC`的核心组件

---

#### `SpringMVC`提供的组件

1. **`DispatcherServlet`**: **中央控制器**/前置控制器/前端控制器，流程控制的中心，统一调度控制其它组件执行，降低组件之间的耦合，提高组件的扩展性
1. **`HandlerMapping`**：处理器映射器，可使用不同的方式设置映射关系，例如：配置文件方式、接口实现方式、注解方式等
1. **`HandleAdapter`**：处理器适配器
1. **`ViewResolver`**：视图解析器，支持多种类型的视图解析

---

#### 自定义开发的组件

1. **`Handler`/`Controller`**： 处理器/**控制器**/后端控制器
1. **`View`**：视图

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `SpringMVC`的请求流程

---

![width:1120](./.assets/diagram/springmvc_process.png)

---

1. 用户发送请求至`DispatcherServlet`(`web.xml`配置`DispatcherServlet`)：收到请求后`DispatcherServlet`不进行处理，而是委托给其他组件进行处理，作为统一访问点，进行全局的流程控制
1. `DispatcherServlet`调用`HandlerMapping`查找`Handler`及`Interceptor`
    1. 生成并返回相应的`Handler`及`Interceptor`给`DispatcherServlet`
1. `DispatcherServlet`调用`HandlerAdapter`

---

1. `HandlerAdapter`经适配调用具体的`Controller`
    1. `Controller`执行并返回`ModelAndView`给`DispatcherServlet`
1. `DispatcherServlet`将`ModelAndView`传递给`ViewResolver`
    1. `ViewResolver`解析后返回具体`View`
1. `View`根据`Model`里的数据进行渲染视图
1. `DispatcherServlet`响应用户

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## The First `SpringMVC` Project

---

### 打包方式

```xml {.line-numbers}
<packaging>war</packaging>
```

---

### 添加依赖

```xml {.line-numbers}
<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>${springframework.version}</version>
</dependency>
<!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>4.0.1</version>
    <scope>provided</scope>
</dependency>
```

---

### `IDEA`配置`Local Tomcat Server`

1. `Run >>> Edit Configurations...`
1. `+ (Add New Configuration) >>> Tomcat Server >>> Local`

---

1. `Server >>> Application server >>> Configure...`: config the local environment
1. `Server >>> On frame deactivation`: choose the `Update classes and resources`
1. `Server >>> On 'Update' action`: choose the `Update classes and resources`

---

1. `Deployment >>> + >>> Artifact`: add the `xxx.war exploded`
1. `Deployment >>> Application Context`: set value as `/` would be better

---

### 配置文件

1. `Web`配置文件：`src/main/webapp/WEB-INF/web.xml`
1. `SpringMVC`配置文件：`src/main/resources/springmvc-config.xml`

---

```xml {.line-numbers}
<!--region dispatcherServlet-->
<servlet>
    <servlet-name>dispatcherServlet</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:springmvc-config.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
</servlet>
<servlet-mapping>
    <servlet-name>dispatcherServlet</servlet-name>
    <url-pattern>/</url-pattern>
</servlet-mapping>
<!--endregion dispatcherServlet-->
```

---

```xml {.line-numbers}
<!--region characterEncodingFilter-->
<filter>
    <filter-name>characterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
        <param-name>encoding</param-name>
        <param-value>utf-8</param-value>
    </init-param>
    <init-param>
        <param-name>forceEncoding</param-name>
        <param-value>true</param-value>
    </init-param>
</filter>
<filter-mapping>
    <filter-name>characterEncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
<!--endregion CharacterEncodingFilter-->
```

<!-- ---

1. `<url-pattern>/</url-pattern>`：匹配路径型的`URL`，不匹配带后缀的`URL`（如`xxx.jsp`，`yyy.html`等）
1. `<url-pattern>/*</url-pattern>`：既匹配路径型的`URL`，也匹配带后缀的`URL` -->

---

### Controller and View

1. `Controller`
    1. `HelloController`: `/hello`
    1. `IndexController`: `/`
1. `View`
    1. `WEB-INF/views/hello.jsp`

---

1. `forward`：转发，服务端转发
1. `redirect`：重定向，服务端返回新的`URL`，客户端发出新的请求

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
