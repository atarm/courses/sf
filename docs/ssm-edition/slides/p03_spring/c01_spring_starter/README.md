---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Spring Starter_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Spring Starter

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`Spring`的诞生、演变、优缺点及适用场景](#spring的诞生-演变-优缺点及适用场景)
3. [`Spring Ecosystem` and `Spring Framework`](#spring-ecosystem-and-spring-framework)
4. [`Spring` Core Concepts](#spring-core-concepts)
5. [The First `Spring` Application](#the-first-spring-application)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 了解`Spring`的历史、优缺点及适用场景
    1. 了解`Spring`的生态及其主要组成部分
    1. 了解`Spring`的核心概念
- **能力目标**：
    1. 能梳理分析复杂概念或事件
    1. 能编写简单的`Spring Application`
- **素养目标**：
    1. 养成良好的科学分析意识
    1. 养成良好的结构思维意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring`的诞生、演变、优缺点及适用场景

---

### 企业级应用 VS 企业应用

1. `Enterprise-Level Application企业级应用`：通常指复杂的、大规模的软件系统，用于满足企业级别的需求和业务流程，这些应用程序通常需要支持分布式部署、高可用性、容错性、扩展性、安全性等。
1. `Enterprise Application/Business Application企业应用`：通常指专门用于企业管理和运营的应用程序，例如人力资源管理系统、财务管理系统、客户关系管理系统等

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring Ecosystem` and `Spring Framework`

---

1. `Spring Ecosystem`/`Spring Projects`: <https://spring.io/projects/>，21个`project`
1. `Spring Framework`: <https://spring.io/projects/spring-framework>，22个`artifact`

❗ 不明确说明的情况下，以下所述`spring`指`Spring Framework`，而不是`Spring Ecosystem` ❗

---

1. 设计理念：`BOP(Bean Oriented Programming)`，在`spring`中所有对象都可以看成是一个`bean`
1. 核心概念/核心技术：`IoC`和`AOP`
1. `Spring-Framework`是其他`Spring Projects`的`foundation`，`core module`是`Spring-Framework`其他绝大部分`artifacts`的`foundation`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Spring` Core Concepts

---

### `IoC Container`

1. `Container(容器)`
1. `IoC(Inversion of Control, 控制反转)`
1. `DI(Dependency Injection, 依赖注入)`

---

#### `Container`

>容器是一种为某种特定组件的运行提供必要支持的一个软件环境。例如，Tomcat就是一个Servlet容器，它可以为Servlet的运行提供运行环境。类似Docker这样的软件也是一个容器，它提供了必要的Linux环境以便运行一个特定的Linux进程。
>>[廖雪峰. Java教程-Spring开发-IoC容器](https://www.liaoxuefeng.com/wiki/1252599548343744/1266265100383840)

---

### `IoC`

1. `IoC`即是一种设计模式，也是一种设计原则
1. `IoC`作为设计模式：`IoC Container`，解决对象依赖关系管理问题，将对象的创建和依赖关系的管理从应用程序代码转移到`IoC Container`中
1. `IoC`作为设计原则：对应`DIP(Dependency Inversion Principle, 依赖倒置原则)`

---

#### `DI`

1. `DI`是`IoC`的一个实现机制/实现技术，即，将一个对象所依赖的其他对象（即它的依赖）从`Container`中取出并注入到该对象中，从而实现对象之间的解耦和管理

---

`Spring IoC`/`IoC` = `Container` + `DI`

---

### `AOP`

1. `AOP(Aspect Oriented Programming，面向切面编程)`: 将系统中的`横切关注点（Cross-Cutting Concerns）`从主业务逻辑中分离出来并进行模块化，以便于维护和重用
1. `aspect横切关注点`: 与业务无关或重复的、但又必须要与业务逻辑一起的代码，比如日志、安全、事务等
1. `AOP`将这些关注点封装成一个个的`aspect`，然后通过`interceptor`或`proxy`等方式，将切面织入到目标对象中

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## The First `Spring` Application

---

1. 编写实体类
1. 编写`dao interface`和`dao class`
1. 编写`aop logger class`
1. 编写`applicationContext.xml`
1. 编写测试类
    1. 不使用`spring-test`
    1. 使用`spring-test`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
