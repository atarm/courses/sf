---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Spring and MyBatis Integration_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Spring and MyBatis Integration

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`spring`与`mybatis`集成的关键事项](#spring与mybatis集成的关键事项)
3. [`Mapper`获取](#mapper获取)
4. [`Declarative Transaction`声明式事务](#declarative-transaction声明式事务)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 掌握`spring`与`mybatis`集成的方法
    1. 掌握实现声明式事务的方法
- **能力目标**：
    1. 能通过`spring`与`mybatis`的整合实现`crud`
- **素养目标**：
    1. 养成良好的科学分析意识
    1. 养成良好的结构思维意识
    1. 养成良好的问题提出意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `spring`与`mybatis`集成的关键事项

---

1. 需要添加哪些新的`dependencies`？
1. 哪些配置放在`mybatis-config.xml`中，哪些配置放在`spring-mybatis.xml`中？
1. 如何从`ioc`中获取`mapper`？
    1. 如何创建`SqlSessionFactory`
    1. 如何获取`SqlSession`
    1. 如何获取`Mapper`

❗ 以下描述中，`mybatis-config.xml`表示`mybatis`的配置文件，`spring-mybatis.xml`表示`spring`的配置文件 ❗

---

### 添加`mybatis-spring`依赖

```xml {.line-numbers}
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis-spring</artifactId>
    <version>${mybatis-spring.version}</version>
</dependency>
```

>[mybatis.org. Introduction What is MyBatis-Spring?](http://mybatis.org/spring/)

---

### 配置项

1. `SqlSessionFactoryBean`替代`SqlSessionFactoryBuilder`的功能，生成`sqlSessionFactory`实例
    1. `dataSource`是`SqlSessionFactoryBean`的必要属性
    1. `SqlSessionFactoryBean`会忽略`mybatis-config.xml`中的`<environments>`
1. `mybatis`其他配置选项即可以在`mybatis-config.xml`中设置，也可以在`spring-mybatis.xml`中设置

---

>1. `sqlSessionFactoryBean`有一个唯一的必要属性：用于`JDBC`的`DataSource`
>1. 确切地说，任何环境配置（`<environments>`），数据源（`<DataSource>`）和`MyBatis`的事务管理器（`<transactionManager>`）都会被忽略。`SqlSessionFactoryBean`会创建它自有的 `MyBatis`环境配置（`Environment`），并按要求设置自定义环境的值。
>
>>- [mybatis.org. SqlSessionFactoryBean.](https://mybatis.org/spring/factorybean.html)
>>- [mybatis.org. SqlSessionFactoryBean(zh).](https://mybatis.org/spring/zh/factorybean.html)

---

```xml {.line-numbers}
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>${druid.version}</version>
</dependency>
```

```xml {.line-numbers}
<!--region dataSource-->
<context:property-placeholder location="classpath:database.properties"/>
<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" destroy-method="close">
    <property name="driverClassName" value="${jdbc.driver}"/>
    <property name="url" value="${jdbc.url}"/>
    <property name="username" value="${jdbc.username}"/>
    <property name="password" value="${jdbc.password}"/>
</bean>
<!--endregion dataSource-->
```

---

```xml {.line-numbers}
<!--region config mybatis in `mybatis-config.xml` -->
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <property name="dataSource" ref="dataSource"/>
    <property name="configLocation" value="classpath:mybatis-config.xml"/>
</bean>
<!--endregion config mybatis in `mybatis-config.xml` -->
```

---

```xml {.line-numbers}
<!--region config mybatis in `spring-mybatis.xml`-->
<bean id="mybatisConfig" class="org.apache.ibatis.session.Configuration">
    <property name="logImpl" value="org.apache.ibatis.logging.log4j2.Log4j2Impl"/>
    <property name="useGeneratedKeys" value="true"/>
    <property name="mapUnderscoreToCamelCase" value="true"/>
</bean>

<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <property name="dataSource" ref="dataSource"/>
    <property name="typeAliasesPackage" value="org.arm.courses.sf.ssm.spring.pojo"/>
    <property name="configuration" ref="mybatisConfig"/>
</bean>
<!--endregion config mybatis in `spring-mybatis.xml`-->
```

---

1. `pojo.User`
1. `dao.UserMapper`
1. `service.UserService`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

---

## `Mapper`获取

---

1. `SqlSessionTemplate`: 对`SqlSession`的进一步封装和管理
1. `MapperFactoryBean`: 根据指定的`mapper interface`生成`bean`并由`IoC`进行管理
1. `MapperScannerConfigurer`: 根据指定的`package`扫描`mapper interface`批量生成`bean`并由`IoC`进行管理

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 通过`SqlSessionTemplate`获取`Mapper`

---

1. logic class
    1. `UserServiceImpl.getAllUsers()`
1. config file
    1. `spring-mybatis.xml`
1. test script
    1. `UserServiceImplTest.getAllUsers()`

---

```xml {.line-numbers}
<!--region `SqlSessionTemplate`-->
<bean id="sqlSessionTemplate" class="org.mybatis.spring.SqlSessionTemplate">
    <constructor-arg name="sqlSessionFactory" ref="sqlSessionFactory"/>
</bean>
<!--endregion `SqlSessionTemplate`-->
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `MapperFactoryBean`单个注册`Mapper`

---

1. logic class
    1. `UserServiceImpl.getUserById()`
1. config file
    1. `spring-mybatis.xml`
1. test script
    1. `UserServiceImplTest.getUserById()`

---

```xml {.line-numbers}
<!--region `MapperFactoryBean`-->
<bean id="userMapperByMapperFactoryBean" class="org.mybatis.spring.mapper.MapperFactoryBean">
    <property name="sqlSessionFactory" ref="sqlSessionFactory"/>
    <property name="mapperInterface" value="org.arm.courses.sf.ssm.spring.dao.UserMapper"/>
</bean>
<!--region `MapperFactoryBean`-->
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `MapperScannerConfigurer`批量注册`Mapper`

---

1. `mapper`注册到`ioc`时，`id`值为首字母小写的`mapper interface name`

---

1. logic class
    1. `UserServiceImpl.updateUserById()`
1. config file
    1. `spring-mybatis.xml`
1. test script
    1. `UserServiceImplTest.updateUserById()`

---

```xml {.line-numbers}
<!--region `MapperScannerConfigurer`-->
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <!-- optional if there is only one `SqlSessionFactory` instance in the `ioc`-->
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
    <property name="basePackage" value="org.arm.courses.sf.ssm.spring.dao"/>
</bean>
<!--endregion `MapperScannerConfigurer`-->
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Some Questions

---

1. `sqlSession.close()`在哪里？

---

1. `sqlSessionTemplate`内部管理`sqlSession`并自动关闭`sqlSession`
1. `mapper`方法返回时，由`spring`进行`sqlSession.close()`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Declarative Transaction`声明式事务

---

### 编程式事务

```java {.line-numbers}
Connection conn = ...;
try {
    conn.setAutoCommit(false);
    ...
    doJobOne();
    doJobTwo();
    ...
    conn.commit();
}catch(Exception e){
    conn.rollBack();
}finally{
    conn.close();
}
```

---

### 声明式事务的核心问题

1. 哪些方法需要进行事务处理
1. 这些方法的事务规则是什么

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 事务规则

---

#### `propagation`事务传播机制

当事务方法被另一个事务方法调用时，指定事务应该如何传播

```java {.line-numbers}

public void combineTx(){
    tx01();
    tx02();
}
```

---

1. `REQUIRED`（默认值）: 使用已有的事务，即，使用调用方的事务
1. `REQUIRES_NEW`: 开启新的事务，即，被调用方开启新的事务

<!-- 1. `MANDATORY`: 
1. `NESTED`: 
1. `SUPPORTS`: 
1. `NOT_SUPPORTED`: 
1. `NEVER`:  -->

---

#### `isolation`事务隔离级别

多个事务方法并发运行时，指定事务间的同步

1. `DEFAULT`（默认值）: 具体数据库的默认隔离级别
1. `READ_UNCOMMITTED`: `tx01`可以读取`tx02`未提交的写操作
1. `READ_COMMITTED`: `tx01`不能读取`tx02`未提交的写操作
1. `REPEATABLE_READ`: `tx01`执行时禁止其他事务对该记录进行写操作
1. `SERIALIZABLE`: `tx01`执行时禁止其他事务对该表进行写操作

---

| 隔离级别         | Oracle  | MySQL   |
| ---------------- | ------- | ------- |
| READ UNCOMMITTED | ×       | √       |
| READ COMMITTED   | √(默认) | √       |
| REPEATABLE READ  | ×       | √(默认) |
| SERIALIZABLE     | √       | √       |

---

#### 回滚规则

1. `rollback-for`：设定能够触发回滚的异常类型
    1. 默认只针对`runtime exception`触发事务回滚
    1. 通过全限定类名设置其他需要回滚事务的异常，多个类名用逗号隔开
1. `no-rollback-for`：设定不触发回滚的异常类型
    1. 默认针对`checked Exception`不触发事务回滚
    1. 通过全限定类名设置其他不需回滚事务的异常，多个类名用逗号隔开

---

#### 其他

1. `timeout`：以秒为单位，默认值为`-1`，即，不设超时
1. `read-only`：事务是否为只读，能够针对查询操作进行优化，默认值为`false`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 声明式事务配置

---

1. 配置事务管理器：注册`txManager`并为其注入`dataSource`依赖
1. `xml`方式
    1. 配置`<tx:advice>`，绑定`txManager`并对不同的方法定义不同的事务规则
    1. 配置`<aop:config>`，绑定`pointcut`和`advice`
1. `annotation`方式
    1. 配置`<tx:annotation-driven>`，绑定`txManager`
    1. 在类或方法上标注`@Transactional`并定义事务规则

---

1. logic class
    1. `UserServiceImpl.addUser()`
1. config file
    1. `spring-mybatis.xml`
1. test script
    1. `UserServiceImplTest.addUser()`

---

```xml {.line-numbers}
<insert id="addUser" parameterType="User" useGeneratedKeys="true" keyProperty="id">
    insert into `t_user`(name, password)
    values (#{user.name}, #{user.password})
</insert>
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
