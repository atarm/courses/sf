---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_IoC and AOP_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# IoC and AOP

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [`IoC`](#ioc)
3. [`Annotation`实现`IoC`](#annotation实现ioc)
4. [`AOP`](#aop)
5. [`Annotation`实现`AOP`](#annotation实现aop)

<!-- /code_chunk_output -->

---
<!--2rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

## Goals目标

---

- **知识目标**：
    1. 理解并掌握多种方式编写`IoC`程序的方式
    1. 理解并掌握多种方式编写`AOP`程序的方式
- **能力目标**：
    1. 能通过`IoC`和`AOP`编写解决实际需求的应用程序
- **素养目标**：
    1. 养成良好的科学分析意识
    1. 养成良好的结构思维意识
    1. 养成良好的问题提出意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `IoC`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `ApplicationContext`

---

```java {.line-numbers}
// spring-context.jar
package org.springframework.context;
public interface ApplicationContext extends EnvironmentCapable, 
        ListableBeanFactory, HierarchicalBeanFactory,
        MessageSource, ApplicationEventPublisher, ResourcePatternResolver {
}

All Superinterfaces:
ApplicationEventPublisher, BeanFactory,
    EnvironmentCapable, HierarchicalBeanFactory,
    ListableBeanFactory, MessageSource,
    ResourceLoader, ResourcePatternResolver

Important Implementing Classes:
class org.springframework.context.support.FileSystemXmlApplicationContext{}
class org.springframework.context.support.ClassPathXmlApplicationContext{}
class org.springframework.context.annotation.AnnotationConfigApplicationContext{}
```

---

1. `BeanFactory`: `IoC`的基本实现，主要面向`spring`内部使用，而不是面向框架使用者使用
1. `ApplicationContext`: `BeanFactory`的子接口，提供了更多的特性，面向框架使用者使用

---

### 获取`bean`

```java {.line-numbers}
// spring-beans.jar
// Methods inherited from interface org.springframework.beans.factory.BeanFactory
public Object getBean(String name)
        throws BeansException
Return an instance, which may be shared or independent, of the specified bean.

public <T> T getBean(Class<T> requiredType)
       throws BeansException
Return the bean instance that uniquely matches the given object type, if any.
Throws NoUniqueBeanDefinitionException - if more than one bean of the given type was found
```

---

1. 获得了`ApplicationContext`的实例，就获得了`IoC`的引用
1. 通过`xml`、`config-class`或`annotation`将创建方式和依赖关系进行描述，然后由`IoC`管理（创建和注入）

---

```java {.line-numbers}
// without ioc
UserService userService = new UserServiceImpl();
UserDao userDao = new UserDaoImpl();
userService.setUserDao(userDao);
userService.save();

// with ioc
ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
UserService userService = app.getBean("userService");
userService.save();
```

---

### The Problems Solved by `IoC`

1. **loosing coupling降低耦合、动态绑定**：`IoC`降低对象间的依赖耦合，可实现动态绑定
1. **sharing component组件共享、组件管理**：共享`IoC`中的组件，组件的生命周期由`IoC`管理

`IoC`实现了组件的 **"管理"** 与 **"使用"** 的相分离

><https://www.liaoxuefeng.com/wiki/1252599548343744/1282381977747489>

---

1. 在`spring`组件称为`SpringBean`（简称`Bean`）
1. `IoC`是非侵入式的：组件无需实现`spring`的特定接口，即，组件本身不知道`IoC`的存在
    1. 既可以在`IoC`中创建初始化，也可以自行编码创建初始化
    1. 测试脚本既可以依赖`IoC`，也可以不依赖`IoC`

---

### 实现依赖配置的三种方式

1. `xml`
1. `config-class`
1. `annotation`
    1. `pure-annotation`
    1. `xml + annotation`
    1. `config-class + annotation`

---

#### `xml` config

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
 http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- services -->
    <bean id="userService" class="service.UserServiceImpl">
        <property name="userDao" ref="userDao"/>
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>
    <!-- more bean definitions for services go here -->
</beans>
```

---

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/aop
       http://www.springframework.org/schema/aop/spring-aop.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/context/spring-context.xsd">
</beans>
```

><http://www.springframework.org/schema/>

---

#### `config-class` config

```java {.line-numbers}
@Configuration
public class BeansConfig {

    @Bean("userDao")
    public UserDaoImpl userDao() {
        return new UserDaoImpl();
    }

    @Bean("userService")
    public UserServiceImpl userService() {
        UserServiceImpl userService = new UserServiceImpl();
        userService.setUserDao(userDao());
        return userService;
    }
}
```

---

#### `annotation` config

```java {.line-numbers}
@Service
public class UserServiceImpl {

    @Autowired
    private UserDaoImpl userDao;

    public List<User> findUserList() {
        return userDao.findUserList();
    }
}
```

---

```java {.line-numbers}
// pure-annotation
new AnnotationConfigApplicationContext("service");
```

```xml {.line-numbers}
<!-- xml + annotation -->
<context:component-scan base-package='service'/>
```

```java {.line-numbers}
// config-class + annotation
@Configuration
@ComponentScan
public class AppConfig {
}
```

---

### `<bean>`注册`bean`

1. `id`: `bean`在`ioc`中的唯一标识，也称为`name`或`beanName`
1. `class`: `bean`的全限定类名
1. `scope`
    1. `singleton`: 单例，配置文件加载时实例化, **_default_**
    1. `prototype`: 多例，调用`getBean()`时实例化
    1. `request`: 用于`web project`
    1. `session`: 用于`web project`

---

1. `autowire`: 设置自动装配策略
1. `init-method`: 设置`DI`之后调用的方法
1. `destroy-method`: 设置销毁前调用的方法

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Instantiation` and `Dependency Injection`

---

1. `instantiation实例化`: **_creating_** a variable form a type
1. `Dependency Injection依赖注入`
    1. `initialization初始化`: **_setting_** a variable's **_initial value_** , immediately after it was created
    1. `assignment赋值`: **_changing_** a variable's value, after it has been initialized

```java {.line-numbers}
User user = new User("张三");
user.setName("李四");
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Instantiation实例化

---

#### `Instantiation`的方式

1. `constructor`
    1. `NoArgsConstructor`
    1. `ArgsConstructor`
1. `factory`
    1. `factory method`
    1. `factory bean`
    1. `XyzFactoryBean implements FactoryBean<Xyz>`

---

#### `NoArgsConstructor`

```xml {.line-numbers}
<bean id="userDao"
        class="org.arm.courses.sf.ssm.spring.dao.impl.UserDaoImpl"/>
```

---

#### `ArgsConstructor`

```xml {.line-numbers}
<bean id="exampleUser"
        class="org.arm.courses.sf.ssm.spring.pojo.User">
    <constructor-arg name="name" value="zhangsan"/>
    <constructor-arg name="password" value="123456"/>
    <constructor-arg name="age" value="18"/>
</bean>
```

---

#### `static factory method`

```java {.line-numbers}
public class UserDaoImplFactory {
    public static UserDao getUserDaoByFactoryMethod() {
        return new UserDaoImpl();
    }
}
```

```xml {.line-numbers}
<bean id="userDaoByStaticFactoryMethod"
        class="org.arm.courses.sf.ssm.spring.dao.impl.UserDaoImplFactory"
        factory-method="getUserDaoByFactoryMethod"/>
```

---

#### `factory bean`

```java {.line-numbers}
public class UserDaoImplFactory {
    public UserDao getUserDaoByFactoryBean() {
        return new UserDaoImpl();
    }
}
```

```xml {.line-numbers}
<bean id="userDaoImplFactory"
        class="org.arm.courses.sf.ssm.spring.schema.ioc.dao.UserDaoImplFactory"/>
<bean id="userDaoByFactoryBean"
        factory-bean="userDaoImplFactory"
        factory-method="getUserDaoByFactoryBean"/>
```

---

```java {.line-numbers}
public class UserDaoImplFactoryBean implements FactoryBean<UserDaoImpl> {
    @Override
    public UserDaoImpl getObject() throws Exception {
        return new UserDaoImpl();
    }

    @Override
    public Class<?> getObjectType() {
        return UserDaoImpl.class;
    }

    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
    }
}
```

---

#### `XyzFactoryBean implements FactoryBean<Xyz>`

```xml {.line-numbers}
<bean id="userDaoByFactoryBeanT"
    class="org.arm.courses.sf.ssm.spring.schema.ioc.dao.UserDaoImplFactoryBean"/>
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `DI`

<!-- ---

```java {.line-numbers}
public class UserServiceImpl implements UserService {
    @Override
    public void updateUser() {
        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = (UserDao) app.getBean("userDao");
        User user = (User) app.getBean("exampleUser");
        user.setId(1);
        userDao.updateUserById(user);
    }
}
```

```xml {.line-numbers}
<bean id="userService"
        class="org.arm.courses.sf.ssm.spring.service.impl.UserServiceImpl"/>
```

---

```java {.line-numbers}
@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:applicationContext.xml")
class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    void updateUser() {
        userService.updateUser();
        assertThat("always pass", anything());
    }
}
``` -->

---

#### `DI`的方式

1. `constructor-based injection`：通过有参`constructor`注入依赖，`xml config` or `@Autowired` at `constructor`
1. `setter-based injection`：通过`setter`注入依赖，`xml config` or `@Autowired` at `setter`
1. `field-based injection`：通过`reflection`注入依赖，`@Autowired` at `member field`，有时也称为`property-based injection`

❗ `DI`即由`IoC`自动从`container`中获取`bean`并将其初始化或赋值到另一个`bean`中 ❗

<!-- ---

### `p namespace`

```xml {.line-numbers}
xmlns:p="http://www.springframework.org/schema/p"
``` -->

---

```java {.line-numbers}
@Setter
@AllArgsConstructor
public class UserServiceImplWithDi implements UserService {
    private UserDao userDao;

    @Override
    public void updateUser(User user) {
        userDao.updateUserById(user, user.getId());
    }
}
```

---

##### `constructor-based injection`

```xml {.line-numbers}
<bean id="userServiceConstructorDi"
        class="org.arm.courses.sf.ssm.spring.service.impl.UserServiceImplWithDi">
    <constructor-arg name="userDao" ref="userDao"/>
</bean>
```

---

##### `setter-based injection`

```xml {.line-numbers}
<bean id="userServiceDi"
        class="org.arm.courses.sf.ssm.spring.service.impl.UserServiceImplWithDi">
    <property name="userDao" ref="userDao"/>
</bean>
```

❗ `<property>`代表的是`setter-based DI`，而不是`property-based DI`，建议不使用`property-based DI`这个术语，以免造成误解 ❗

---

1. `field字段`: a variable that holds data or state information
1. `attribute属性`: almost the same as `field`, **_usually but not-mandatory_** refer to `non-static field`
1. `property属性`: a combination of a `getter` method and a `setter` method that provides access to a private `field` or `attribute` of a class

---

>The Spring team generally advocates constructor injection, as it lets you implement application components as immutable objects and ensures that required dependencies are not null. Furthermore, constructor-injected components are always returned to the client (calling) code in a fully initialized state. As a side note, a large number of constructor arguments is a bad code smell, implying that the class likely has too many responsibilities and should be refactored to better address proper separation of concerns.
>><https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-factory-collaborators>

---

#### `DI`的数据类型

1. 字面量数据类型：基本数据类型及`String`
    1. 无特殊字符：`<property name="xxx" value="yyy" type="java.lang.String"/>`
    1. 有特殊字符：`<property name="xxx"><![CDATA[a < b]]></property>`
1. 引用数据类型
    1. 引用`bean`：`<property name="xxx" ref="yyy"/>`
    1. 内部`bean`：`<property name="xxx"> <bean class="zzz"> </bean> </property>`

❗ 内部`bean`只能在该`bean`内部使用 ❗

---

##### 集合数据类型 - `list`

```xml {.line-numbers}
<property name="strList">
    <list>
        <value>111</value>
        <value>222</value>
        <value>333</value>
        <value>444</value>
    </list>
</property>
```

---

```xml {.line-numbers}
<property name="userList">
    <list>
        <ref bean="user1"/>
        <ref bean="user2"/>
    </list>
</property>
```

---

```xml {.line-numbers}
<util:list id="demoDoubleList">
    <value type="double">1.2341</value>
    <value type="double">1.2342</value>
    <value type="double">1.2343</value>
    <value type="double">1.2344</value>
</util:list>
```

---

##### 集合数据类型 - `map`

```xml {.line-numbers}
<property name="userMap">
    <map>
        <entry key="u1" value-ref="user1"></entry>
        <entry key="u2" value-ref="user2"></entry>
    </map>
</property>
```

---

```java {.line-numbers}
public void setProperties(Properties properties) {
    this.properties = properties;
}
```

```xml {.line-numbers}
<property name="properties">
    <props>
        <prop key="p1">aaa</prop>
        <prop key="p2">bbb</prop>
        <prop key="p3">ccc</prop>
    </props>
</property>
```

---

```xml {.line-numbers}
<property>
    <null/>
</property>
```

---

1. logic class
    1. `schema.ioc.dao.UserDaoImpl`
    1. `schema.ioc.service.UserServiceImpl`
1. config file
    1. `classpath:schema/applicationContext.xml`
1. test script
    1. `schema.ioc.service.UserServiceImplTest`

---

#### Autowire自动装配

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Annotation`实现`IoC`

---

### `XML`配置的优缺点

1. 优点
    1. 业务代码与依赖配置相分离
    1. 集中
1. 缺点
    1. 繁琐

---

### `Annotation`配置的优缺点

1. 优点
    1. 方便
1. 缺点
    1. 业务代码与依赖配置耦合
    1. 分散
    1. 无法为第三方依赖添加注解

---

### `Annotation`: 组件定义

1. `@Component`: `bean`
1. `@Controller`: `presentation-layer bean`
1. `@Service`: `business-layer bean`
1. `@Repository`: `persistence-layer bean`

❗ 语义不一样，功能一样 ❗

---

1. `@Scope`: `bean`的`scope`
1. `@PostConstruct`: 构造方法之后被调用
1. `@PreDestroy`: 对象被销毁前被调用

---

### `Annotation`: 依赖注入

1. `@Autowired`: 根据数据类型注入
1. `@Qualifier`: 根据`id`注入，结合`@Autowired`使用
1. `@Value`: 基本数据类型或`String`注入

---

```java {.line-numbers}
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {
  boolean required() default true;
}
```

---

1. `@Resource`
    1. 字段注解，根据`id`注入，相当于`@Autowired`+`@Qualifier`
    1. `javax.annotation`

```xml {.line-numbers}
<dependency>
    <groupId>javax.annotation</groupId>
    <artifactId>javax.annotation-api</artifactId>
    <version>{javax-version}</version>
</dependency>
```

---

```java {.line-numbers}
@Target({TYPE, FIELD, METHOD})
@Retention(RUNTIME)
public @interface Resource {
    String name() default "";
    // omit others...
}
```

---

### `XML + Annotation`

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                            http://www.springframework.org/schema/beans/spring-beans.xsd
                            http://www.springframework.org/schema/aop
                            http://www.springframework.org/schema/aop/spring-aop.xsd
                            http://www.springframework.org/schema/context
                            http://www.springframework.org/schema/context/spring-context.xsd">
    <context:component-scan base-package="xxx.yyy"/>
</beans>
```

---

### `Config-Class + Annotation`

1. `@Configuration`: 定义一个配置类
1. `@ComponentScan`: 自动扫描当前类所在包及其子包，将所有标注为`@Component`的`bean`创建并根据`@Autowired`进行装配
1. `@Bean`: 标注该方法的返回值将存储到`ioc`中
1. `@PropertySource`: 加载`properties-file`中的`key-value`
1. `@Import`: 导入其他配置类

---

```java {.line-numbers}
@Configuration
@ComponentScan
public class AppConfig {
}
```

```java {.line-numbers}
@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(classes = AppConfig.class)
class UserServiceImplTest {
}
```

---

1. logic class
    1. `annotation.ioc.dao.UserDaoImpl`
    1. `annotation.ioc.service.UserServiceImpl`
1. config class
    1. `annotation.ioc.AppConfig`
1. test script
    1. `annotation.ioc.service.UserServiceImpl`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `AOP`

---

1. `AOP`最早由`AOP Alliance`提出，`spring`将其引入
1. `AOP`将系统分解为不同的关注点，即，`aspect切面`
1. `AOP`主要用于实现与业务逻辑不相关或固定的"增强功能"，如：日志、安全、事务等
1. `AOP`主要解决代码重复的问题
1. `spring aop`本质是`proxy`

---

![height:520](./.assets/image/spring-framework-aop-2.png)

>[pdai. Spring核心之面向切面编程（AOP）](https://pdai.tech/md/spring/spring-x-framework-helloworld.html)

---

### `Java`平台`AOP`的三种方式

1. `compile编译期`：在编译时，由编译器将`aspect`调用编译进`bytecode`，这种方式需要定义新的关键字并扩展编译器，`AspectJ`扩展了`java compiler`，使用关键字`aspect`实现织入
1. `loading类加载器`：在`bytecode`被`load`进`jvm`时，通过一个特殊的`class loader`，对`bytecode`实现织入
1. `runtime运行期`：`bytecode`和`aspect`都是普通`class`，通过`jvm`的动态代理功能或者第三方库实现运行期动态织入

---

### `AOP`的相关概念

1. `aspect（切面）`：一个横跨多个业务逻辑的功能，或称为关注点，`joinpoint`、`pointcut`和`advice`的组合
    1. `joinpoint（连接点）`：程序的插入点
    1. `pointcut（切入点）`：一组符合某种规则的`joinpoint`的集合
    1. `advice（通知/增强）`：连接点上执行的动作
1. `weaving（织入）`：将切面整合到程序最终的执行流程中
1. `interceptor（拦截器）`：一种实现增强的方式
1. `target object（目标对象）`：执行业务逻辑的对象
1. `aop Proxy（aop代理）`：客户端/调用端持有的增强后的对象引用

<!-- 1. `introduction（引介）`：为一个已有的对象动态增加新的接口 -->

---

### `advice`的类型

1. before
    1. `@Before`
1. after
    1. `@After`
    1. `@AfterReturning`
    1. `@AfterThrowing`
1. before + after
    1. `@Around`

>[通知类型.](https://pdai.tech/md/spring/spring-x-framework-aop.html#aop%E6%9C%AF%E8%AF%AD)

---

### `AOP`的配置方式

1. `xml`
1. `@AspectJ annotation`

<!-- ---

### `Spring AOP` VS `AspectJ` -->

---

### `Pointcut` rules

```java {.line-numbers}
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern?
            name-pattern(param-pattern) throws-pattern?)
//The execution of any public method:
execution(public * *(..))
//The execution of any method with a name that begins with set:
execution(* set*(..))
//The execution of any method defined by the AccountService interface:
execution(* com.xyz.service.AccountService.*(..))
//The execution of any method defined in the service package:
execution(* com.xyz.service.*.*(..))
//The execution of any method defined in the service package or one of its sub-packages:
execution(* com.xyz.service..*.*(..))
```

>[spring.io. aop-pointcuts-examples.](https://docs.spring.io/spring-framework/docs/5.3.25/reference/html/core.html#aop-pointcuts-examples)

---

1. `LoggingAspect`
1. `AopDemo`
1. `aopDemoContext.xml`
1. `AopDemoTest`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Annotation`实现`AOP`

---

1. `spring`使用`AspectJ`为`AOP`提供的`annotation`
1. `spring`对实现了`interface`的`class`使用`jdk dynamic proxy`，否则，使用`cglib dynamic proxy`
    1. `jdk dynamic proxy`: 底层通过`reflection`实现
    1. `cglib dynamic proxy`: 底层通过继承实现，如果`bean`定义为`final`，则`cglib`无法为其创建`subclass`

---

1. `@Aspect`
1. `@Pointcut`
1. `@Before`
1. `@After`
1. `@AfterReturning`
1. `@After-Throwing`
1. `@Around`

---

1. aspect class
    1. `annotation.aop.LoggingAspect`
1. logic class
    1. `annotation.aop.AopDemo`
1. config file
    1. `annotation/aopDemoContext.xml`
1. test script
    1. `annotation.aop.AopDemoTest`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
