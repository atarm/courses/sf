# Spring Framework

## Futhermore

### Standards and Specifications

<!-- {standards, specifications, conventions goes here} -->

1. [spring-projects/spring-framework 5.3.x](https://github.com/spring-projects/spring-framework/tree/5.3.x)

### Documents and Supports

<!-- {trusted or official documents goes here} -->

1. 🏛️ [spring-framework docs archive.](https://docs.spring.io/spring-framework/docs/)
    1. [Spring Framework Documentation 5.3.25](https://docs.spring.io/spring-framework/docs/5.3.25/reference/html/)
    1. [Spring Framework 5.3.25 API](https://docs.spring.io/spring-framework/docs/5.3.25/javadoc-api/)

### Manuals and CheatSheets

<!-- {manuals goes here} -->

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses and tutorials goes here} -->

1. 👍 [Spring基础 - Spring和Spring框架组成.](https://pdai.tech/md/spring/spring-x-framework-introduce.html)

### Papers and Articles

<!-- {special topics goes here} -->

1. [MyBatis在Spring环境下的事务管理.](https://juejin.cn/post/6844903464296939528)

### Playgrounds and Exercises

<!-- {exercises here} -->

### Examples and Templates

<!-- {examples here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

### Miscellaneous

<!-- {misc here} -->
