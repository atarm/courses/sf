# Spring

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [指引](#指引)
    1. [内容01：修改`maven`父项目配置](#内容01修改maven父项目配置)
    2. [内容02：创建并配置`maven`子项目](#内容02创建并配置maven子项目)
    3. [内容03：编写`pojo class`](#内容03编写pojo-class)
    4. [内容04：编写`dao interface`和`service interface`](#内容04编写dao-interface和service-interface)
    5. [内容05：`xml`方式实现`IoC`并编写运行`test script`](#内容05xml方式实现ioc并编写运行test-script)
    6. [内容06：`annotation`方式实现`IoC`并编写运行`test script`](#内容06annotation方式实现ioc并编写运行test-script)
    7. [内容07：`xml`方式实现`AOP`并编写运行`test script`](#内容07xml方式实现aop并编写运行test-script)
    8. [内容08：`annotation`方式实现`AOP`并编写运行`test script`](#内容08annotation方式实现aop并编写运行test-script)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`spring framework`的核心概念`IoC`和`AOP`
    1. 掌握`xml`和`annotation`配置`IoC`和`AOP`
    1. 能运用`spring framework`的`IoC`和`AOP`设计开发`service`和`dao`层
1. 内容与要求：
    1. 内容01：修改`maven`父项目配置
    1. 内容02：创建并配置`maven`子项目
    1. 内容03：编写`pojo class`
    1. 内容04：编写`dao interface`和`service interface`
    1. 内容05：`xml`方式实现`IoC`并编写运行`test script`
    1. 内容06：`annotation`方式实现`IoC`并编写运行`test script`
    1. 内容07：`xml`方式实现`AOP`并编写运行`test script`
    1. 内容08：`annotation`方式实现`AOP`并编写运行`test script`
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`IDEA`或其他`Java`开发环境、`jdk8`或以上、`mysql:5.7`或以上

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 指引

### 内容01：修改`maven`父项目配置

在原父项目`pom.xml`加入`spring-context`、`spring-aspects`和`spring-test`依赖声明，参考如下：

❗ 该部分的配置是在原父项目`pom.xml`的基础上进行添加的配置 ❗

```xml {.line-numbers}
<properties>
    <!--region dependency.springframework.version-->
    <springframework.version>5.3.25</springframework.version>
    <!--endregion dependency.springframework.version-->
</properties>

<dependencyManagement>
    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${springframework.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-aspects -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
            <version>${springframework.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-test -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${springframework.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

### 内容02：创建并配置`maven`子项目

创建一个`maven`子项目，`artifactId`为`ssm-experiments-mybatis`，位于`ssm-experiments`所在目录之下，父项目、坐标、打包类型配置参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<parent>
    <groupId>org.{fn_and_en}.courses.sf</groupId>
    <artifactId>ssm-experiments</artifactId>
    <version>1.0-SNAPSHOT</version>
</parent>

<artifactId>ssm-experiments-spring</artifactId>
<packaging>jar</packaging>
```

在子项目中定义依赖，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aspects</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
    </dependency>
</dependencies>
```

### 内容03：编写`pojo class`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.pojo;

import lombok.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private String password;
}
```

### 内容04：编写`dao interface`和`service interface`

`dao interface`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.dao;

import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;

public interface UserDao {
    int updateUserById(User user);
}
```

`service interface`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.service;

import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;

public interface UserService {
    boolean updateUserById(User user, int id);
}
```

### 内容05：`xml`方式实现`IoC`并编写运行`test script`

`dao class`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.schema.ioc.dao;

import org.{fn_and_en}.courses.sf.ssm.spring.dao.UserDao;
import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;

public class UserDaoImpl implements UserDao {
    @Override
    public int updateUserById(User user) {
        System.out.println("updateUserById() running...");
        return 1;
    }
}
```

`service class`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.schema.ioc.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.{fn_and_en}.courses.sf.ssm.spring.dao.UserDao;
import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;
import org.{fn_and_en}.courses.sf.ssm.spring.service.UserService;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    @Override
    public boolean updateUserById(User user, int id) {
        user.setId(id);
        return userDao.updateUserById(user) == 1;
    }
}
```

`config file`（路径：`resources/schema/iocDemoContext.xml`）参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="userDao"
          class="org.{fn_and_en}.courses.sf.ssm.spring.schema.ioc.dao.UserDaoImpl"/>
    <bean id="userService"
          class="org.{fn_and_en}.courses.sf.ssm.spring.schema.ioc.service.UserServiceImpl">
        <property name="userDao" ref="userDao"/>
    </bean>
</beans>
```

`test script`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.schema.ioc.service;

import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;
import org.{fn_and_en}.courses.sf.ssm.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:schema/iocDemoContext.xml")
class UserServiceImplTest {

    @Autowired
    UserService userService;

    @Test
    void updateUserById() {
        User user = new User("张三", "abcdefg");
        assertThat(userService.updateUserById(user, 1), is(true));
    }
}
```

### 内容06：`annotation`方式实现`IoC`并编写运行`test script`

`dao class`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.ioc.dao;

import org.{fn_and_en}.courses.sf.ssm.spring.dao.UserDao;
import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    @Override
    public int updateUserById(User user) {
        System.out.println("updateUserById() running...");
        return 1;
    }
}
```

`service class`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.ioc.service;

import org.{fn_and_en}.courses.sf.ssm.spring.dao.UserDao;
import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;
import org.{fn_and_en}.courses.sf.ssm.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public boolean updateUserById(User user, int id) {
        user.setId(id);
        return userDao.updateUserById(user) == 1;
    }
}
```

`config class`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.ioc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class IocAppConfig {
}
```

`test script`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.ioc.service;

import org.{fn_and_en}.courses.sf.ssm.spring.annotation.ioc.IocAppConfig;
import org.{fn_and_en}.courses.sf.ssm.spring.pojo.User;
import org.{fn_and_en}.courses.sf.ssm.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(classes = IocAppConfig.class)
class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Test
    void updateUserById() {
        User user = new User("张三", "abcdefg");
        assertThat(userService.updateUserById(user, 1), is(true));
    }
}
```

### 内容07：`xml`方式实现`AOP`并编写运行`test script`

在`resources`目录下增加`log4j2.xml`，参考如下：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
        </Console>
        <File name="File" fileName="target/logs/log.log">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
        </File>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
            <AppenderRef ref="File"/>
        </Root>
    </Loggers>
</Configuration>
```

`LoggingAspect`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.schema.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

public class LoggingAspect {
    private static final Logger logger = LogManager.getLogger(LoggingAspect.class);

    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("before method: " + methodName);
    }

    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("after(finally) method: " + methodName);
    }

    public void afterMethodReturning(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("after returning method: " + methodName);
    }

    public void afterMethodThrowing(JoinPoint joinPoint){
        String methodName = joinPoint.getSignature().getName();
        logger.info("after throwing method: " + methodName);
    }

    public Object aroundMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        String methodName = proceedingJoinPoint.getSignature().getName();
        logger.info("around-before method: " + methodName);
        Object ret = proceedingJoinPoint.proceed();
        logger.info("around-after method: " + methodName);
        return ret;
    }
}
```

`AopDemo`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.schema.aop;

public class AopDemo {
    public void method01() {
        System.out.println("in method01...");
    }

    public void method02() {
        System.out.println("in method02...");
    }

    public void method03() throws Exception{
        System.out.println("in method03...");
        throw new Exception("exception throws by method03...");
    }
}
```

`config file`（路径：`resources/schema/aopDemoContext.xml`）参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/aop
       http://www.springframework.org/schema/aop/spring-aop.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/context/spring-context.xsd">
    <bean id="logAspect"
          class="org.{fn_and_en}.courses.sf.ssm.spring.schema.aop.LoggingAspect"/>
    <!-- target object -->
    <bean id="aopDemo"
          class="org.{fn_and_en}.courses.sf.ssm.spring.schema.aop.AopDemo"/>
    <aop:config>
        <!-- aspect config -->
        <aop:aspect ref="logAspect">
            <!-- pointcut -->
            <aop:pointcut id="aopDemoPointCut"
                          expression="execution(* org.{fn_and_en}.courses.sf.ssm.spring.schema.aop.*.*(..))"/>
            <aop:before method="beforeMethod" pointcut-ref="aopDemoPointCut"/>
            <aop:after method="afterMethod" pointcut-ref="aopDemoPointCut"/>
            <aop:after-returning method="afterMethodReturning" pointcut-ref="aopDemoPointCut"/>
            <aop:after-throwing method="afterMethodThrowing" pointcut-ref="aopDemoPointCut"/>
            <aop:around method="aroundMethod" pointcut-ref="aopDemoPointCut"/>
        </aop:aspect>
    </aop:config>
</beans>
```

`test script`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.schema.aop;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:schema/aopDemoContext.xml")
class AopDemoTest {

    @Autowired
    AopDemo aopDemo;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void method01() {
        aopDemo.method01();
        assertThat("always pass", anything());
    }

    @Test
    void method02() {
        aopDemo.method02();
        assertThat("always pass", anything());
    }

    @Test
    void method03() {
        assertThrows(Exception.class, () -> aopDemo.method03());
    }
}
```

### 内容08：`annotation`方式实现`AOP`并编写运行`test script`

`LoggingAspect`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Component
@Aspect
@EnableAspectJAutoProxy
public class LoggingAspect {
    private static final Logger logger = LogManager.getLogger(LoggingAspect.class);

    @Pointcut("execution(* org.{fn_and_en}.courses.sf.ssm.spring.annotation.aop.AopDemo.*(..))")
    private void pointCutMethod() {
    }

    @Before("pointCutMethod()")
    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("before method: " + methodName);
    }

    @After("pointCutMethod()")
    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("after(finally) method: " + methodName);
    }


    @AfterReturning(pointcut = "pointCutMethod()", returning = "result")
    public void afterMethodReturning(JoinPoint joinPoint, String result) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("after returning method: " + methodName + " with return value: " + result);
    }

    @AfterThrowing(pointcut = "pointCutMethod()", throwing = "exception")
    public void afterMethodThrowing(JoinPoint joinPoint, Exception exception) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("after throwing method: " + methodName + " with throwing message " + exception.getMessage());
    }

    @Around("pointCutMethod()")
    public Object aroundMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        String methodName = proceedingJoinPoint.getSignature().getName();
        logger.info("around-before method: " + methodName);
        Object ret = proceedingJoinPoint.proceed();
        logger.info("around-after method: " + methodName);
        return ret;
    }
}
```

`AopDemo`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.aop;

import org.springframework.stereotype.Component;

@Component
public class AopDemo {
    public void method01() {
        System.out.println("in method01...");
    }

    public String method02() {
        System.out.println("in method02...");
        return "hello from method2...";
    }

    public void method03() throws Exception {
        System.out.println("in method03...");
        throw new Exception("exception throws by method03...");
    }
}
```

`AppConfig`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AopAppConfig {
}
```

`test script`参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.spring.annotation.aop;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(classes = AopAppConfig.class)
class AopDemoTest {

    @Autowired
    AopDemo aopDemo;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void method01() {
        aopDemo.method01();
        assertThat("always pass", anything());
    }

    @Test
    void method02() {
        aopDemo.method02();
        assertThat("always pass", anything());
    }

    @Test
    void method03() {
        assertThrows(Exception.class, () -> aopDemo.method03());
    }
}
```
