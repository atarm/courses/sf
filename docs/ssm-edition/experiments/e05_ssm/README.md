# SpringMVC-Spring-MyBatis Integration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [前置条件](#前置条件)
4. [指引](#指引)
    1. [内容01：修改`maven`父模块配置](#内容01修改maven父模块配置)
    2. [内容02：创建并配置`maven`子模块](#内容02创建并配置maven子模块)
    3. [内容03：编写`database.properties`配置文件](#内容03编写databaseproperties配置文件)
    4. [内容04：编写`spring-mybatis-config.xml`配置文件](#内容04编写spring-mybatis-configxml配置文件)
    5. [内容05：编写`log4j2.xml`配置文件](#内容05编写log4j2xml配置文件)
    6. [内容06：编写`springmvc-config.xml`配置文件](#内容06编写springmvc-configxml配置文件)
    7. [内容07：编写`web.xml`配置文件](#内容07编写webxml配置文件)
    8. [内容08：编写`entity`](#内容08编写entity)
    9. [内容09：编写`persistance`](#内容09编写persistance)
    10. [内容10：编写`service`](#内容10编写service)
    11. [内容11：编写`controller`](#内容11编写controller)
5. [运行](#运行)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`SpringMVC-Spring-MyBatis`框架的整合
    1. 能运用`SpringMVC-Spring-MyBatis`开发多层架构的`Java Web`应用程序
1. 内容与要求：
    1. 内容01：修改`maven`父项目配置
    1. 内容02：创建并配置`maven`子项目
    1. 内容03：编写`database.properties`配置文件
    1. 内容04：编写`spring-mybatis-config.xml`配置文件
    1. 内容05：编写`log4j2.xml`配置文件
    1. 内容06：编写`springmvc-config.xml`配置文件
    1. 内容07：编写`web.xml`配置文件
    1. 内容08：编写`entity`
    1. 内容09：编写`persistance`
    1. 内容10：编写`service`
    1. 内容11：编写`controller`
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`IDEA`或其他`Java`开发环境、`jdk8`或以上、`mysql:5.7`或以上

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"*

## 前置条件

1. 已安装`mysql:5.7`或以上
1. `windows`平台下具有数据库连接客户端（`cmd`易出现乱码问题）

## 指引

### 内容01：修改`maven`父模块配置

在原父模块`ssm-experiments`的`pom.xml`添加新的依赖声明，参考如下：

❗ 本节内容在`ssm-experiments`模块下操作，该部分的配置是在原父项目`pom.xml`的基础上进行添加的配置 ❗

```xml {.line-numbers}
<properties>
    <mybatis-spring.version>2.1.0</mybatis-spring.version>
    <druid.version>1.2.14</druid.version>
</properties>

<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${springframework.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>${mybatis-spring.version}</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>${druid.version}</version>
        </dependency>
    </dependencies>
</dependencyManagement>
```

### 内容02：创建并配置`maven`子模块

创建一个`maven`子模块，`artifactId`为`ssm-experiments-ssm`，位于`ssm-experiments`所在目录之下，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.{fn_and_en}.courses.sf</groupId>
        <artifactId>ssm-experiments</artifactId>
        <version>1.0-SNAPSHOT</version>
    </parent>

    <artifactId>ssm-experiments-ssm</artifactId>
    <packaging>war</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>
        <dependency>
            <groupId>jakarta.servlet</groupId>
            <artifactId>jakarta.servlet-api</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
    </dependencies>
</project>
```

### 内容03：编写`database.properties`配置文件

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

在`src/main/resources`目录下新建`database.properties`文件，参考如下（❗ 请根据实际情况修改`jdbc.username`和`jdbc.password` ❗）：

```properties
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/ums?serverTimezone=Asia/Shanghai&useSSL=false&useUnicode=true&characterEncoding=utf8
jdbc.username=root
jdbc.password=root
```

### 内容04：编写`spring-mybatis-config.xml`配置文件

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

在`src/main/resources`目录下新建`spring-mybatis-config.xml`文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/aop
       http://www.springframework.org/schema/aop/spring-aop.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/tx
       http://www.springframework.org/schema/tx/spring-tx.xsd">

    <!--region `dataSource`-->
    <context:property-placeholder location="classpath:database.properties"/>
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" destroy-method="close">
        <property name="driverClassName" value="${jdbc.driver}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean>
    <!--endregion `dataSource`-->

    <!--region config mybatis in `spring-mybatis-config.xml`-->
    <bean id="mybatisConfig" class="org.apache.ibatis.session.Configuration">
        <property name="logImpl" value="org.apache.ibatis.logging.log4j2.Log4j2Impl"/>
        <property name="useGeneratedKeys" value="true"/>
        <property name="mapUnderscoreToCamelCase" value="true"/>
    </bean>
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <property name="typeAliasesPackage" value="org.{fn_and_en}.courses.sf.ssm.ssm.pojo"/>
        <property name="configuration" ref="mybatisConfig"/>
    </bean>
    <!--endregion config mybatis in `spring-mybatis-config.xml`-->

    <!--region `MapperScannerConfigurer`-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
        <property name="basePackage" value="org.{fn_and_en}.courses.sf.ssm.ssm.dao"/>
    </bean>
    <!--endregion `MapperScannerConfigurer`-->

</beans>
```

### 内容05：编写`log4j2.xml`配置文件

在`src/main/resources`目录下，新建`log4j2.xml`文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n" charset="utf-8"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="INFO">
            <AppenderRef ref="Console"/>
        </Root>
        <Logger name="org.{fn_and_en}.courses.sf.ssm.ssm.dao" level="TRACE" additivity="true">
            <AppenderRef ref="Console"/>
        </Logger>
    </Loggers>
</Configuration>
```

### 内容06：编写`springmvc-config.xml`配置文件

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

在`src/main/resources`目录下，新建`springmvc-config.xml`文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <context:component-scan base-package="org.{fn_and_en}.courses.sf.ssm.ssm.controller"/>
    <context:component-scan base-package="org.{fn_and_en}.courses.sf.ssm.ssm.service"/>

    <!--region annotation-driven-->
    <!--will automate register corresponding `HandlerMapping` and `HandlerAdapter`-->
    <mvc:annotation-driven/>
    <!--endregion annotation-driven-->

    <!--region import spring-mybatis-config.xml-->
    <import resource="classpath:spring-mybatis-config.xml"/>
    <!--endregion import spring-mybatis-config.xml-->
</beans>
```

### 内容07：编写`web.xml`配置文件

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

在`src/main/webapp/WEB-INF`目录下，新建`web.xml`文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xsi:schemaLocation="
    http://xmlns.jcp.org/xml/ns/javaee
    http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <display-name>org-{fn_and_en}</display-name>

    <!--region characterEncodingFilter-->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!--endregion CharacterEncodingFilter-->

    <!--region dispatcherServlet-->
    <servlet>
        <servlet-name>dispatcherServlet</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-config.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcherServlet</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
    <!--endregion dispatcherServlet-->
</web-app>
```

### 内容08：编写`entity`

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

新建`User`类，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.ssm.pojo;

import lombok.*;
import org.apache.ibatis.type.Alias;

@Data
@Alias("User")
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private String password;
}
```

### 内容09：编写`persistance`

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

新建`UserMapper`接口，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.ssm.dao;

import org.apache.ibatis.annotations.Param;
import org.{fn_and_en}.courses.sf.ssm.ssm.pojo.User;

import java.util.List;

public interface UserMapper {
    List<User> findAllUsers();

    User findUserById(@Param("id") int id);
}
```

新建`UserMapper.xml`文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.{fn_and_en}.courses.sf.ssm.ssm.dao.UserMapper">
    <select id="findAllUsers" resultType="User">
        select *
        from `t_user`
    </select>

    <select id="findUserById" parameterType="int" resultType="User">
        select *
        from `t_user`
        where id = #{id}
    </select>
</mapper>
```

### 内容10：编写`service`

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

新建`UserService`接口，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.ssm.service;

import org.{fn_and_en}.courses.sf.ssm.ssm.pojo.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    User getUserById(int id) throws Exception;

}
```

新建`UserServiceImpl`类，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.ssm.service;

import org.{fn_and_en}.courses.sf.ssm.ssm.dao.UserMapper;
import org.{fn_and_en}.courses.sf.ssm.ssm.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getAllUsers() {
        return userMapper.findAllUsers();
    }

    @Override
    public User getUserById(int id) throws Exception {

        User user = userMapper.findUserById(id);
        if (user == null) {
            throw new Exception("User not found");
        }
        return user;
    }
}
```

### 内容11：编写`controller`

❗ 本节内容在`ssm-experiments-ssm`模块下操作 ❗

新建`UserController`类，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.ssm.controller;

import org.{fn_and_en}.courses.sf.ssm.ssm.pojo.User;
import org.{fn_and_en}.courses.sf.ssm.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("")
    public List<User> getAllUsers() {
        return service.getAllUsers();
    }

    @GetMapping("/{id}")
    public Map<String, Object> getUserById(@PathVariable("id") int id) {
        HashMap<String, Object> map = new HashMap<>();
        try {
            User user = service.getUserById(id);
            map.put("user", user);
        } catch (Exception e) {
            map.put("message", e.getMessage());
        }
        return map;
    }
}
```

## 运行

```sql {.line-numbers}
-- ums_db.sql
DROP DATABASE IF EXISTS ums;

CREATE DATABASE ums;

USE ums;

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user`
(
    `id`       int(20)     NOT NULL AUTO_INCREMENT,
    `name`     varchar(20) NOT NULL,
    `password` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `t_user`(`name`,`password`)
values ('张三', '123456'),
       ('李四', '123456'),
       ('王五', '654321'),
       ('赵六', '654321');
```

1. 使用数据库客户端工具，连接到数据库，执行`ums_db.sql`（如上所示）中的SQL语句，创建数据库表和插入数据
1. 配置`IDE`中的`Tomcat`环境（将`Application Context`设置成`/`），启动`Tomcat`服务器
1. 运行`postman`或其他接口测试工具，访问以下`URL`
    1. <http://localhost:8080/api/users>，`GET`方法
    1. <http://localhost:8080/api/users/1>，`GET`方法
    1. <http://localhost:8080/api/users/2>，`GET`方法
    1. <http://localhost:8080/api/users/3>，`GET`方法
    1. <http://localhost:8080/api/users/4>，`GET`方法
    1. <http://localhost:8080/api/users/5>，`GET`方法
