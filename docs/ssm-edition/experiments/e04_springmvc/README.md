# SpringMVC

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [指引](#指引)
    1. [内容01：修改`maven`父项目配置](#内容01修改maven父项目配置)
    2. [内容02：创建并配置`maven`子项目](#内容02创建并配置maven子项目)
    3. [内容03：`web.xml`配置](#内容03webxml配置)
    4. [内容04：`index`](#内容04index)
    5. [内容05：文件上传](#内容05文件上传)
    6. [内容06：文件下载](#内容06文件下载)
    7. [内容07：`RESTful API`](#内容07restful-api)
4. [运行](#运行)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`SpringMVC`框架的基本使用
    1. 能运用`SpringMVC`开发`Java Web`应用程序
1. 内容与要求：
    1. 内容01：修改`maven`父模块配置
    1. 内容02：创建并配置`maven`子模块
    1. 内容03：`web.xml`配置
    1. 内容04：`index`
    1. 内容05：文件上传
    1. 内容06：文件下载
    1. 内容07：`RESTful API`
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`IDEA`或其他`Java`开发环境、`jdk8`或以上

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"*

## 指引

### 内容01：修改`maven`父模块配置

在原父模块`ssm-experiments`的`pom.xml`添加新的依赖声明，参考如下：

❗ 本节内容在`ssm-experiments`模块下操作，该部分的配置是在原父项目`pom.xml`的基础上进行添加的配置 ❗

```xml {.line-numbers}
<properties>
    <jstl.version>2.0.0</jstl.version>
    <taglibs.version>1.2.5</taglibs.version>
    <servlet.version>4.0.4</servlet.version>
    <commons-fileupload.version>1.5</commons-fileupload.version>
    <hibernate-validator.version>6.2.5.Final</hibernate-validator.version>
    <jackson-databind.version>2.13.5</jackson-databind.version>
</properties>

<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>${springframework.version}</version>
    </dependency>
    <dependency>
        <groupId>jakarta.servlet</groupId>
        <artifactId>jakarta.servlet-api</artifactId>
        <version>${servlet.version}</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>jakarta.servlet.jsp.jstl</groupId>
        <artifactId>jakarta.servlet.jsp.jstl-api</artifactId>
        <version>${jstl.version}</version>
    </dependency>
    <dependency>
        <groupId>org.apache.taglibs</groupId>
        <artifactId>taglibs-standard-impl</artifactId>
        <version>${taglibs.version}</version>
        <scope>runtime</scope>
    </dependency>
    <dependency>
        <groupId>org.apache.taglibs</groupId>
        <artifactId>taglibs-standard-spec</artifactId>
        <version>${taglibs.version}</version>
    </dependency>
    <dependency>
        <groupId>commons-fileupload</groupId>
        <artifactId>commons-fileupload</artifactId>
        <version>${commons-fileupload.version}</version>
    </dependency>
    <dependency>
        <groupId>org.hibernate.validator</groupId>
        <artifactId>hibernate-validator</artifactId>
        <version>${hibernate-validator.version}</version>
    </dependency>
    <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
        <version>${jackson-databind.version}</version>
    </dependency>
</dependencies>
```

### 内容02：创建并配置`maven`子模块

创建一个`maven`子模块，`artifactId`为`ssm-experiments-springmvc`，位于`ssm-experiments`所在目录之下，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.{fn_and_en}.courses.sf</groupId>
        <artifactId>ssm-experiments</artifactId>
        <version>1.0-SNAPSHOT</version>
    </parent>

    <artifactId>ssm-experiments-springmvc</artifactId>
    <packaging>war</packaging>

<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
    </dependency>
    <dependency>
        <groupId>jakarta.servlet</groupId>
        <artifactId>jakarta.servlet-api</artifactId>
    </dependency>
    <dependency>
        <groupId>jakarta.servlet.jsp.jstl</groupId>
        <artifactId>jakarta.servlet.jsp.jstl-api</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.taglibs</groupId>
        <artifactId>taglibs-standard-impl</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.taglibs</groupId>
        <artifactId>taglibs-standard-spec</artifactId>
    </dependency>
    <dependency>
        <groupId>commons-fileupload</groupId>
        <artifactId>commons-fileupload</artifactId>
    </dependency>
    <dependency>
        <groupId>org.hibernate.validator</groupId>
        <artifactId>hibernate-validator</artifactId>
    </dependency>
    <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
    </dependency>
</dependencies>
</project>
```

### 内容03：`web.xml`配置

❗ 本节内容在`ssm-experiments-springmvc`模块下操作 ❗

在`src/main/resources/`下新建`springmvc-config.xml`文件。

在`src/main/`下新建`webapp`目录，并在`webapp`目录下新建`WEB-INF`目录，然后在`WEB-INF`目录下新建`web.xml`文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xsi:schemaLocation="
    http://xmlns.jcp.org/xml/ns/javaee
    http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <display-name>org-{fn_and_en}</display-name>

    <!--region characterEncodingFilter-->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!--endregion CharacterEncodingFilter-->

    <!--region dispatcherServlet-->
    <servlet>
        <servlet-name>dispatcherServlet</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-config.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>dispatcherServlet</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
    <!--endregion dispatcherServlet-->
</web-app>
```

### 内容04：`index`

❗ 本节内容在`ssm-experiments-springmvc`模块下操作 ❗

新建`IndexService`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class IndexService {
    public Map<String, String> getDefault() {
        HashMap<String, String> ret = new HashMap<String, String>();
        ret.put("message","hello, i am {fn_and_en}...");
        return ret;
    }
}
```

新建`IndexController`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.controller;

import org.{fn_and_en}.courses.sf.ssm.springmvc.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @Autowired
    private IndexService service;

    @RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index", service.getDefault());
    }
}
```

在`webapp/WEB-INF`下新建`views/index.jsp`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```jsp {.line-numbers}
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>index</title>
</head>
<body>
${message}
</body>
</html>
```

修改`springmvc-config.xml`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <context:component-scan base-package="org.{fn_and_en}.courses.sf.ssm.springmvc.controller"/>
    <context:component-scan base-package="org.{fn_and_en}.courses.sf.ssm.springmvc.service"/>

    <!--region annotation-driven-->
    <!--will automate register corresponding `HandlerMapping` and `HandlerAdapter`-->
    <mvc:annotation-driven/>
    <!--endregion annotation-driven-->

    <!--region viewResolver-->
    <bean id="viewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/views/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
    <!--endregion viewResolver-->
</beans>
```

创建并配置一个新的`Tomcat Local`环境（❗ 请将`Deployment`标签页下的`Application context`设置为`/` ❗）

### 内容05：文件上传

❗ 本节内容在`ssm-experiments-springmvc`模块下操作 ❗

新建`FileUploadService`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class FileUploadService {

    public static String generateFileName(String filename) {
        String formatDate = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
        int random = new Random().nextInt(10000);
        return formatDate + "_" + random + "_" + filename;
    }

    public void saveFile(MultipartFile multipartFile, String targetDirectory) throws IOException {
        String originalFilename = multipartFile.getOriginalFilename();
        assert originalFilename != null;
        String targetFileName = generateFileName(originalFilename);

        //region save the file to the target directory
        File target = new File(targetDirectory, targetFileName);
        FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), target);
        //endregion save the file to the target directory

        //region insert the file infos into database

        //endregion insert the file infos into database
    }
}
```

新建`FileUploadController`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.controller;

import org.{fn_and_en}.courses.sf.ssm.springmvc.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RequestMapping("/file/upload")
@Controller
public class FileUploadController {

    public static final String FILE_LOC = "/file/upload";

    @Autowired
    private FileUploadService service;

    @RequestMapping("")
    public String fileUpload() {
        return "/file/upload/index";
    }

    @PostMapping("/upload")
    public ModelAndView upload(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("/file/upload/result");
        if (multipartFile == null || multipartFile.isEmpty()) {
            modelAndView.addObject("message", "未选择文件");
        } else {
            try {
                service.saveFile(multipartFile, request.getSession().getServletContext().getRealPath(FILE_LOC));
                modelAndView.addObject("message", "上传成功");
            } catch (IOException e) {
                e.printStackTrace();
                modelAndView.addObject("message", "上传失败" + e.getMessage());
            }
        }
        return modelAndView;
    }
}
```

修改`springmvc-config.xml`，在`<beans>`标签内添加内容如下：

```xml {.line-numbers}
<!--region multipartResolver-->
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
    <property name="maxUploadSize" value="10485760"/> <!--10M-->
    <property name="defaultEncoding" value="UTF-8"/>
</bean>
<!--endregion multipartResolver-->
```

在`webapp/WEB-INF`下新建`views/file/upload/index.jsp`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```jsp {.line-numbers}
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>Upload</title>
</head>
<body>
<h2>文件上传</h2>
<form action="${pageContext.request.contextPath}/file/upload/upload" method="post" enctype="multipart/form-data">
    <p>请选择文件：<input type="file" name="file"></p>
    <p><input type="submit" value="上传"></p>
</form>
</body>
</html>
```

在`webapp/WEB-INF`下新建`views/file/upload/result.jsp`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```jsp {.line-numbers}
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>Upload Result</title>
</head>
<body>
<h1>上传结果：${message}</h1>
</body>
</html>
```

### 内容06：文件下载

❗ 本节内容在`ssm-experiments-springmvc`模块下操作 ❗

新建`FileDownloadService`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.service;

import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileDownloadService {

    public Map<String, ArrayList<String>> getFiles(String directoryPath) {
        File directory = new File(directoryPath);
        File[] files = directory.listFiles();
        ArrayList<String> filenames = new ArrayList<String>();
        if (files != null) {
            for (File file : files) {
                filenames.add(file.getName());
            }
        }
        HashMap<String, ArrayList<String>> ret = new HashMap<>();
        ret.put("filenames", filenames);
        return ret;
    }

    public void downloadFile(File file, ServletOutputStream out) throws IOException {
        FileInputStream in = new FileInputStream(file);
        int count = 0;
        byte[] b = new byte[1024];
        while ((count = in.read(b)) != -1) {
            out.write(b, 0, count);
        }
        out.flush();
        in.close();
    }
}
```

新建`FileDownloadController`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.controller;

import org.{fn_and_en}.courses.sf.ssm.springmvc.service.FileDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@RequestMapping("/file/download")
@Controller
public class FileDownloadController {

    public static final String FILE_LOC = "/file/download";

    @Autowired
    private FileDownloadService service;

    @GetMapping("")
    public ModelAndView listFiles(HttpServletRequest request) {
        String directory = request.getSession().getServletContext().getRealPath(FILE_LOC);
        return new ModelAndView("file/download/index", service.getFiles(directory));
    }

    @GetMapping("/download")
    public void download(@RequestParam String filename,
                         HttpServletRequest request, HttpServletResponse response) {
        File file = new File(request.getSession().getServletContext().getRealPath(FILE_LOC), filename);
        try {
            // region http-header
            // get the file content-type and set it as the response's content-type
            response.setHeader("Content-Type", Files.probeContentType(file.toPath()));
            response.setHeader("Content-Disposition", "attachment; filename="
                + new String(filename.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));
            // endregion http-header
            ServletOutputStream out = response.getOutputStream();
            service.downloadFile(file, out);
            out.flush();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
```

在`webapp/WEB-INF`下新建`views/file/download/index.jsp`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```jsp {.line-numbers}
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>Files</title>
</head>
<body>
<table>
    <tr>
        <td>下载文件：</td>
    </tr>
    <c:forEach items="${filenames}" var="filename">
        <tr>
            <td>
                <a href="${pageContext.request.contextPath}/file/download/download?filename=${filename}">${filename}</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
```

在`webapp/`下新建两个文本文件`file/download/file-01.txt`和`file/download/file-02.txt`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```text {.line-numbers}
this is file-01.txt, i am {fn_and_en}...
```

```text {.line-numbers}
this is file-02.txt, i am {fn_and_en}...
```

### 内容07：`RESTful API`

❗ 本节内容在`ssm-experiments-springmvc`模块下操作 ❗

新建`User`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String username;
    private String password;
    private String email;
    private String phone;
}
```

新建`ApiUserService`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.service;

import org.{fn_and_en}.courses.sf.ssm.springmvc.pojo.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ApiUserService {

    private final List<User> users;

    public ApiUserService() {
        users = new ArrayList<>();
        users.add(new User("admin", "123456", "admin@example.com", "13812345678"));
        users.add(new User("user", "123456", "user@example.com", "13712345678"));
    }

    public List<User> getUsers() {
        return users;
    }

    public User getUser(int id) throws Exception {
        if (id < 0) {
            throw new Exception("id must be positive.");
        }
        if (id >= users.size()) {
            throw new Exception("user not found.");
        }
        return users.get(id);
    }

    public User login(String email, String password) throws Exception {

        for (User user : users) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return user;
            }
        }
        throw new Exception("login failed.");
    }
}
```

新建`ApiUserController`，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.springmvc.controller;

import org.{fn_and_en}.courses.sf.ssm.springmvc.pojo.User;
import org.{fn_and_en}.courses.sf.ssm.springmvc.service.ApiUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ApiUserController {
    @Autowired
    private ApiUserService service;

    @GetMapping("/users")
    public List<User> users() {
        return service.getUsers();
    }

    @GetMapping("/users/{id}")
    public Map<String, Object> user(@PathVariable("id") int id) {
        HashMap<String, Object> ret = new HashMap<>();
        try {
            User user = service.getUser(id);
            ret.put("success", true);
            ret.put("user", user);
        } catch (Exception e) {
            ret.put("success", false);
            ret.put("message", e.getMessage());
        }
        return ret;
    }

    @PostMapping("/login")
    public Map<String, Object> login(@RequestBody LoginRequest loginRequest) {
        HashMap<String, Object> map = new HashMap<>();
        try {
            User user = service.login(loginRequest.email, loginRequest.password);
            map.put("user", user);
        } catch (Exception e) {
            map.put("error", "LOGIN_FAILED");
            map.put("message", e.getMessage());
        }
        return map;
    }

    public static class LoginRequest {
        public String email;
        public String password;
    }
}
```

## 运行

1. 内容04：`index`
    1. 访问<http://localhost:8080/>
1. 内容05：文件上传
    1. 访问<http://localhost:8080/file/upload>
    1. 选择文件并上传
1. 内容06：文件下载
    1. 访问<http://localhost:8080/file/download>
    1. 点击文件名下载
1. 内容07：`RESTful API`
    1. 使用`Postman`或其他类似工具，访问<http://localhost:8080/api/users>，使用`GET`方法访问服务端
    1. 使用`Postman`或其他类似工具，访问<http://localhost:8080/api/users/0>，使用`GET`方法访问服务端
    1. 使用`Postman`或其他类似工具，访问<http://localhost:8080/api/users/1>，使用`GET`方法访问服务端
    1. 使用`Postman`或其他类似工具，访问<http://localhost:8080/api/users/2>，使用`GET`方法访问服务端
    1. 使用`Postman`或其他类似工具，访问<http://localhost:8080/api/login>，使用`POST`方法访问服务端，`Content-Type`为`application/json`，`Body`为`{ "email": "user@example.com", "password": "123456" }`
    1. 使用`Postman`或其他类似工具，访问<http://localhost:8080/api/login>，使用`POST`方法访问服务端，`Content-Type`为`application/json`，`Body`为`{ "email": "user@example.com", "password": "12345678" }`
