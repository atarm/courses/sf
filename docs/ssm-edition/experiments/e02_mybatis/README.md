# MyBatis

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本信息Basic Information](#基本信息basic-information)
2. [说明及注意事项Cautions](#说明及注意事项cautions)
3. [前置条件](#前置条件)
4. [指引](#指引)
    1. [内容01：数据环境准备](#内容01数据环境准备)
    2. [内容02：创建并配置`maven`项目](#内容02创建并配置maven项目)
        1. [创建`maven`父工程项目](#创建maven父工程项目)
        2. [创建`maven`子工程项目](#创建maven子工程项目)
        3. [配置`maven`父工程项目`pom.xml`](#配置maven父工程项目pomxml)
        4. [配置`maven`子工程项目`pom.xml`](#配置maven子工程项目pomxml)
    3. [内容03：配置`mybatis`配置文件](#内容03配置mybatis配置文件)
        1. [创建并配置数据库连接配置文件`database.properties`](#创建并配置数据库连接配置文件databaseproperties)
        2. [创建并配置日志配置文件`log4j2.xml`](#创建并配置日志配置文件log4j2xml)
        3. [创建并配置`mybatis`配置文件`mybatis-config.xml`](#创建并配置mybatis配置文件mybatis-configxml)
    4. [内容04：编写`entity`和`mapper`](#内容04编写entity和mapper)
        1. [编写`entity`-`Bill`](#编写entity-bill)
        2. [编写`entity`：`Provider`](#编写entityprovider)
        3. [编写`mapper interface`：`ProviderMapper`](#编写mapper-interfaceprovidermapper)
        4. [编写`mapper file`：`ProviderMapper.xml`](#编写mapper-fileprovidermapperxml)
        5. [编写`mapper interface`：`BillMapper`](#编写mapper-interfacebillmapper)
        6. [编写`mapper file`：`BillMapper.xml`](#编写mapper-filebillmapperxml)
        7. [编写工具类`MyBatisUtils`](#编写工具类mybatisutils)
    5. [内容05：编写测试脚本](#内容05编写测试脚本)
        1. [编写测试类`ProviderMapperTest`](#编写测试类providermappertest)
        2. [编写测试类`BillMapperTest`](#编写测试类billmappertest)
        3. [运行测试](#运行测试)

<!-- /code_chunk_output -->

## 基本信息Basic Information

1. 性质：设计型
1. 目的：
    1. 掌握`mybatis`
1. 内容与要求：
    1. 内容01：数据环境准备
    1. 内容02：创建并配置`maven`项目
    1. 内容03：配置`mybatis`配置文件
    1. 内容04：编写`entity`和`mapper`
    1. 内容05：编写测试脚本
1. 环境：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 系统环境：`Windows`或`Linux`或`MacOS`
    1. 开发环境：`IDEA`或其他`Java`开发环境、`jdk8`或以上、`mysql:5.7`或以上

## 说明及注意事项Cautions

1. 指引中使用 **尖括号`<>`表示键盘按键** ，例如：`<CTRL>`代表`control`键，`<j>`代表`j`键, `<CTRL - j>`代表同时按下`control`和`j`键
1. 指引中的 **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"**

## 前置条件

1. 已安装`mysql:5.7`或以上
1. `windows`平台下具有数据库连接客户端（`cmd`易出现乱码问题）

## 指引

### 内容01：数据环境准备

1. 连接`mysql`数据库（❗ 不建议使用`Windows`的`cmd`连接数据库，因为，可能会出现乱码问题 ❗）
1. 运行`sql script`创建数据库、数据表和测试数据

### 内容02：创建并配置`maven`项目

#### 创建`maven`父工程项目

创建一个`maven`父工程，`artifactId`为`ssm-experiments`，坐标和打包类型配置参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<groupId>org.{fn_and_en}.courses.sf</groupId>
<artifactId>ssm-experiments</artifactId>
<version>1.0-SNAPSHOT</version>
<packaging>pom</packaging>
```

#### 创建`maven`子工程项目

创建一个`maven`子工程，`artifactId`为`ssm-experiments-mybatis`，位于`ssm-experiments`所在目录之下，父项目、坐标、打包类型配置参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<parent>
    <groupId>org.{fn_and_en}.courses.sf</groupId>
    <artifactId>ssm-experiments</artifactId>
    <version>1.0-SNAPSHOT</version>
</parent>

<artifactId>ssm-experiments-mybatis</artifactId>
<packaging>jar</packaging>
```

#### 配置`maven`父工程项目`pom.xml`

配置子模块，参考如下：

```xml {.line-numbers}
<modules>
    <module>mybatis</module>
</modules>
```

配置构建参数，参考如下：

```xml {.line-numbers}
<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
```

配置依赖版本参数，参考如下：

```xml {.line-numbers}
<properties>
    <!--region dependency.mybatis.version-->
    <mybatis.version>3.5.7</mybatis.version>
    <mysql-connector-java.version>8.0.29</mysql-connector-java.version>
    <!--endregion dependency.mybatis.version-->

    <!--region dependency.commons.version-->
    <lombok.version>1.18.24</lombok.version>
    <junit.version>5.8.2</junit.version>
    <hamcrest.version>2.2</hamcrest.version>
    <rest-assured.version>5.3.0</rest-assured.version>
    <log4j.version>2.18.0</log4j.version>
    <!--endregion dependency.commons.version-->
</properties>
```

配置插件版本参数，参考如下：

```xml {.line-numbers}
<properties>
    <!--region plugin.version-->
    <plugin.surefire.version>3.0.0-M6</plugin.surefire.version>
    <!--endregion plugin.version-->
</properties>
```

配置构建，参考如下：

```xml {.line-numbers}
<build>
    <resources>
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.xml</include>
            </includes>
        </resource>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*</include>
            </includes>
        </resource>
    </resources>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>${plugin.surefire.version}</version>
        </plugin>
    </plugins>
</build>
```

配置依赖，参考如下：

```xml {.line-numbers}
<dependencyManagement>
    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.mybatis/mybatis -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>${mybatis.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>${mysql-connector-java.version}</version>
            <scope>runtime</scope>
        </dependency>
    </dependencies>
</dependencyManagement>

<dependencies>
    <!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api -->
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-api</artifactId>
        <version>${log4j.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>${log4j.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>${lombok.version}</version>
        <scope>provided</scope>
    </dependency>
    <!--region test-->
    <!-- https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter -->
    <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter</artifactId>
        <version>${junit.version}</version>
        <scope>test</scope>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.hamcrest/hamcrest -->
    <dependency>
        <groupId>org.hamcrest</groupId>
        <artifactId>hamcrest</artifactId>
        <version>${hamcrest.version}</version>
        <scope>test</scope>
    </dependency>
    <!--endregion test-->
</dependencies>
```

#### 配置`maven`子工程项目`pom.xml`

配置依赖，参考如下：

```xml {.line-numbers}
    <dependencies>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
    </dependencies>
```

### 内容03：配置`mybatis`配置文件

❗ 以下代码位于`src/main/resources`目录下 ❗

#### 创建并配置数据库连接配置文件`database.properties`

参考如下（❗ 请根据实际情况修改`jdbc.username`和`jdbc.password` ❗）：

```properties {.line-numbers}
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/smbms?serverTimezone=Asia/Shanghai&useSSL=false&useUnicode=true&characterEncoding=utf8
jdbc.username=root
jdbc.password=root
```

#### 创建并配置日志配置文件`log4j2.xml`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %l %msg%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="error">
            <AppenderRef ref="Console"/>
        </Root>
        <Logger name="org.{fn_and_en}.courses.sf.ssm.mybatis.dao" level="TRACE" additivity="false">
            <AppenderRef ref="Console"/>
        </Logger>
    </Loggers>
</Configuration>
```

#### 创建并配置`mybatis`配置文件`mybatis-config.xml`

配置文件框架，参考如下：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
    PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

</configuration>
```

配置外部`properties`，参考如下：

```xml {.line-numbers}
<properties resource="database.properties"/>
```

全局配置，参考如下：

```xml {.line-numbers}
<settings>
    <setting name="logImpl" value="LOG4J2"/>
    <setting name="autoMappingBehavior" value="FULL"/>
</settings>
```

配置别名，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<typeAliases>
    <package name="org.{fn_and_en}.courses.sf.ssm.mybatis.pojo"/>
</typeAliases>
```

配置数据库环境，参考如下：

```xml {.line-numbers}
<environments default="development">
    <environment id="development">
        <transactionManager type="JDBC"/>
        <dataSource type="POOLED">
            <property name="driver" value="${jdbc.driver}"/>
            <property name="url" value="${jdbc.url}"/>
            <property name="username" value="${jdbc.username}"/>
            <property name="password" value="${jdbc.password}"/>
        </dataSource>
    </environment>
</environments>
```

配置映射文件，参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<mappers>
    <package name="org.{fn_and_en}.courses.sf.ssm.mybatis.dao"/>
</mappers>
```

### 内容04：编写`entity`和`mapper`

❗ 以下代码位于`src/main/java`目录下 ❗

#### 编写`entity`-`Bill`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Alias("Bill")
public class Bill {
    private Integer id;
    private String billCode;
    private String productName;
    private String productDesc;
    private String productUnit;
    private BigDecimal productCount;
    private BigDecimal totalPrice;
    private Integer isPayment;
    private Integer createdBy;
    private Date creationDate;
    private Integer modifyBy;
    private Date modifyDate;
    private Provider provider;
}
```

#### 编写`entity`：`Provider`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Data
@Alias("Provider")
public class Provider {
    private Integer id;
    private String proCode;
    private String proName;
    private String proDesc;
    private String proContact;
    private String proPhone;
    private String proAddress;
    private String proFax;
    private Integer createdBy;
    private Date creationDate;
    private Integer modifyBy;
    private Date modifyDate;
}
```

#### 编写`mapper interface`：`ProviderMapper`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.dao;

import org.apache.ibatis.annotations.Param;
import org.{fn_and_en}.courses.sf.ssm.mybatis.pojo.Provider;

import java.util.List;

public interface ProviderMapper {
    int count();

    List<Provider> getProviders(@Param("proCode") String proCode, @Param("proName") String proName);

    int add(Provider provider);

    int update(Provider provider);

    int deleteById(@Param("id") Integer id);
}
```

#### 编写`mapper file`：`ProviderMapper.xml`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="org.{fn_and_en}.courses.sf.ssm.mybatis.dao.ProviderMapper">

    <select id="count" resultType="int">
        select count(*) as count
        from smbms_provider
    </select>

    <select id="getProviders" resultType="Provider">
        select * from smbms_provider
        <where>
            <if test="proCode != null and proCode != ''">
                and proCode like CONCAT ('%',#{proCode},'%')
            </if>
            <if test="proName != null and proName != ''">
                and proName like CONCAT ('%',#{proName},'%')
            </if>
        </where>
    </select>

    <insert id="add" parameterType="Provider">
        insert into smbms_provider (proCode, proName, proDesc, proContact, proPhone,
                                    proAddress, proFax, createdBy, creationDate)
        values (#{proCode}, #{proName}, #{proDesc}, #{proContact}, #{proPhone}, #{proAddress},
                #{proFax}, #{createdBy}, #{creationDate})
    </insert>

    <update id="update" parameterType="Provider">
        update smbms_provider
        <trim prefix="set" suffixOverrides="," suffix="where id = #{id}">
            <if test="proCode != null">proCode=#{proCode},</if>
            <if test="proName != null">proName=#{proName},</if>
            <if test="proDesc != null">proDesc=#{proDesc},</if>
            <if test="proContact != null">proContact=#{proContact},</if>
            <if test="proPhone != null">proPhone=#{proPhone},</if>
            <if test="proAddress != null">proAddress=#{proAddress},</if>
            <if test="proFax != null">proFax=#{proFax},</if>
            <if test="modifyBy != null">modifyBy=#{modifyBy},</if>
            <if test="modifyDate != null">modifyDate=#{modifyDate},</if>
        </trim>
    </update>

    <delete id="deleteById" parameterType="Integer">
        delete
        from smbms_provider
        where id = #{id}
    </delete>
</mapper>
```

#### 编写`mapper interface`：`BillMapper`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.dao;

import org.apache.ibatis.annotations.Param;
import org.{fn_and_en}.courses.sf.ssm.mybatis.pojo.Bill;

import java.util.List;

public interface BillMapper {

    List<Bill> getBills(@Param("productName") String productName, @Param("providerId") Integer providerId, @Param("isPayment") Integer isPayment);

    List<Bill> getBillsByProviderIds(@Param("proIds") Integer[] proIds);
}
```

#### 编写`mapper file`：`BillMapper.xml`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```xml {.line-numbers}
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="org.{fn_and_en}.courses.sf.ssm.mybatis.dao.BillMapper">
    <resultMap type="Bill" id="billMap">
        <id property="id" column="bid"/>
        <association property="provider" javaType="Provider">
            <id property="id" column="pid"/>
        </association>
    </resultMap>

    <select id="getBills" resultMap="billMap" flushCache="true">
        # select b.*,p.proName as providerName from `smbms_bill` as b,`smbms_provider` as p
        select b.id as bid, b.*,p.id as pid, p.* from `smbms_bill` as b,`smbms_provider` as p
        where b.providerId = p.id
        <if test="productName != null and productName != ''">
            and b.productName like CONCAT ('%',#{productName},'%')
        </if>
        <if test="providerId != null">
            and b.providerId = #{providerId}
        </if>
        <if test="isPayment != null">
            and b.isPayment = #{isPayment}
        </if>
    </select>

    <select id="getBillsByProviderIds" resultType="bill">
        select * from `smbms_bill`
        <if test="proIds != null">
            <foreach collection="proIds" item="proId" open="where providerId in(" separator="," close=")">
                <if test="proId != null">#{proId}</if>
            </foreach>
        </if>
    </select>
</mapper>
```

#### 编写工具类`MyBatisUtils`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MyBatisUtils {
    static SqlSessionFactory factory;

    // 获取SqlSessionFactory对象
    static {
        InputStream is = null;
        try {
            is = Resources.getResourceAsStream("mybatis-config.xml");
            factory = new SqlSessionFactoryBuilder().build(is);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public static SqlSession openSession() {
        return factory.openSession();
    }
}
```

### 内容05：编写测试脚本

❗ 以下代码位于`src/test/java`目录下 ❗

#### 编写测试类`ProviderMapperTest`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.dao;

import org.apache.ibatis.session.SqlSession;
import org.{fn_and_en}.courses.sf.ssm.mybatis.pojo.Provider;
import org.{fn_and_en}.courses.sf.ssm.mybatis.utils.MyBatisUtils;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProviderMapperTest {

    @Test
    void count() {
        try (SqlSession sqlSession = MyBatisUtils.openSession()) {
            int count = sqlSession.getMapper(ProviderMapper.class).count();
            assertThat(count, is(equalTo(15)));
        }
    }

    @Test
    void getProviders() {
        try (SqlSession sqlSession = MyBatisUtils.openSession()) {
            String proCode = "BJ";
            List<Provider> providerList = sqlSession.getMapper(ProviderMapper.class).getProviders(proCode, null);
            assertThat(providerList.size(), is(equalTo(4)));
            providerList.forEach(System.out::println);
        }
    }

    @Test
    void update() {
        try (SqlSession sqlSession = MyBatisUtils.openSession()) {
            Provider provider = new Provider();
            provider.setId(14);
            provider.setProContact("张扬");
            provider.setProAddress("供应商测试地址修改");
            provider.setModifyBy(1);
            provider.setModifyDate(new Date());
            int affected_rows = sqlSession.getMapper(ProviderMapper.class).update(provider);
            assertThat(affected_rows, is(equalTo(1)));
        }
    }

    @Test
    void deleteById() {
        try (SqlSession sqlSession = MyBatisUtils.openSession()) {
            assertThrows(org.apache.ibatis.exceptions.PersistenceException.class, () -> sqlSession.getMapper(ProviderMapper.class).deleteById(8));
        }
    }
}
```

#### 编写测试类`BillMapperTest`

参考如下（❗ **`"{fn_and_en}"`** 请修改成您的 **姓名全名的拼音** 以及  **学号末两位** ，例如 **"zhangsan68"** ❗）：

```java {.line-numbers}
package org.{fn_and_en}.courses.sf.ssm.mybatis.dao;

import org.apache.ibatis.session.SqlSession;
import org.{fn_and_en}.courses.sf.ssm.mybatis.pojo.Bill;
import org.{fn_and_en}.courses.sf.ssm.mybatis.utils.MyBatisUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


class BillMapperTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getBills() {
        try (SqlSession sqlSession = MyBatisUtils.openSession()) {
            List<Bill> billList = sqlSession.getMapper(BillMapper.class).getBills(null, null, 2);
            assertThat(billList.size(), is(equalTo(15)));
            billList.forEach(System.out::println);
        }
    }

    @Test
    void getBillsByProviderIds() {
        try (SqlSession sqlSession = MyBatisUtils.openSession()) {
            BillMapper billMapper = sqlSession.getMapper(BillMapper.class);
            List<Bill> billList = billMapper.getBillsByProviderIds(new Integer[]{1, 14, 100});
            assertThat(billList.size(), is(equalTo(6)));
            billList.forEach(System.out::println);
            billList = billMapper.getBillsByProviderIds(null);
            assertThat(billList.size(), is(equalTo(18)));
            billList.forEach(System.out::println);
        }
    }
}
```

#### 运行测试

在父项目或子项目中运行`mvn clean test`
