# 软件框架技术Software Framework

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于本仓库About](#关于本仓库about)
    1. [许可协议Licenses](#许可协议licenses)
    2. [目录组织Structure](#目录组织structure)
    3. [`emoji`](#emoji)
    4. [帮助完善Contribution](#帮助完善contribution)
2. [Futhermore](#futhermore)
    1. [Standards and Specifications](#standards-and-specifications)
    2. [Documents and Supports](#documents-and-supports)
    3. [Manuals and CheatSheets](#manuals-and-cheatsheets)
    4. [Books and Monographs](#books-and-monographs)
    5. [Courses and Tutorials](#courses-and-tutorials)
    6. [Papers and Articles](#papers-and-articles)
    7. [Playgrounds and Exercises](#playgrounds-and-exercises)
    8. [Examples and Templates](#examples-and-templates)
    9. [Auxiliaries and Tools](#auxiliaries-and-tools)
    10. [Miscellaneous](#miscellaneous)

<!-- /code_chunk_output -->

## 关于本仓库About

### 许可协议Licenses

![CC-BY-SA-4.0](./.assets/LICENSES/cc-by-sa-4.0/image/CC_BY-SA_88x31.png)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

<!--

### 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>
-->

### 目录组织Structure

```bash {.line-numbers}
.
├── .editorconfig           ==> EditorConfig
├── .git                    ==> git repository
├── .gitignore              ==> gitignore
├── README.md               ==> 本文件
├── codes                   ==> 代码
│   ├── sprintboot-edition
│   └── ssm-edition
└── docs                    ==> 文档
    ├── commons             ==> 公共
    ├── sprintboot-edition  ==> SpringBoot版
    └── ssm-edition         ==> SSM版
```

```bash {.line-numbers}
docs
├── xxx-edition
    ├── abouts          ==> 相关介绍，如：大纲等
    ├── addons          ==> 附加内容，如：专题论述等
    ├── exercises       ==> 习题及其解析
    ├── experiments     ==> 实践指引及FAQ
    ├── glossaries      ==> 术语表
    ├── lectures        ==> 讲义
    └── slides          ==> 幻灯片/课件/PPT（在`VSCode`上使用`MARP`编辑）
```

### `emoji`

1. 资源形式：
    1. 📖 纸质出版物
    1. 💻 电子出版物
    1. 🌐 互联网资源
1. 评价等级：
    1. 👑 顶级好评（权威）
    1. 👍 一级好评
    1. 🌟 二级好评
    1. ⭐ 一般好评
1. 重要等级：
    1. 🌟 重要
    1. ⭐ 次重要
1. 费用情况：
    1. 🆓 免费
    1. 💰 收费
1. 其他：
    1. 🏛️ 官方
    1. ❗ 注意
    1. ❓ 有歧义，或待确认

### 帮助完善Contribution

期待您一起完善，您可以通过以下方式帮助完善：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`的`issue`发起一个新的`issue`（标签设置成`optimize`）

仓库地址：

1. 主地址：<https://gitlab.com/atarm/courses/sf>
1. 镜像地址：<https://jihulab.com/atarm/courses/sf>

## Futhermore

### Standards and Specifications

<!-- {standards, specifications, conventions goes here} -->

### Documents and Supports

<!-- {trusted or official documents goes here} -->

1. [Spring Framework Artifacts.](https://github.com/spring-projects/spring-framework/wiki/Spring-Framework-Artifacts)

### Manuals and CheatSheets

<!-- {manuals goes here} -->

1. 👍 [javadoc.](https://javadoc.io/)
1. 👍 [devdocs.](https://devdocs.io/)
1. 👍 [quickref.](https://quickref.me/)
1. 👍 [devhints.](https://devhints.io/)

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses and tutorials goes here} -->

### Papers and Articles

<!-- {special topics goes here} -->

### Playgrounds and Exercises

<!-- {exercises here} -->

### Examples and Templates

<!-- {examples here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

### Miscellaneous

<!-- {misc here} -->
