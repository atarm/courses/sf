#!/usr/bin/env bash

projects=("sprintboot-edition/itheima-springboot")

base_dir=$(cd "$(dirname "$0")" || exit; pwd)

if [[ -z ${base_dir} ]]; then
    exit
fi

repostiory_path="${base_dir}/m2/repository"
tar_filepath="${base_dir}/m2-repository.tar.gz"
is_mvn_exist=false

function before(){
    if command -v mvn >/dev/null 2>&1 ;then
        is_mvn_exist=true
    else
        is_mvn_exist=false
    fi

    if [[ -f "${tar_filepath}" ]];then
        rm "${tar_filepath}"
    fi
}

function after(){
    tar cfz "${tar_filepath}" --exclude=*.DS_Store -C "${base_dir}/m2" repository
    # rm -rf m2
}

function download_single(){
    project_path=$1

    cd "${project_path}" || exit

    if [[ -f "./mvnw" ]];then
        "./mvnw" dependency:go-offline -Dmaven.repo.local="${repostiory_path}"
    else [[ ${is_mvn_exist} = true ]]
        mvn dependency:go-offline -Dmaven.repo.local="${repostiory_path}"
    fi

    cd - || exit
}

function download_all(){
    for project in "${projects[@]}";do
        download_single "${base_dir}/${project}"
    done
}

before
download_all
after