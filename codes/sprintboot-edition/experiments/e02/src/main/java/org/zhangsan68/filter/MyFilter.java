package org.zhangsan68.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter({"/servlet/my-servlet"})
public class MyFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        System.out.println("hello " + this.getClass().getCanonicalName() + "...");
        response.getWriter().println("before chain.doFilter()...");
        chain.doFilter(request, response);
        response.getWriter().println("after chain.doFilter()...");
    }
}
