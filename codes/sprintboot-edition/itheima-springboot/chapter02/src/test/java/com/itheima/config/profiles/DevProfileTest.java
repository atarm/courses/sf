package com.itheima.config.profiles;

//region restassured

import io.restassured.RestAssured;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.when;
//endregion

//region hamcrest
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.*;
//endregion hamcrest
//junit
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
//spring
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ExtendWith(SpringExtension.class)
public class DevProfileTest {

    @BeforeEach
    public void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8081;
    }

    @AfterEach
    public void tearDown() {
        RestAssured.reset();
    }

    @Test
    public void testDevProfile() {
        String actual = get("/show-db").then().extract().body().asString();
        assertThat(actual, is(equalTo("数据库配置环境 --> dev")));
    }
}
