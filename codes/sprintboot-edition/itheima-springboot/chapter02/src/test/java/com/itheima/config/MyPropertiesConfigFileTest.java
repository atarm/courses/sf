package com.itheima.config;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MyPropertiesConfigFileTest {

    @Value("${test.id}")
    private int id;

    @Value("${test.name}")
    private String name;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testMyProperties() {
        int expectedId = 110;
        String expectedName = "test";
        Assertions.assertEquals(expectedId, this.id);
        Assertions.assertEquals(expectedName, this.name);
    }
}