package com.itheima.domain;

import com.itheima.domain.Student;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class StudentTest {
    @Autowired
    private Student student;

    @Test
    public void testStudent() {
        System.out.println(student);
    }

}
