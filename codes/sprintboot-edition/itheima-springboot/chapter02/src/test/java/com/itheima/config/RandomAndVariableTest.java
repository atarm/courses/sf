package com.itheima.config;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class RandomAndVariableTest {

    @Value("${tom.age}")
    private int tomAge;

    @Value("${tom.description}")
    private String description;

    @Value("${system.env.path}")
    private String systemPath;

    @Test
    public void testRandom() {
        assertThat(tomAge, both(greaterThanOrEqualTo(10)).and(lessThanOrEqualTo(20)));
    }

    @Test
    public void testVariable() {
        System.out.println(description);
        System.out.println(systemPath);
    }
}
