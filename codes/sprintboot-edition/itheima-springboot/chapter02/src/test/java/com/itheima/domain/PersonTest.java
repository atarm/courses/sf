package com.itheima.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PersonTest {
    @Autowired
    private Person person;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getId() {

    }

    @Test
    void getName() {
    }

    @Test
    void getHobby() {
        ArrayList<String> expectedHobbies = new ArrayList<String>();
        expectedHobbies.add("play");
        expectedHobbies.add("read");
        expectedHobbies.add("sleep");
        assertThat(person.getHobby(), is(equalTo(expectedHobbies)));
    }

    @Test
    void getFamily() {
    }

    @Test
    void getMap() {
        System.out.println(person.getMap().getClass());
    }

    @Test
    void getPet() {
        Pet expected=new Pet();
        expected.setType("dog");
        expected.setName("kity");
        assertThat(person.getPet(),is(equalTo(expected)));
    }
}