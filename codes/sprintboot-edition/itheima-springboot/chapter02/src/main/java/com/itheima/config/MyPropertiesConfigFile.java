package com.itheima.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * @author aRoming
 */
@Configuration
@PropertySource("classpath:my-properties.properties")
public class MyPropertiesConfigFile {
}

