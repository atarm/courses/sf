package com.itheima.domain;

import java.util.Objects;

/**
 * @Classname Pet
 * @Description TODO
 * @Date 2019-3-1 14:45
 * @Created by CrazyStone
 */
public class Pet {
    private String type;
    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pet{" +
            "type='" + type + '\'' +
            ", name='" + name + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pet pet = (Pet) o;
        return Objects.equals(type, pet.type) && Objects.equals(name, pet.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }
}
