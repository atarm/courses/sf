package com.itheima.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


/**
 * @author aRoming
 */
@ImportResource("classpath:my-service-bean.xml") // load customized config
@Configuration
public class MyXmlConfigFile {
}
