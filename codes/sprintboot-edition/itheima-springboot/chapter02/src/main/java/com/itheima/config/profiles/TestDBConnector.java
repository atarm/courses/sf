package com.itheima.config.profiles;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * @author aRoming
 */
@Configuration
@Profile("test")
public class TestDBConnector implements DBConnector {
    @Override
    public String configure() {
        String ret = "数据库配置环境 --> test";
        System.out.println(ret);
        return ret;
    }
}

