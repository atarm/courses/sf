package com.itheima.config.profiles;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("prod")
public class ProdDBConnector implements DBConnector {
    @Override
    public String configure() {
        String ret = "数据库配置环境 --> prod";
        System.out.println(ret);
        return ret;
    }
}

