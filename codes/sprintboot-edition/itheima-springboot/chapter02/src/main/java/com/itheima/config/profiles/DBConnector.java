package com.itheima.config.profiles;

/**
 * @Classname DBConnector
 * @Description TODO
 * @Date 2019-3-1 15:05
 * @Created by CrazyStone
 */
public interface DBConnector {
    String configure();
}
