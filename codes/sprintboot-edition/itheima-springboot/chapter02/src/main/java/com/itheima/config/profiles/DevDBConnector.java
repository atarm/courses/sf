package com.itheima.config.profiles;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class DevDBConnector implements DBConnector {
    @Override
    public String configure() {
        String ret = "数据库配置环境 --> dev";
        System.out.println(ret);
        return ret;
    }
}

