package com.itheima.config;

import com.itheima.service.MyOtherService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname MyConfigClass
 * @Description TODO
 * @Date 2019-3-1 14:58
 * @Created by CrazyStone
 */
@Configuration   // 定义该类是一个配置类
public class MyConfigClass {
    @Bean
    public MyOtherService myOtherService() {
        return new MyOtherService();
    }
}
