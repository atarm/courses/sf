package com.itheima.config;

import com.itheima.servlet.component.MyFilter;
import com.itheima.servlet.component.MyListener;
import com.itheima.servlet.component.MyServlet;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class ServletConfig {
    @Bean
    public ServletRegistrationBean getServlet(MyServlet servlet) {
        ServletRegistrationBean registrationBean =
            new ServletRegistrationBean(servlet, "/servlet/component/my-servlet");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean getFilter(MyFilter filter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(filter);
        registrationBean.setUrlPatterns(Arrays.asList("/toLoginPage", "/servlet/component/my-filter"));
        return registrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean getServletListener(MyListener listener) {
        ServletListenerRegistrationBean registrationBean = new ServletListenerRegistrationBean(listener);
        return registrationBean;
    }
}