package com.itheima.servlet.component;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Component
public class MyFilter implements Filter {
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        String ret = "hello " + this.getClass().getCanonicalName() + "...";
        System.out.println(ret);
//        servletResponse.getWriter().println(ret);
        filterChain.doFilter(servletRequest, servletResponse);
    }

//    @Override
//    public void destroy() {
//    }
}

