package com.itheima.controller.file;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

/**
 * @author aRoming
 */
@Controller
public class UploadController {

    @Value("${file.upload.location}")
    private String uploadLocation;

    @GetMapping("/file/upload")
    public String toUpload() {
        return "upload";
    }

    @PostMapping("/file/upload.action")
    public String uploadFile(MultipartFile[] fileUpload, Model model) {
        model.addAttribute("uploadStatus", "上传成功！");
        for (MultipartFile file : fileUpload) {
            // rename the filename
            String filename = UUID.randomUUID() + "_" + file.getOriginalFilename();
            try {
                file.transferTo(new File(this.uploadLocation + filename));
            } catch (Exception e) {
                e.printStackTrace();
                model.addAttribute("uploadStatus", "上传失败： " + e.getMessage());
            }
        }
        return "upload";
    }
}
