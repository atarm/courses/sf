package com.itheima.controller.file;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * @author aRoming
 */
@Controller
@RequestMapping("/file")
public class DownloadController {

    @Value("${file.download.location}")
    private String downloadLocation;

    // 向文件下载页面跳转

    /**
     * download page
     *
     * @return page name
     */
    @GetMapping("/download")
    public String download() {
        return "download";
    }

    /**
     *
     * @param request HttpServletRequest
     * @param filename filename to download
     * @return file contents
     * @throws Exception
     */
    @GetMapping("/download.action")
    public ResponseEntity<byte[]> downloadAction(HttpServletRequest request,
                                                 String filename) throws Exception {
        // 创建该文件对象
        File file = new File(downloadLocation + filename);
        // 设置响应头
        HttpHeaders headers = new HttpHeaders();
        // 对文件名进行转码
        filename = FileControllerUtils.getFilename(request, filename);
        // 通知浏览器以下载方式打开
        headers.setContentDispositionFormData("attachment", filename);
        // 定义以流的形式下载返回文件数据
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        try {
            return new ResponseEntity<>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<byte[]>(e.getMessage().getBytes(), HttpStatus.EXPECTATION_FAILED);
        }
    }
}
