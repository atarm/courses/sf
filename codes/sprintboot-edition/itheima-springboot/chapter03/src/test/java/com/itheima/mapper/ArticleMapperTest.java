package com.itheima.mapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ArticleMapperTest {

    @Autowired
    private ArticleMapper articleMapper;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void selectArticle() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Article{");
        stringBuilder.append("id=1, title='Spring Boot基础入门', content='从入门到精通讲解...'");
        stringBuilder.append(", ");
        stringBuilder.append("commentList=[");
        stringBuilder.append("Comment{id=1, content='很全、很详细', author='狂奔的蜗牛', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=2, content='赞一个', author='tom', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=3, content='很详细', author='kitty', aId=1}");
        stringBuilder.append(", ");
        stringBuilder.append("Comment{id=4, content='很好，非常详细', author='张三', aId=1}");
        stringBuilder.append("]");
        stringBuilder.append("}");
        String expected = stringBuilder.toString();
        assertThat(articleMapper.selectArticle(1).toString(), is(equalTo(expected)));
    }

    @Test
    void updateArticle() {
    }
}