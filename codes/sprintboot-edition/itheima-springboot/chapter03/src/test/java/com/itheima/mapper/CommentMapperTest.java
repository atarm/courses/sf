package com.itheima.mapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class CommentMapperTest {

    @Autowired
    private CommentMapper commentMapper;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findById() {
        assertThat(commentMapper.findById(1).toString(),
            is(equalTo("Comment{id=1, content='很全、很详细', author='狂奔的蜗牛', aId=1}")));
    }

    @Test
    void insertComment() {
    }

    @Test
    void updateComment() {
    }

    @Test
    void deleteComment() {
    }
}