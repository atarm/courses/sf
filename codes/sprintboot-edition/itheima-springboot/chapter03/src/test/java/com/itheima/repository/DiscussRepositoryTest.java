package com.itheima.repository;

import com.itheima.domain.Discuss;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.startsWith;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class DiscussRepositoryTest {

    @Autowired
    private DiscussRepository repository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void deleteInBatch() {
    }

    @Test
    void findByAuthorNotNull() {
        List<Discuss> list = repository.findByAuthorNotNull();
        System.out.println(list);
    }

    @Test
    void findById() {
        assertThat(repository.findById(1).isPresent(),is(true));
    }

    @Test
    void getDiscussPaged() {
        Pageable pageable = PageRequest.of(0, 3);
        List<Discuss> allPaged = repository.getDiscussPaged(1, pageable);
        System.out.println(allPaged);
    }

    @Test
    void findByExample() {
        Discuss discuss = new Discuss();
        discuss.setAuthor("张三");
        Example<Discuss> example = Example.of(discuss);
        List<Discuss> list = repository.findAll(example);
        System.out.println(list);
    }

    @Test
    void findByExampleMather() {
        Discuss discuss = new Discuss();
        discuss.setAuthor("张");
        ExampleMatcher matcher = ExampleMatcher.matching()
            .withMatcher("author", startsWith());
        Example<Discuss> example = Example.of(discuss, matcher);
        List<Discuss> list = repository.findAll(example);
        System.out.println(list);
    }

    @Test
    void getDiscussPaged2() {
    }

    @Test
    void updateDiscuss() {
    }

    @Test
    @Modifying
    @Transactional
    @Rollback(true)
    void deleteDiscuss() {
        assertThat(repository.deleteDiscuss(3), is(equalTo(1)));
    }
}