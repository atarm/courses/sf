package com.itheima.repository;

import com.itheima.domain.Address;
import com.itheima.domain.Family;
import com.itheima.domain.Person;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class PersonRepositoryTest {
    @Autowired
    private PersonRepository repository;

    @BeforeEach
    void setUp() {
        Person person = new Person("张", "有才");
        person.setAddress(new Address("北京", "China"));
        List<Family> list = new ArrayList<>();
        list.add(new Family("父亲", "张良"));
        list.add(new Family("母亲", "李香君"));
        person.setFamilyList(list);
        Person person2 = new Person("James", "Harden");
        repository.save(person);
        repository.save(person2);
    }

    @AfterEach
    void tearDown() {
        this.repository.deleteAll();
    }

    @Test
    void findByLastname() {
    }

    @Test
    void findPersonByLastname() {
    }

    @Test
    void findByFirstnameAndLastname() {
    }

    @Test
    void findByAddress_City() {
        assertThat(repository.findByAddress_City("北京").size(), is(equalTo(1)));
    }

    @Test
    void findByFamilyList_Username() {
    }

    @Test
    public void updatePerson() {
        Person person = repository.findByFirstnameAndLastname("张", "有才").get(0);
        person.setLastname("小明");
        assertThat(repository.save(person).getLastname(), is(equalTo("小明")));
    }

    @Test
    public void deletePerson() {
        repository.delete(repository.findByFirstnameAndLastname("张", "有才").get(0));
        assertThat(repository.findByFirstnameAndLastname("张", "有才").size(), is(equalTo(0)));
    }
}