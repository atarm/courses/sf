package com.itheima.repository;

import com.itheima.domain.Discuss;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DiscussRepository extends JpaRepository<Discuss, Integer> {
    /**
     * retrieve discuss list which author is not null
     *
     * @return list of discuss
     */
    List<Discuss> findByAuthorNotNull();

    /**
     * 根据文章id分页查询Discuss评论集合
     *
     * @param aid
     * @param pageable
     * @return list of discuss
     */
    @Query(value = "SELECT c FROM t_comment c WHERE  c.aId = ?1")
    List<Discuss> getDiscussPaged(Integer aid, Pageable pageable);

    /**
     * 使用元素SQL语句，根据文章id分页查询Discuss评论集合
     *
     * @param aid
     * @param pageable
     * @return
     */
    @Query(value = "SELECT * FROM t_comment WHERE a_Id = ?1", nativeQuery = true)
    List<Discuss> getDiscussPaged2(Integer aid, Pageable pageable);

    /**
     * 根据评论id修改评论作者author
     *
     * @param author
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE t_comment c SET c.author = ?1 WHERE  c.id = ?2")
    int updateDiscuss(String author, Integer id);

    /**
     * 根据评论id删除评论
     *
     * @param id
     */
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM t_comment c WHERE c.id = ?1")
    int deleteDiscuss(Integer id);
}
