package com.itheima.service;

import com.itheima.dao.CommentRepository;
import com.itheima.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@CacheConfig(cacheNames = "comment")
@Service
public class AnnotationCommentService {
    @Autowired
    private CommentRepository commentRepository;

    @Cacheable(unless = "#result==null")
    public Comment findCommentById(int id) {
        Optional<Comment> optional = commentRepository.findById(id);
        return optional.isPresent()? optional.get() : null;
    }

    @CachePut(key = "#result.id")
    public Comment updateComment(Comment comment) {
        commentRepository.updateComment(comment.getAuthor(), comment.getAId());
        return comment;
    }

    @CacheEvict()
    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
    }
}