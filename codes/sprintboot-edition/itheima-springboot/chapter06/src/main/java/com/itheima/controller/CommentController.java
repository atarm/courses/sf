package com.itheima.controller;

import com.itheima.domain.Comment;
import com.itheima.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/get/{id}")
    public Comment findById(@PathVariable("id") int id) {
        Comment comment = commentService.findCommentById(id);
        return comment;
    }

    @PatchMapping("/update/{id}/{author}")
    public Comment updateComment(@PathVariable("id") int id,
                                 @PathVariable("author") String author) {
        Comment comment = commentService.findCommentById(id);
        comment.setAuthor(author);
        Comment updateComment = commentService.updateComment(comment);
        return updateComment;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteComment(@PathVariable("id") int id) {
        commentService.deleteCommentById(id);
    }
}
