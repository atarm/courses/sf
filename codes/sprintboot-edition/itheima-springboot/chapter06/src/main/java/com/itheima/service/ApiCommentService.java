package com.itheima.service;

import com.itheima.dao.CommentRepository;
import com.itheima.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class ApiCommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private RedisTemplate redisTemplate;

    public Comment findCommentById(int id) {
        // retrieve
        Object object = redisTemplate.opsForValue().get("comment_" + id);
        if (object != null) {
            return (Comment) object;
        } else {
            // 缓存中没有，就进入数据库查询
            Optional<Comment> optional = commentRepository.findById(id);
            if (optional.isPresent()) {
                Comment comment = optional.get();
                // 将查询结果进行缓存，并设置有效期为1天
                redisTemplate.opsForValue().set("comment_" + id, comment, 5, TimeUnit.MINUTES);
                return comment;
            } else {
                return null;
            }
        }
    }

    public Comment updateComment(Comment comment) {
        commentRepository.updateComment(comment.getAuthor(), comment.getAId());
        // 更新数据后进行缓存更新
        redisTemplate.opsForValue().set("comment_" + comment.getId(), comment);
        return comment;
    }

    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
        // 删除数据后进行缓存删除
        redisTemplate.delete("comment_" + id);
    }
}

