package com.itheima.service;

import com.itheima.dao.CommentRepository;
import com.itheima.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public Comment findCommentById(int id) {
        Optional<Comment> optional = commentRepository.findById(id);
        return optional.isPresent()? optional.get() : null;
    }

    public Comment updateComment(Comment comment) {
        commentRepository.updateComment(comment.getAuthor(), comment.getAId());
        return comment;
    }

    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
    }
}
