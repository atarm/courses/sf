package com.itheima.controller;

import com.itheima.domain.Comment;
import com.itheima.service.ApiCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Classname ApiCommentController
 * @Description TODO
 * @Date 2019-3-4 14:36
 * @Created by CrazyStone
 */
@RestController
@RequestMapping("/api")  // 窄化请求路径
public class ApiCommentController {
    @Autowired
    private ApiCommentService apiCommentService;

    @GetMapping("/get/{id}")
    public Comment findById(@PathVariable("id") int id) {
        return apiCommentService.findCommentById(id);
    }

    @PatchMapping("/update/{id}/{author}")
    public Comment updateComment(@PathVariable("id") int id,
                                 @PathVariable("author") String author) {
        Comment comment = apiCommentService.findCommentById(id);
        comment.setAuthor(author);
        return apiCommentService.updateComment(comment);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteComment(@PathVariable("id") int id) {
        apiCommentService.deleteCommentById(id);
    }
}
