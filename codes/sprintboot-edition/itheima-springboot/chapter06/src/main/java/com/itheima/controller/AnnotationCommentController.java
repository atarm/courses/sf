package com.itheima.controller;

import com.itheima.domain.Comment;
import com.itheima.service.AnnotationCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Classname AnnotationCommentController
 * @Description TODO
 * @Date 2019-3-4 9:40
 * @Created by CrazyStone
 */
@RestController
@RequestMapping("/annotation")
public class AnnotationCommentController {
    @Autowired
    private AnnotationCommentService annotationCommentService;

    @GetMapping("/get/{id}")
    public Comment findById(@PathVariable("id") int id) {
        Comment comment = annotationCommentService.findCommentById(id);
        return comment;
    }

    @PatchMapping("/update/{id}/{author}")
    public Comment updateComment(@PathVariable("id") int id,
                                 @PathVariable("author") String author) {
        Comment comment = annotationCommentService.findCommentById(id);
        comment.setAuthor(author);
        Comment updateComment = annotationCommentService.updateComment(comment);
        return updateComment;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteComment(@PathVariable("id") int id) {
        annotationCommentService.deleteCommentById(id);
    }
}

