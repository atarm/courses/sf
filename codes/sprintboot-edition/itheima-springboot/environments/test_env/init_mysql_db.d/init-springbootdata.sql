DROP DATABASE IF EXISTS springbootdata;

CREATE DATABASE springbootdata;

USE springbootdata;

-- region table `t_article`
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article`
(
    `id`      int(20) NOT NULL AUTO_INCREMENT COMMENT '文章id',
    `title`   varchar(200) DEFAULT NULL COMMENT '文章标题',
    `content` longtext COMMENT '文章内容',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `t_article`
VALUES ('1', 'Spring Boot基础入门', '从入门到精通讲解...');
INSERT INTO `t_article`
VALUES ('2', 'Spring Cloud基础入门', '从入门到精通讲解...');
-- endregion table `t_article`

-- region table `t_comment`
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`
(
    `id`      int(20) NOT NULL AUTO_INCREMENT COMMENT '评论id',
    `content` longtext COMMENT '评论内容',
    `author`  varchar(200) DEFAULT NULL COMMENT '评论作者',
    `a_id`    int(20)      DEFAULT NULL COMMENT '关联的文章id',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `t_comment`
VALUES ('1', '很全、很详细', '狂奔的蜗牛', '1');
INSERT INTO `t_comment`
VALUES ('2', '赞一个', 'tom', '1');
INSERT INTO `t_comment`
VALUES ('3', '很详细', 'kitty', '1');
INSERT INTO `t_comment`
VALUES ('4', '很好，非常详细', '张三', '1');
INSERT INTO `t_comment`
VALUES ('5', '很不错', '张杨', '2');
-- endregion table `t_comment`


-- region table `t_customer`
DROP TABLE IF EXISTS `t_customer`;
CREATE TABLE `t_customer`
(
    `id`       int(20)    NOT NULL AUTO_INCREMENT,
    `username` varchar(200)        DEFAULT NULL,
    `password` varchar(200)        DEFAULT NULL,
    `valid`    tinyint(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8mb4;
INSERT INTO `t_customer`
VALUES ('1', 'shitou', '$2a$10$5ooQI8dir8jv0/gCa1Six.GpzAdIPf6pMqdminZ/3ijYzivCyPlfK', '1');
INSERT INTO `t_customer`
VALUES ('2', '李四', '$2a$10$5ooQI8dir8jv0/gCa1Six.GpzAdIPf6pMqdminZ/3ijYzivCyPlfK', '1');
-- endregion table `t_customer`

-- region table `t_authority`
DROP TABLE IF EXISTS `t_authority`;
CREATE TABLE `t_authority`
(
    `id`        int(20) NOT NULL AUTO_INCREMENT,
    `authority` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb4;
INSERT INTO `t_authority`
VALUES ('1', 'ROLE_common');
INSERT INTO `t_authority`
VALUES ('2', 'ROLE_vip');
-- endregion table `t_authority`

-- region table `t_customer_authority`
DROP TABLE IF EXISTS `t_customer_authority`;
CREATE TABLE `t_customer_authority`
(
    `id`           int(20) NOT NULL AUTO_INCREMENT,
    `customer_id`  int(20) DEFAULT NULL,
    `authority_id` int(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4;
INSERT INTO `t_customer_authority`
VALUES ('1', '1', '1');
INSERT INTO `t_customer_authority`
VALUES ('2', '2', '2');
-- endregion table `t_customer_authority`

-- region table `persistent_logins`
create table persistent_logins
(
    username  varchar(64) not null,
    series    varchar(64) primary key,
    token     varchar(64) not null,
    last_used timestamp   not null
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
-- endregion table `persistent_logins`
