#!/usr/bin/env bash

DIR=$(dirname "$0")

cd "${DIR}/test_env"
docker compose down --volumes
cd -