package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InitializrChapter01Application {

    public static void main(String[] args) {
        SpringApplication.run(InitializrChapter01Application.class, args);
    }

}
