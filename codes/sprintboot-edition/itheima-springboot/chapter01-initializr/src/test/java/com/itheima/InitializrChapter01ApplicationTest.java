package com.itheima;

import com.itheima.controller.HelloController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * IDEA开发工具快捷方式自动创建的主程序启动类对应的单元测试类
 */
//@RunWith(SpringRunner.class)   // 测试启动器，并加载Spring Boot测试注解
@ExtendWith(SpringExtension.class)
@SpringBootTest       // 标记为Spring Boot单元测试类，并加载项目的ApplicationContext上下文环境
public class InitializrChapter01ApplicationTest {

    @Autowired
    private HelloController helloController;

    // 自动创建的单元测试方法示例
    @Test
    public void contextLoads() {
    }

    @Test
    public void testHelloController() {
        String actual = helloController.hello();
        String expect = "hello，setup spring boot project with initializr...";
        //Assertions.assertEquals(expect, actual);
    }
}
