package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @classname MavenChapter01Application
 * @description 项目启动类
 * @date 2019-3-1 13:48
 * @created by CrazyStone
 */
@SpringBootApplication  // 标记该类为主程序启动类
public class MavenChapter01Application {
    /**
     * @param args none
     */
    public static void main(String[] args) {
        SpringApplication.run(MavenChapter01Application.class, args);
    }
}
