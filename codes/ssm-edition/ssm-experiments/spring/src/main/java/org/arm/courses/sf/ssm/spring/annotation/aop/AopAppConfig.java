package org.arm.courses.sf.ssm.spring.annotation.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AopAppConfig {
}
