package org.arm.courses.sf.ssm.spring.annotation.ioc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class IocAppConfig {
}
