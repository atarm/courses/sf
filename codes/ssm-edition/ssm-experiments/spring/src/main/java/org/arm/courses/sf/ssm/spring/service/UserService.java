package org.arm.courses.sf.ssm.spring.service;

import org.arm.courses.sf.ssm.spring.pojo.User;

public interface UserService {
    boolean updateUserById(User user, int id);
}
