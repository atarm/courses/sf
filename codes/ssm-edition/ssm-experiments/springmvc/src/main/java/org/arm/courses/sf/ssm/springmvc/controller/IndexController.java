package org.arm.courses.sf.ssm.springmvc.controller;

import org.arm.courses.sf.ssm.springmvc.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @Autowired
    private IndexService service;

    @RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index", service.getDefault());
    }
}
