package org.arm.courses.sf.ssm.springmvc.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class FileUploadService {

    /**
     * generate a random file name with current datetime plus the original file name
     * @param filename the original file name
     * @return the generated file name
     */
    public static String generateFileName(String filename) {
        String formatDate = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
        int random = new Random().nextInt(10000);
        return formatDate + "_" + random + "_" + filename;
    }

    /**
     * save the file to the target directory and insert the file infos into database
     * @param multipartFile the file to be saved
     * @param targetDirectory  the target directory
     * @throws IOException if the file cannot be saved
     */
    public void saveFile(MultipartFile multipartFile, String targetDirectory) throws IOException {
        String originalFilename = multipartFile.getOriginalFilename();
        assert originalFilename != null;
        String targetFileName = generateFileName(originalFilename);

        //region save the file to the target directory
        File target = new File(targetDirectory, targetFileName);
        FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), target);
        //endregion save the file to the target directory

        //region insert the file infos into database

        //endregion insert the file infos into database
    }
}
