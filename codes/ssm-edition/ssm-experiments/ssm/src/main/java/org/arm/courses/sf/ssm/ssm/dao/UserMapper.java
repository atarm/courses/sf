package org.arm.courses.sf.ssm.ssm.dao;

import org.apache.ibatis.annotations.Param;
import org.arm.courses.sf.ssm.ssm.pojo.User;

import java.util.List;

public interface UserMapper {
    List<User> findAllUsers();

    User findUserById(@Param("id") int id);
}
