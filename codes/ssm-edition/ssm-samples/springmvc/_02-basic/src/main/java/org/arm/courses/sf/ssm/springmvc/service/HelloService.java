package org.arm.courses.sf.ssm.springmvc.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
//@Component
public class HelloService {

    public Map<String, String> hello() {
        HashMap<String, String> ret = new HashMap<String, String>();
        ret.put("message","hello, SpringMVC...");
        return ret;
    }
}
