<%--
  Created by IntelliJ IDEA.
  User: roamwonder
  Date: 2023/5/5
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>file upload</title>
</head>
<body>
<h2>文件上传</h2>
<form action="/file/upload/upload.action" method="post" enctype="multipart/form-data">
    <p>请选择文件：<input type="file" name="file"></p>
    <p><input type="submit" value="上传"></p>
</form>
</body>
</html>
