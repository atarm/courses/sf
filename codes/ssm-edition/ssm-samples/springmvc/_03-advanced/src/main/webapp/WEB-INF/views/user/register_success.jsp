<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>注册成功</title>
</head>
<body>
<fieldset>
    <legend>注册成功</legend>
    用户名：${user.username }<br>
    密码：${user.password }<br>
    邮箱：${user.email }<br>
    手机号：${user.phone }<br>
</fieldset>
</body>
</html>
