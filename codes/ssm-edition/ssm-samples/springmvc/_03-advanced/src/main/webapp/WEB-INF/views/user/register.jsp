<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>注册</title>
</head>
<body>
<form:form commandName="user" modelAttribute="user" action="/user/register.action" method="post">
    <fieldset>
        <legend>用户注册</legend>
        用户名*：<form:input path="username"/><span><form:errors path="username"/></span><br>
        密码*：<form:input path="password"/><span><form:errors path="password"/></span><br>
        邮箱*：<form:input path="email"/><span><form:errors path="email"/></span><br>
        手机号*：<form:input path="phone"/><span><form:errors path="phone"/></span><br>
        <input type="submit" value="注册">
    </fieldset>
</form:form>
</body>
</html>
