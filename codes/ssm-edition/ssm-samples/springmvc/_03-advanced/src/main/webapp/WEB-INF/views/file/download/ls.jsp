<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>list files</title>
</head>
<body>
<table>
    <tr>
        <td>可下载的文件</td>
    </tr>

<%--    <%--%>
<%--        ArrayList<String> filenames = (ArrayList<String>) request.getAttribute("filenames");--%>
<%--        for (String filename : filenames) {--%>
<%--            %>--%>
<%--                <tr>--%>
<%--                    <td>--%>
<%--                        <a href="${pageContext.request.contextPath}/download?filename=<%=filename%>">--%>
<%--                            <%=filename%>--%>
<%--                        </a>--%>
<%--                    </td>--%>
<%--                </tr>--%>
<%--            <%--%>
<%--        }--%>
<%--    %>--%>

        <c:forEach items="${filenames}" var="filename">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/file/download/download.action?filename=${filename}">${filename}</a>
                </td>
            </tr>
        </c:forEach>
</table>
</body>
</html>
