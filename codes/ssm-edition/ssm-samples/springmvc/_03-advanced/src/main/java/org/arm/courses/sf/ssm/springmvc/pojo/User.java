package org.arm.courses.sf.ssm.springmvc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @NotBlank(message = "用户名不能为空")
    private String username;

//    @JsonIgnore
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Length(min = 6, max = 18, message = "密码长度必须在6~18位")
    private String password;
    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    private String email;
    @Pattern(regexp = "[1][3|4|5|7|8][0-9]{9}", message = "手机号码格式不正确")
    private String phone;
}
