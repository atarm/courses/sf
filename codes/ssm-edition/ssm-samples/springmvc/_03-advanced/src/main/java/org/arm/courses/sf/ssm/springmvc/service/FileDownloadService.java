package org.arm.courses.sf.ssm.springmvc.service;

import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileDownloadService {

    /**
     * list all files in the directory
     * @param directoryPath the directory path
     * @return map of filenames which key is "filenames" and value is the list of filenames
     */
    public Map<String, ArrayList<String>> listFiles(String directoryPath) {
        File directory = new File(directoryPath);
        File[] files = directory.listFiles();
        ArrayList<String> filenames = new ArrayList<String>();
        if (files != null) {
            for (File file : files) {
                filenames.add(file.getName());
            }
        }
        HashMap<String, ArrayList<String>> ret = new HashMap<>();
        ret.put("filenames", filenames);
        return ret;
    }

    /**
     * download the file
     * @param file the file to be downloaded
     * @param out the output stream of the response
     * @throws IOException if the file cannot be downloaded
     */
    public void download(File file, ServletOutputStream out) throws IOException {
        FileInputStream in = new FileInputStream(file);
        int count = 0;
        byte[] b = new byte[1024];
        while ((count = in.read(b)) != -1) {
            out.write(b, 0, count);
        }
        out.flush();
        in.close();
    }
}
