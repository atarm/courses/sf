package org.arm.courses.sf.ssm.springmvc.controller;

import org.arm.courses.sf.ssm.springmvc.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;

@RequestMapping("/user")
@Controller
public class UserController {

    @RequestMapping(value = "/register")
    public String register(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "user/register";
    }

    /**
     * @param user the user want to register and should be validated and if success, will be added to model
     * @param bindingResult the result of validation
//     * @param errors the errors of validation
     * @param model the model of view
     * @return the view depends on the validation result, if success, return success view, else return register view
     */
    @RequestMapping(value = "/register.action")
    public String registerAction(@Valid User user, BindingResult bindingResult,
                                  Model model) {
//        if (errors.hasErrors()) {
////            FieldError fieldError = bindingResult.getFieldError();
////            System.out.println("Code:" + fieldError.getCode() + ", object:" + fieldError.getObjectName()
////                + ", field:" + fieldError.getField());
//            return "/user/register";
//        }
        if (bindingResult.hasErrors()){
            return "/user/register";
        }
        model.addAttribute("user", user);
        return "/user/register_success";
    }
}
