package org.arm.courses.sf.ssm.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HelloController {

    @RequestMapping("/hello")
    public ModelAndView hello(HttpServletRequest request, HttpServletResponse response){
        ModelAndView modelAndView = new ModelAndView("hello");
        modelAndView.addObject("message","hello, SpringMVC...");
        return modelAndView;
    }
}
