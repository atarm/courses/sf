package org.arm.courses.sf.ssm.spring.schema.ioc.dao;

import org.arm.courses.sf.ssm.spring.dao.UserDao;

public class UserDaoImplFactory {
    public static UserDao getUserDaoByFactoryMethod() {
        return new UserDaoImpl();
    }

    public UserDao getUserDaoByFactoryBean() {
        return new UserDaoImpl();
    }
}
