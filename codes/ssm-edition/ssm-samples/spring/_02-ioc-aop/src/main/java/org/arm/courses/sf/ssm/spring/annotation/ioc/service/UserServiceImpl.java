package org.arm.courses.sf.ssm.spring.annotation.ioc.service;

import org.arm.courses.sf.ssm.spring.annotation.ioc.dao.UserDaoImpl;
import org.arm.courses.sf.ssm.spring.pojo.User;
import org.arm.courses.sf.ssm.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDaoImpl userDaoImpl;

    @Override
    public void updateUser() {
        User user = new User();
        user.setId(1);
        userDaoImpl.updateUserById(user);
    }
}
