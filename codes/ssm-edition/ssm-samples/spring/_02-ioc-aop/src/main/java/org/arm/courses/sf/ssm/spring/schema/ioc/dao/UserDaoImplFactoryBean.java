package org.arm.courses.sf.ssm.spring.schema.ioc.dao;

import org.springframework.beans.factory.FactoryBean;

public class UserDaoImplFactoryBean implements FactoryBean<UserDaoImpl> {
    @Override
    public UserDaoImpl getObject() throws Exception {
        return new UserDaoImpl();
    }

    @Override
    public Class<?> getObjectType() {
        return UserDaoImpl.class;
    }

    @Override
    public boolean isSingleton() {
        return FactoryBean.super.isSingleton();
    }
}
