package org.arm.courses.sf.ssm.spring.dao;

import org.arm.courses.sf.ssm.spring.pojo.User;

public interface UserDao {
    int updateUserById(User user);
}
