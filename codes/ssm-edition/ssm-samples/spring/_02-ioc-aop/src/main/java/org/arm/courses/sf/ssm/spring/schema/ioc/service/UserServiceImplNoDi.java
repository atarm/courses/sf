package org.arm.courses.sf.ssm.spring.schema.ioc.service;

import org.arm.courses.sf.ssm.spring.dao.UserDao;
import org.arm.courses.sf.ssm.spring.pojo.User;
import org.arm.courses.sf.ssm.spring.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class UserServiceImplNoDi implements UserService {
    @Override
    public void updateUser() {
        ApplicationContext app = new ClassPathXmlApplicationContext("schema/iocDemoContext.xml");
        UserDao userDao = (UserDao) app.getBean("userDao");
        User user = (User) app.getBean("exampleUser");
        user.setId(1);
        userDao.updateUserById(user);
    }
}
