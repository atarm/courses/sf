package org.arm.courses.sf.ssm.spring.schema.ioc.service;

import org.arm.courses.sf.ssm.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:schema/iocDemoContext.xml")
class UserServiceImplNoDiTest {
    @Autowired
    private UserService userService;

    @Test
    void updateUser() {
        userService.updateUser();
        assertThat("always pass", anything());
    }
}