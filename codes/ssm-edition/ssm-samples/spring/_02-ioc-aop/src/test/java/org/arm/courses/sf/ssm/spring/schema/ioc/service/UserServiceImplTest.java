package org.arm.courses.sf.ssm.spring.schema.ioc.service;

import org.arm.courses.sf.ssm.spring.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:schema/iocDemoContext.xml")
class UserServiceImplTest {

    @Autowired
    @Qualifier("userServicePropertyDi")
    private UserService userServicePropertyDi;

    @Autowired
    @Qualifier("userServiceConstructorDi")
    private UserService userServiceConstructorDi;


    @Test
    void updateUserPropertyDi() {
        userServicePropertyDi.updateUser();
        System.out.println("this class is ==> " + userServicePropertyDi.getClass());
        assertThat("always pass", anything());
    }

    @Test
    void updateUserConstructorDi() {
        userServiceConstructorDi.updateUser();
        System.out.println("this class is ==> " + userServiceConstructorDi.getClass());
        assertThat("always pass", anything());
    }
}