package org.arm.courses.sf.ssm.spring.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;

public class UserDaoLogger {
    private static final Logger logger = LogManager.getLogger(UserDaoLogger.class);


    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("before calling method: " + methodName);
    }

    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("after calling method: " + methodName);
    }
}
