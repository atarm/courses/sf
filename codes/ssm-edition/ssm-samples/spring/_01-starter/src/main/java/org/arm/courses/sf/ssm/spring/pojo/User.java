package org.arm.courses.sf.ssm.spring.pojo;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private String name;
    private String password;
    private Integer age;
    private String address;
}
