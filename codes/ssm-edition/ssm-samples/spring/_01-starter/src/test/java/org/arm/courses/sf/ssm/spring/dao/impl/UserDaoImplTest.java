package org.arm.courses.sf.ssm.spring.dao.impl;

import org.arm.courses.sf.ssm.spring.dao.UserDao;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:applicationContext.xml")
class UserDaoImplTest {
    @Autowired
//    @Qualifier("userDao")
    private UserDao userDao;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void updateUserById() {
        userDao.updateUserById(null);
        assertThat("always pass", anything());
    }
}