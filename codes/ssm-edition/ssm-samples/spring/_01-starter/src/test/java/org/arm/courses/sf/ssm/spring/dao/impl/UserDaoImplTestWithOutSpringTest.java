package org.arm.courses.sf.ssm.spring.dao.impl;

import org.arm.courses.sf.ssm.spring.dao.UserDao;
import org.junit.jupiter.api.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

class UserDaoImplTestWithOutSpringTest {
    private static ApplicationContext context;

    @BeforeAll
    static void beforeAll() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @AfterAll
    static void afterAll() {
        context = null;
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void updateUserById() {
        UserDao userDao = (UserDao) context.getBean("userDao");
        userDao.updateUserById(null);
        assertThat("always pass", anything());
    }
}