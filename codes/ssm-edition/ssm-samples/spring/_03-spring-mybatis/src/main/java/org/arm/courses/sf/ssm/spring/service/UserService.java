package org.arm.courses.sf.ssm.spring.service;

import org.arm.courses.sf.ssm.spring.pojo.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    User getUserById(int id);

    boolean addUser(User user);

    boolean updateUserById(User user, int id);

    boolean deleteUser(User user);

    boolean deleteUser(int id);
}
