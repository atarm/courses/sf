package org.arm.courses.sf.ssm.spring.dao;

import org.apache.ibatis.annotations.Param;
import org.arm.courses.sf.ssm.spring.pojo.User;

import java.util.List;

public interface UserMapper {
    List<User> findAllUsers();

    User findUserById(@Param("id") int id);

    int addUser(@Param("user") User user);

    int updateUserById(@Param("user") User user);

    int deleteUserById(@Param("id") int id);
}
