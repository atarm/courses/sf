package org.arm.courses.sf.ssm.spring.service;

import org.arm.courses.sf.ssm.spring.dao.UserMapper;
import org.arm.courses.sf.ssm.spring.pojo.User;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private SqlSessionTemplate sqlSession;
    @Autowired
    @Qualifier("userMapper")
    private UserMapper userMapper;

    @Autowired
    @Qualifier("userMapper_")
    private UserMapper userMapper_;

    @Override
    public List<User> getAllUsers() {
        return this.sqlSession.getMapper(UserMapper.class).findAllUsers();
    }

    @Override
    public User getUserById(int id) {
        return userMapper_.findUserById(id);
    }

    @Override
    public boolean addUser(User user) {
        userMapper.addUser(user);
        return 1 / 0 == 1;
    }

    @Override
    public boolean updateUserById(User user, int id) {
        user.setId(id);
        return userMapper.updateUserById(user) == 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteUser(User user) {
        return user.getId() != null && deleteUser(user.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteUser(int id) {
        return userMapper.deleteUserById(id) == 1;
    }
}
