package org.arm.courses.sf.ssm.spring.service;

import org.arm.courses.sf.ssm.spring.pojo.User;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(locations = "classpath:spring-mybatis.xml")
class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    void getAllUsers() {
        List<User> userList = userService.getAllUsers();
        userList.forEach(System.out::println);
        assertThat(userList.size(), is(equalTo(4)));
    }

    @Test
    void getUserById() {
        User user = userService.getUserById(1);
        assertThat(user.getName(), is(equalTo("张三")));
    }

//    @Test
//    public void testExceptionIsThrown() {
//        String str = null;
//        assertThat(() -> str.length(), CoreMatchers.<Throwable>instanceOf(NullPointerException.class));
//    }

    /**
     * preconditions: table with 4 records
     * input: new User("孙七", "abcdefg")
     * steps: call userService.addUser()
     * expected output: 1. throws a RuntimeException; 2. rollback the addUser action
     */
    @Test
    void addUser() {
        User user = new User("孙七", "abcdefg");
//        userService.addUser(user);
        assertThrows(ArithmeticException.class, ()-> userService.addUser(user));
//        assertAll(
//            () -> assertThrows(ArithmeticException.class, ()-> userService.addUser(user)),
//            () -> assertEquals(4, userService.getAllUsers().size())
//        );
    }
}