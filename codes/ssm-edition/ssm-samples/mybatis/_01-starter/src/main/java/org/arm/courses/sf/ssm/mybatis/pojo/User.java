package org.arm.courses.sf.ssm.mybatis.pojo;


import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("User")
public class User {
    private Integer id;
    private String name;
    private String password;
}
