package org.arm.courses.sf.ssm.mybatis.dao;

import org.apache.ibatis.annotations.Param;
import org.arm.courses.sf.ssm.mybatis.pojo.User;

import java.util.List;

public interface UserMapper {
    List<User> findAllUsers();

    List<User> findUsersIn(@Param("ids") int[] ids);

//    List<User> findUsersIn(Set<Integer> ids);

    User findUserById(int id);

    int updateUser(@Param("user") User user);

    int updateUserByUserAndId(User user, int userid);

    int updatePasswordById(@Param("userId") int id, @Param("userPassword") String password);

    /**
     * insert new user into database
     *
     * @param user the user wanted to insert into database
     * @return number of affected rows
     */
    int addUser(User user);

    int removeUserById(int id);

    int removeUserByNameAndPassword(@Param("userName") String name, @Param("userPassword") String password);
}
