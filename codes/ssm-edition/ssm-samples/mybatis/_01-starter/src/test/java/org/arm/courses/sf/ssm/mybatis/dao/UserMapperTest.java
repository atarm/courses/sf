package org.arm.courses.sf.ssm.mybatis.dao;

import org.apache.ibatis.session.SqlSession;
import org.arm.courses.sf.ssm.mybatis.pojo.User;
import org.arm.courses.sf.ssm.mybatis.utils.MyBatisUtils;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

class UserMapperTest {
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testFindAllUsers() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            List<User> users = mapper.findAllUsers();
            assertThat(users.isEmpty(), Is.is(false));
            users.forEach(System.out::println);
        }
    }

    @Test
    void testFindUserById() {
        // 获取并安全释放SqlSession对象
        try (SqlSession session = MyBatisUtils.openSession()) {
            // 获取mapper对象
            UserMapper mapper = session.getMapper(UserMapper.class);
            // 使用mapper对象
            User user = mapper.findUserById(1);
            assertThat(user.getName(), IsEqual.equalTo("张三"));
            System.out.println(user);
        }
    }

    @Test
    void testUpdateUser() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            User user = new User();
            user.setId(1);
            user.setName("汤姆");
            user.setPassword("654321");
            int affected_rows = userMapper.updateUser(user);
            assertThat(affected_rows, Is.is(1));
//        sqlSession.commit();//do not commit in test scripts
        }
    }

    @Test
    void testUpdateUserByUserAndId() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            User user = new User();
            user.setName("汤姆");
            user.setPassword("654321");
            int affected_rows = userMapper.updateUserByUserAndId(user, 1);
            assertThat(affected_rows, Is.is(1));
//        sqlSession.commit();//do not commit in test scripts
        }
    }
    @Test
    void testAddUser() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            User user = new User();
            user.setName("测试");
            user.setPassword("9876543210");
            int affected_rows = userMapper.addUser(user);
            assertThat(affected_rows, Is.is(1));
//        sqlSession.commit();//do not commit in test scripts
        }
    }

    @Test
    void testRemoveUser() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            int affected_rows = userMapper.removeUserById(1);
            assertThat(affected_rows, Is.is(1));
//        sqlSession.commit();//do not commit in test scripts
        }
    }

    @Test
    void updatePasswordById() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            int affected_rows = userMapper.updatePasswordById(1, "999999");
            assertThat(affected_rows, Is.is(1));
//        sqlSession.commit();//do not commit in test scripts
        }
    }

    @Test
    void removeUserByNameAndPassword() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            int affected_rows = userMapper.removeUserByNameAndPassword("abc", "999999");
            assertThat(affected_rows, Is.is(0));
//        sqlSession.commit();//do not commit in test scripts
        }
    }

    @Test
    void findUsersIn() {
        try (SqlSession session = MyBatisUtils.openSession()) {
            UserMapper userMapper = session.getMapper(UserMapper.class);
            int[] ids = new int[]{1, 2, 3};
            List<User> users = userMapper.findUsersIn(ids);
            assertThat(users.size(), Is.is(3));
        }
    }
}
