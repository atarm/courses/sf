DROP DATABASE IF EXISTS mybatis;

CREATE DATABASE mybatis;

USE mybatis;

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user`
(
    `id`       int(20)     NOT NULL AUTO_INCREMENT,
    `name`     varchar(20) NOT NULL,
    `password` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `t_user`(`name`,`password`)
values ('张三', '123456'),
       ('李四', '123456'),
       ('王五', '654321'),
       ('赵六', '654321');