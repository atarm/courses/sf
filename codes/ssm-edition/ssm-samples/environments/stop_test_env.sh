#!/usr/bin/env bash

DIR=$(dirname "$0")

cd "${DIR}/test"
docker compose down --volumes
cd -