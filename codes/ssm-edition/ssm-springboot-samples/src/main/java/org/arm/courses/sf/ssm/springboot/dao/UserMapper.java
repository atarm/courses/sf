package org.arm.courses.sf.ssm.springboot.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.arm.courses.sf.ssm.springboot.pojo.User;

import java.util.List;

@Mapper
public interface UserMapper {
    List<User> findAllUsers();

    User findUserById(@Param("id") int id);
}
