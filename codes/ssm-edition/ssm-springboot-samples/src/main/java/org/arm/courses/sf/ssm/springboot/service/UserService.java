package org.arm.courses.sf.ssm.springboot.service;

import org.arm.courses.sf.ssm.springboot.pojo.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    User getUserById(int id) throws Exception;

}
